DELETE
FROM common.flyway_schema_history
WHERE installed_rank = 29;

DELETE
FROM common.flyway_schema_history
WHERE installed_rank = 27;

DELETE
FROM common.flyway_schema_history
WHERE installed_rank = 23;

DELETE
FROM common.flyway_schema_history
WHERE installed_rank = 25;

DELETE
FROM common.flyway_schema_history
WHERE installed_rank = 22;

DELETE
FROM common.flyway_schema_history
WHERE installed_rank = 21;

DELETE
FROM common.flyway_schema_history
WHERE installed_rank = 20;

DELETE
FROM common.flyway_schema_history
WHERE installed_rank = 24;

DELETE
FROM common.flyway_schema_history
WHERE installed_rank = 26;

DELETE
FROM common.flyway_schema_history
WHERE installed_rank = 9;

drop table common.stock_notification_line_items cascade;

drop table common.stock_notifications cascade;

drop table common.oos_phased_out_item cascade;

drop table common.oos_rationing_item cascade;

drop table common.oos_stock_out_item cascade;

drop table common.oos_insufficient_funding_item cascade;

drop table common.oos_full_filled_item cascade;

drop table common.oos_close_to_expire_item cascade;

drop table common.oos_stock_out_notification cascade;

drop table common.msd_stock_out_notifications cascade;

