/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto;

import static org.assertj.core.api.Assertions.assertThat;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.common.domain.MsdDailyStockStatus;

public class MsdDailyStockStatusDtoTest {

  private MsdDailyStockStatusDto stockStatusDto;

  @Before
  public void initialize() {
    stockStatusDto = new MsdDailyStockStatusDto();
  }

  @Test
  public void equalsContract() {
    EqualsVerifier.forClass(MsdDailyStockStatusDto.class)
        .withRedefinedSuperclass()
        .suppress(Warning.STRICT_INHERITANCE)
        .suppress(Warning.NONFINAL_FIELDS)
        .verify();
  }

  @Test
  public void shouldBeInstanceOfBaseDto() {
    assertThat(stockStatusDto instanceof BaseDto).isTrue();
  }

  @Test
  public void shouldBeInstanceOfMsdDailyStockStatusImporter() {
    assertThat(stockStatusDto instanceof MsdDailyStockStatus.Importer).isTrue();
  }

  @Test
  public void shouldBeInstanceOfMsdDailyStockStatusExporter() {
    assertThat(stockStatusDto instanceof MsdDailyStockStatus.Exporter).isTrue();
  }
}
