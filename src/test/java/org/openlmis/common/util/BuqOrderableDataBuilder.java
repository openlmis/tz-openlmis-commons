/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.util;

import java.util.UUID;
import org.openlmis.common.domain.BuqOrderable;
import org.openlmis.common.domain.BuqOrderableCategory;

public class BuqOrderableDataBuilder {

  private BuqOrderableCategory category;
  private UUID orderableId;
  private String buqClass;

  /**
   * Builds instance of {@link BuqOrderableDataBuilder} with sample data.
   */
  public BuqOrderableDataBuilder() {

    category = new BuqOrderableCategoryDataBuilder().build();
    orderableId = UUID.randomUUID();
    buqClass = "S";
  }

  /**
   * Builds instance of {@link BuqOrderable}.
   */
  public BuqOrderable build() {
    BuqOrderable buqOrderable = BuqOrderable.newBuqOrderable(orderableId, category, buqClass);
    return buqOrderable;
  }

  /**
   * Builds instance of {@link BuqOrderable} without id.
   */
  public BuqOrderable buildAsNew() {
    return this.build();
  }


  public BuqOrderableDataBuilder withCategory(BuqOrderableCategory category) {
    this.category = category;
    return this;
  }

  public BuqOrderableDataBuilder withOrderableId(UUID orderableId) {
    this.orderableId = orderableId;
    return this;
  }


}
