/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */


package org.openlmis.common.util;

import java.util.UUID;
import org.openlmis.common.domain.MsdDailyStockStatus;
import org.openlmis.common.domain.MsdDailyStockStatusValue;

public class MsdDailyStockStatusValueDataBuilder {

  private static int instanceNumber = 0;

  public String plant;

  public String partNumber;

  public String unitOfMeasure;

  public String partDescription;

  public String date;

  public Integer monthOfStock;

  public String onHandQuantity;

  private UUID id;

  /**
   * Builds instance of {@link MsdDailyStockStatusValueDataBuilder} with sample data.
   */
  public MsdDailyStockStatusValueDataBuilder() {
    instanceNumber++;
    id = UUID.randomUUID();
    plant = "plant " + instanceNumber;
    partNumber = "part " + instanceNumber;
    unitOfMeasure = "test";
    partDescription = "sample description";
    date = "15 March 2023";
    monthOfStock = 1;
    onHandQuantity = "100";
  }

  /**
   * Builds instance of {@link MsdDailyStockStatus}.
   */
  public MsdDailyStockStatusValue build() {
    MsdDailyStockStatusValue stockStatusValue = new MsdDailyStockStatusValue();
    stockStatusValue.setId(id);
    stockStatusValue.setPlant(plant);
    stockStatusValue.setPartNumber(partNumber);
    stockStatusValue.setPartDescription(partDescription);
    stockStatusValue.setUnitOfMeasure(unitOfMeasure);
    stockStatusValue.setDate(date);
    stockStatusValue.setMonthOfStock(monthOfStock);
    stockStatusValue.setOnHandQuantity(onHandQuantity);
    return stockStatusValue;
  }

  /**
   * Builds instance of {@link MsdDailyStockStatusValue} without id.
   */
  public MsdDailyStockStatusValueDataBuilder withoutId() {
    this.id = null;
    return this;
  }

}
