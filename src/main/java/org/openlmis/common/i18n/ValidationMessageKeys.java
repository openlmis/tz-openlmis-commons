/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.i18n;

public abstract class ValidationMessageKeys extends MessageKeys {

  public static final String ERROR_CONTEXTUAL_STATE_NULL = "Error contextual state null";
  private static final String INTEGRATION_LOG = "Integration_log";
  private static final String ERROR = join(SERVICE_PREFIX, INTEGRATION_LOG);

  private static final String REQUIRED = "required";

  private static final String DESTINATION_ID = "destinationId";

  private static final String NULL = "NULL";
  public static final String ERROR_NULL = join(ERROR, NULL);

  public static final String ERROR_DESTINATION_ID_REQUIRED = join(ERROR, DESTINATION_ID,
      REQUIRED);
  public static final String ERROR_INTEGRATION_NAME_REQUIRED =
      join(ERROR, "integrationName", REQUIRED);
  public static final String ERROR_CODE_REQUIRED =
      join(ERROR, "errorCode", REQUIRED);
  public static final String ERROR_MESSAGE_REQUIRED =
      join(ERROR, "errorMessage", REQUIRED);
  public static final String ERROR_SOURCE_TRANSACTION_ID_REQUIRED =
      join(ERROR, "sourceTransactionId", REQUIRED);

}
