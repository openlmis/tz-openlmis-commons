/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.i18n;

import java.util.Arrays;

public abstract class MessageKeys {

  public static final String SERVICE_TYPE = "serviceType";

  public static final String STOCK_OUT_NOTIFICATION = "stockOutNotification";
  public static final String PROOF_OF_DELIVERY = "proofOfDelivery";
  public static final String MSD_DAILY_STOCK_STATUS = "msdDailyStockStatus";

  public static final String MSD_FACILITY_UPDATE = "msdFacilityUpdate";

  public static final String TZ_REQUISITION_TEMPLATE = "tzRequisitionTemplate";

  public static final String ERROR_CONVERTING_MULTIPLE_REQUISITIONS
      = "convert.multiple.requisition";

  public static final String ERROR_REQUISITION_NOT_FOUND
      = "common.error.requisition.not.found";

  public static final String ERROR_ORDER_NOT_FOUND
      = "common.error.order.not.found";

  public static final String MUST_CONTAIN_VALUE
      = "common.error.requisition.must.contain.value";

  public static final String ERROR_IO
      = "input.output.error";

  protected static final String PROCESSING_PERIOD = "processingPeriod";

  private static final String DELIMITER = ".";

  public static final String SERVICE_PREFIX = "common";

  protected static final String VALIDATION = "validation";

  static final String SERVICE_ERROR_PREFIX = join(SERVICE_PREFIX, "error");

  protected static final String NULL = "null";

  private static final String ERROR = "error";

  protected static final String AND = "and";

  private static final String WIDGET = "widget";

  private static final String JAVERS = "javers";

  public static final String ID = "id";

  public static final String CODE = "code";

  public static final String MISMATCH = "mismatch";

  public static final String DUPLICATED = "duplicated";

  protected static final String INVALID = "invalid";

  public static final String NOT_FOUND = "notFound";

  protected static final String PARAMS = "params";

  public static final String ERROR_LINE_ITEM_REQUIRED = "lineItemIsRequired";

  public static final String ERROR_NOT_FOUND = "Not Found";
  public static final String ERROR_MISSING_MANDATORY_ITEMS = "Missing Mandatory Items";
  protected static final String REQUIRED = "required";
  protected static final String SEARCH = "search";
  protected static final String WITH = "with";
  protected static final String NAME = "name";
  protected static final String TYPE = "type";
  protected static final String FORMAT = "format";
  protected static final String UUID = "uuid";
  protected static final String DATE = "date";
  protected static final String BOOLEAN = "boolean";
  protected static final String UNAUTHORIZED = "unauthorized";
  protected static final String ID_MISMATCH = "idMismatch";
  protected static final String LACKS_PARAMETERS = "lacksParameters";
  protected static final String INVALID_PARAMS = "invalidParams";
  protected static final String NOT_UNIQUE = "notUnique";
  protected static final String NON_EXISTENT = "nonExistent";
  protected static final String EXTERNAL = "external";
  protected static final String DELETING = "deleting";
  protected static final String CREATED = "created";
  protected static final String GENERIC = "generic";
  protected static final String WITHOUT = "without";
  protected static final String FAILED = "failed";
  protected static final String SAVING = "saving";
  protected static final String SAVE = "save";
  protected static final String NUMBER = "number";
  protected static final String NUMERIC = "numeric";
  protected static final String EMPTY = "empty";
  protected static final String RESET = "reset";
  protected static final String WRONG = "wrong";
  protected static final String EXTRA_DATA = "extraData";
  protected static final String IS_INVARIABLE = "isInvariable";
  protected static final String USERNAME = "username";
  protected static final String MUST_BE_UNIQUE = "mustBeUnique";
  protected static final String MUST_BE_PROVIDED = "mustBeProvided";
  protected static final String INVALID_LENGTH = "invalidLength";
  protected static final String UNALLOWED_KEY = "unallowedKey";
  protected static final String MODIFIED_KEY = "modifiedKey";
  protected static final String FIELD_IS_INVARIANT = "fieldIsInvariant";
  protected static final String MISSING = "missing";
  protected static final String PROVIDED = "provided";
  protected static final String ASSIGNED = "isAlreadyAssigned";
  protected static final String NOT_TOGETHER = "notTogether";
  protected static final String PARAMETER = "parameter";
  protected static final String DATA = "data";
  protected static final String EXPORT = "export";
  protected static final String RECORD = "record";
  protected static final String IDEAL_STOCK_AMOUNT = "idealStockAmount";
  protected static final String NOT_ALLOWED = "notAllowed";
  protected static final String FIELD = "field";
  protected static final String UPLOAD = "upload";
  protected static final String FILE = "file";
  protected static final String INCORRECT = "incorrect";
  protected static final String HEADER = "header";
  protected static final String MANDATORY = "mandatory";
  protected static final String COLUMNS = "columns";
  protected static final String FORMATTING = "formatting";
  protected static final String PARSING = "parsing";
  protected static final String MUST_BE_POSITIVE_OR_ZERO = "mustBePositiveOrZero";
  protected static final String EXTENSION = "extension";
  protected static final String TOO = "too";
  protected static final String LARGE = "large";

  public static final String ERROR_PREFIX = join(SERVICE_PREFIX, ERROR);

  public static final String ERROR_SERVICE_REQUIRED = ERROR_PREFIX + ".service.required";

  public static final String ERROR_SERVICE_OCCURED = ERROR_PREFIX + ".service.errorOccured";

  public static final String ERROR_WIDGET_NOT_FOUND = join(ERROR_PREFIX, WIDGET, NOT_FOUND);
  public static final String ERROR_WIDGET_ID_MISMATCH = join(ERROR_PREFIX, WIDGET, ID, MISMATCH);
  public static final String ERROR_WIDGET_CODE_DUPLICATED =
      join(ERROR_PREFIX, WIDGET, CODE, DUPLICATED);


  public static final String ERROR_JAVERS_EXISTING_ENTRY =
      join(ERROR_PREFIX, JAVERS, "entryAlreadyExists");

  public static final String ERROR_MISSING_MANDATORY_FIELD = SERVICE_PREFIX
      + ".report.missingMandatoryField";
  public static final String ERROR_JASPER_REPORT_CREATION_WITH_MESSAGE =
      join(ERROR, "reportCreationWithMessage");

  public static String join(String... params) {
    return String.join(DELIMITER, Arrays.asList(params));
  }

  protected MessageKeys() {
    throw new UnsupportedOperationException();
  }

}
