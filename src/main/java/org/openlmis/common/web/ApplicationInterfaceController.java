/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import static org.openlmis.common.i18n.MessageKeys.NOT_FOUND;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.openlmis.common.domain.ApplicationInterface;
import org.openlmis.common.dto.ApplicationInterfaceDto;
import org.openlmis.common.exception.NotFoundException;
import org.openlmis.common.repository.ApplicationInterfaceRepository;
import org.openlmis.common.repository.custom.ApplicationInterfaceSearchParams;
import org.openlmis.common.service.ApplicationInterfaceBuilder;
import org.openlmis.common.service.ApplicationInterfaceService;
import org.openlmis.common.util.Pagination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.profiler.Profiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Controller used to expose Application interfaces via HTTP.
 */
@Controller
@RequestMapping(ApplicationInterfaceController.RESOURCE_PATH)
@Transactional
public class ApplicationInterfaceController extends BaseController {
  public static final String RESOURCE_PATH = API_PATH + "/applicationInterfaces";

  private static final Logger LOGGER =
      LoggerFactory.getLogger(ApplicationInterfaceController.class);

  @Autowired
  private ApplicationInterfaceRepository repository;

  @Autowired
  private ApplicationInterfaceService service;

  @Autowired
  private ApplicationInterfaceBuilder applicationInterfaceBuilder;

  /**
   * Endpoint to submit a applicationInterface.
   *
   * @param id                      UUID of application
   *                                interface which we want to update.
   * @param applicationInterfaceDto A application interface DTO bound
   *                                to the request body.
   * @return updated application interface dto.
   */
  @PostMapping("/{id}/submit")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public ApplicationInterfaceDto submitApplicationInterface(
      @PathVariable("id") UUID id,
      @RequestBody ApplicationInterfaceDto applicationInterfaceDto,
      BindingResult bindingResult) {
    return service
        .submit(applicationInterfaceDto,
            id, bindingResult);
  }

  /**
   * Allows updating application interface.
   *
   * @param id  UUID of application interface which we want to update.
   * @param applicationInterfaceDto A application interface DTO bound to the request body.
   * @return updated application interface.
   */
  @PutMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public ApplicationInterfaceDto save(@PathVariable("id") UUID id,
                                      @RequestBody ApplicationInterfaceDto
                                          applicationInterfaceDto) {
    if (!repository.existsById(id)) {
      throw new NotFoundException(NOT_FOUND);
    }

    ApplicationInterface updated = service
        .save(applicationInterfaceDto, id);

    return applicationInterfaceBuilder
        .buildDto(updated);
  }

  /**
   * Retrieves the specified application interface.
   */
  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public ApplicationInterfaceDto getSpecifiedApplicationInterface(
      @PathVariable("id") UUID id) {
    ApplicationInterface buq = repository.findById(id).orElseThrow(
        () -> new NotFoundException(NOT_FOUND));

    return applicationInterfaceBuilder.buildDto(buq);
  }

  /**
   * Deletes the specified application interface.
   */
  @DeleteMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteApplicationInterface(@PathVariable("id") UUID id) {
    if (!repository.existsById(id)) {
      throw new NotFoundException(NOT_FOUND);
    }

    repository.deleteById(id);
  }

  /**
   * Retrieves all ApplicationInterfaces that match the parameters passed.
   *
   * @param queryParams {@link ApplicationInterfaceSearchParams} request parameters.
   * @param pageable object used to encapsulate the pagination related values: page, size and sort.
   * @return List of wanted ApplicationInterfaces matching query parameters.
   */
  @RequestMapping(value = "getAll-list", method = GET)
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<ApplicationInterfaceDto> getAllApplicationInterface(
      @RequestParam(required = false) MultiValueMap<String, String> queryParams,
      Pageable pageable) {

    QueryApplicationInterfaceSearchParams params =
        new QueryApplicationInterfaceSearchParams(queryParams);

    Page<ApplicationInterface> page = repository.search(params, pageable);
    List<ApplicationInterfaceDto> content = page
        .getContent()
        .stream()
        .map(applicationInterfaceBuilder::buildDto)
        .collect(Collectors.toList());

    return Pagination.getPage(content, pageable, page.getTotalElements());
  }

  /**
   * Create a new application interface. If the ID is specified, ID will be ignored.
   *
   * @param interfaceDto application interface bound to request body
   * @return created application interface
   */
  @RequestMapping(value = "save", method = POST)
  @ResponseBody
  @ResponseStatus(CREATED)
  public ApplicationInterfaceDto createInterface(
      @RequestBody ApplicationInterfaceDto interfaceDto) {
    Profiler profiler = getProfiler("CREATE_APPLICATION", interfaceDto);

    LOGGER.debug("Try to create a new application interface");

    profiler.start("CONVERT_DTO_TO_DOMAIN");
    interfaceDto.setId(null);
    ApplicationInterface domain = ApplicationInterface.newInstance(interfaceDto);

    profiler.start("SAVE");
    ApplicationInterface result = service.saveOrUpdate(domain);

    profiler.start("CREATE_DTO");
    ApplicationInterfaceDto response = ApplicationInterfaceDto.newInstance(result);

    return stopProfiler(profiler, response);
  }

  /**
   * Retrieve all application interfaces.
   *
   * @return list of all application Interfaces.
   */
  @RequestMapping(value = "getAll", method = GET)
  @ResponseBody
  public List<ApplicationInterfaceDto> getAllApplicationInterfaces() {
    Profiler profiler = getProfiler("GET_APPLICATION_INTERFACES");

    profiler.start("DB_CALL");
    List<ApplicationInterface> db = repository.findAll();

    profiler.start("CONVERT_TO_DTOS");
    List<ApplicationInterfaceDto> dtos = db
        .stream()
        .map(ApplicationInterfaceDto::newInstance)
        .collect(Collectors.toList());

    return stopProfiler(profiler, dtos);
  }

  /**
   * Retrieve a application interface based on id.
   */
  @RequestMapping(value = "getBy/{id}", method = GET)
  @ResponseBody
  public ApplicationInterfaceDto getBy(@PathVariable("id") UUID reasonId) {
    Profiler profiler = getProfiler("GET_APPLICATION_INTERFACE");

    profiler.start("DB_CALL");
    ApplicationInterface reason = repository.findById(reasonId).orElse(null);

    if (null == reason) {
      stopProfiler(profiler, null);
      throw new ResourceNotFoundException(NOT_FOUND);
    }

    ApplicationInterfaceDto response = ApplicationInterfaceDto
        .newInstance(reason);

    return stopProfiler(profiler, response);
  }

  /**
   * Update a stock card line item reason.
   *
   * @param reasonId ID of the reason would be updated
   * @param reason   a stock card line item reason bound to request body
   * @return updated stock card line item reason
   */
  @RequestMapping(value = "update/{id}", method = PUT)
  @ResponseBody
  public ApplicationInterfaceDto updateApplication(@PathVariable("id") UUID reasonId,
                                              @RequestBody ApplicationInterfaceDto reason) {
    Profiler profiler = getProfiler("UPDATE_APPLICATION_INTERFACE");

    profiler.start("CHECK_APPLICATION_ID_EXISTS");
    service.checkUpdateIdExists(reasonId);

    profiler.start("CONVERT_TO_DOMAIN");
    LOGGER.debug("Try to update application interface with id: ", reasonId.toString());
    reason.setId(reasonId);
    ApplicationInterface domain = ApplicationInterface.newInstance(reason);

    profiler.start("UPDATE");
    ApplicationInterface result = service.saveOrUpdate(domain);

    profiler.start("CREATE_DTO");
    ApplicationInterfaceDto response = ApplicationInterfaceDto.newInstance(result);

    return stopProfiler(profiler, response);
  }

  private ApplicationInterfaceDto exportToDto(ApplicationInterface
                                                  request) {
    ApplicationInterfaceDto interfaceDto
        = new ApplicationInterfaceDto();
    request.export(interfaceDto);
    return interfaceDto;
  }

}
