/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.openlmis.common.domain.MsdFacilityUpdate;
import org.openlmis.common.dto.MsdFacilityUpdateDto;
import org.openlmis.common.dto.referencedata.FacilityDto;
import org.openlmis.common.exception.NotFoundException;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.i18n.MsdFacilityUpdateMessageKeys;
import org.openlmis.common.repository.MsdFacilityUpdateRepository;
import org.openlmis.common.service.referencedata.FacilityReferenceDataService;
import org.openlmis.common.util.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(MsdFacilityUpdateController.RESOURCE_PATH)
@RequiredArgsConstructor
@Validated
public class MsdFacilityUpdateController extends BaseController {

  public static final String RESOURCE_PATH = ResourceNames.BASE_PATH + "/msd-facilities";

  private final MsdFacilityUpdateRepository msdFacilityUpdateRepository;
  private final FacilityReferenceDataService facilityReferenceDataService;

  /**
   * Sync new msd facility updates.
   *
   * @param facilityUpdateDtos a list of facility updates to be synced.
   * @param bindingResult      validation results of the msd facility updates.
   * @return
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public String syncFacilityUpdates(
      @RequestBody @Valid List<MsdFacilityUpdateDto> facilityUpdateDtos,
      BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MsdFacilityUpdateMessageKeys.ERROR_INVALID_PARAMS);
    }

    for (MsdFacilityUpdateDto facilityUpdateDto : facilityUpdateDtos) {
      Optional<MsdFacilityUpdate> existingFacilityUpdate = msdFacilityUpdateRepository
          .findOneByFacilityCode(facilityUpdateDto.getFacilityCode());

      MsdFacilityUpdate facilityUpdate = null;
      if (existingFacilityUpdate.isPresent()) {
        facilityUpdate = existingFacilityUpdate.get();
        UUID id = facilityUpdate.getId();

        facilityUpdate = MsdFacilityUpdate.newInstance(facilityUpdateDto);
        facilityUpdate.setId(id);
        facilityUpdate = msdFacilityUpdateRepository.save(facilityUpdate);
      } else {
        facilityUpdate = MsdFacilityUpdate.newInstance(facilityUpdateDto);
        facilityUpdate = msdFacilityUpdateRepository.save(facilityUpdate);
      }

      Optional<FacilityDto> existingFacilityDto = facilityReferenceDataService.findByCode(
          facilityUpdateDto.getFacilityCode()
      );

      if (existingFacilityDto.isPresent()) {
        FacilityDto facilityDto = existingFacilityDto.get();
        facilityDto.setName(facilityUpdateDto.getFacilityName());
        facilityDto.setActive(facilityUpdateDto.getStatus());
        facilityDto.setEnabled(facilityUpdateDto.getStatus());

        facilityReferenceDataService.updateFacility(facilityDto);
      }
    }

    return "Synced Successfully";
  }

  /**
   * Sync new msd facility updates.
   *
   * @param id id of the facility update to be marked as applied..
   * @return
   */
  @PutMapping("/{id}/applied")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public MsdFacilityUpdateDto markFacilityUpdateApplied(@PathVariable("id") UUID id) {
    MsdFacilityUpdate update = msdFacilityUpdateRepository.findById(id)
        .orElseThrow(() -> new NotFoundException(
                new Message(MsdFacilityUpdateMessageKeys.ERROR_NOT_FOUND)
            )
        );

    if (update.isApplied()) {
      throw new ValidationMessageException(
          new Message(MsdFacilityUpdateMessageKeys.ERROR_INVALID_PARAMS)
      );
    }

    update.setApplied(true);
    update = msdFacilityUpdateRepository.save(update);

    return MsdFacilityUpdateDto.newInstance(update);
  }

  /**
   * Retrieves all msd facility updates.
   *
   * @return a list of msd facility updates persisted.
   */
  @GetMapping()
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<MsdFacilityUpdateDto> fetchAllFacilityUpdates(Pageable pageable) {
    return msdFacilityUpdateRepository.findAll(pageable)
        .map(update -> MsdFacilityUpdateDto.newInstance(update));
  }

  /**
   * Retrieves all pending msd facility updates.
   *
   * @return a list of msd facility updates pending to be merged as facilities.
   */
  @GetMapping("/pending")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<MsdFacilityUpdateDto> fetchPendingFacilityUpdates(Pageable pageable) {

    return msdFacilityUpdateRepository.findAllByAppliedIsFalse(pageable)
        .map(update -> MsdFacilityUpdateDto.newInstance(update));
  }
}
