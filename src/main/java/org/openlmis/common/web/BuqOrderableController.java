/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import static org.openlmis.common.web.ResourceNames.BASE_PATH;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.openlmis.common.dto.BuqOrderableCategoryDto;
import org.openlmis.common.service.BuqOrderableService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(BuqOrderableController.RESOURCE_PATH)
@RequiredArgsConstructor
public class BuqOrderableController extends BaseController {

  public static final String RESOURCE_PATH = BASE_PATH + "/buqOrderables";

  private final BuqOrderableService buqOrderableService;

  /**
   * Retrieves all buq orderable categories with name similar to name parameter.
   *
   * @param queryParams name query parameter.
   * @param pageable    object used to encapsulate the pagination related values:
   *                    page, size and sort.
   * @return Page of  buq orderable categories matching query parameter.
   */
  @GetMapping("/categories")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<BuqOrderableCategoryDto> searchBuqOrderableCategories(
      @RequestParam(required = false, name = "name") Optional<String> queryParams,
      Pageable pageable) {

    return queryParams
        .map(nameFilter -> buqOrderableService.searchCategories(nameFilter, pageable))
        .orElseGet(() -> buqOrderableService.findAllCategories(pageable))
        .map(category -> BuqOrderableCategoryDto.newInstance(category));
  }
}
