/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import static org.openlmis.common.web.TzRequisitionTemplateController.RESOURCE_PATH;

import java.util.List;
import java.util.UUID;
import lombok.NoArgsConstructor;
import org.openlmis.common.domain.TzRequisitionTemplate;
import org.openlmis.common.service.requisition.TzRequisitionTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@NoArgsConstructor
@RequestMapping(RESOURCE_PATH)
public class TzRequisitionTemplateController extends BaseController {

  public static final String RESOURCE_PATH = API_PATH + "/tzRequisitionTemplates";

  @Autowired
  private TzRequisitionTemplateService tzRequisitionService;

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  ResponseEntity<List<TzRequisitionTemplate>> findAllTzRequisitionTemplates() {
    List<TzRequisitionTemplate> tzRequisitionTemplates = tzRequisitionService.findAll();
    return ResponseEntity.ok(tzRequisitionTemplates);
  }

  @GetMapping("/{id}")
  ResponseEntity<TzRequisitionTemplate> findOneTzRequisitionTemplate(
          @PathVariable UUID id) {
    TzRequisitionTemplate tzRequisitionTemplate = tzRequisitionService.findOne(id);
    return ResponseEntity.ok(tzRequisitionTemplate);
  }

  @PostMapping
  ResponseEntity<TzRequisitionTemplate> createTzRequisitionTemplate(
          @RequestBody TzRequisitionTemplate tzRequisitionTemplate) {
    TzRequisitionTemplate persistedTzRequisitionTemplate =
            tzRequisitionService.save(tzRequisitionTemplate);
    return ResponseEntity
            .status(HttpStatus.CREATED)
            .body(persistedTzRequisitionTemplate);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  void deleteTzRequisitionTemplate(@PathVariable UUID id) {
    tzRequisitionService.deleteById(id);
  }

  @PutMapping("/{id}")
  ResponseEntity<TzRequisitionTemplate> updateTzRequisitionTemplate(
          @PathVariable UUID id,
          @RequestBody TzRequisitionTemplate updatedTzRequisitionTemplate) {
    TzRequisitionTemplate persistedTzRequisitionTemplate =
            tzRequisitionService.update(id, updatedTzRequisitionTemplate);

    return ResponseEntity.ok(persistedTzRequisitionTemplate);
  }

  @GetMapping("/byRequisitionTemplate/{id}")
  ResponseEntity<TzRequisitionTemplate> findByRequisitionTemplate(
          @PathVariable UUID id) {
    TzRequisitionTemplate tzRequisitionTemplate =
            tzRequisitionService.findByRequisitionTemplateId(id);
    return ResponseEntity.ok(tzRequisitionTemplate);
  }
}
