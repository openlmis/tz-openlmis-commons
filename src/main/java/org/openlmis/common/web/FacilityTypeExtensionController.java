/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import static org.openlmis.common.web.FacilityTypeExtensionController.RESOURCE_PATH;

import java.util.List;
import java.util.UUID;
import org.openlmis.common.domain.FacilityTypeExtension;
import org.openlmis.common.service.FacilityTypeExtensionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Controller that exposes APIs for managing FacilityTypeExtension entities.
 */
@RestController
@RequestMapping(RESOURCE_PATH)
public class FacilityTypeExtensionController
    extends BaseController {

  public static final String RESOURCE_PATH = API_PATH + "/facility-type-extensions";

  private final FacilityTypeExtensionService service;

  @Autowired
  public FacilityTypeExtensionController(
      FacilityTypeExtensionService service) {
    this.service = service;
  }

  /**
   * Creates or updates a FacilityTypeExtension.
   *
   * @param extension The FacilityTypeExtension to save or update.
   * @return The saved or updated FacilityTypeExtension.
   */
  @PostMapping
  public ResponseEntity<FacilityTypeExtension> createOrUpdate(@RequestBody
                                                                FacilityTypeExtension
                                                                    extension) {
    FacilityTypeExtension savedExtension = service.save(extension);
    return ResponseEntity.ok(savedExtension);
  }

  /**
   * Retrieves all FacilityTypeExtensions by the given FacilityTypeId.
   *
   * @param facilityTypeId The FacilityTypeId to search by.
   * @return List of FacilityTypeExtensions matching the given FacilityTypeId.
   */
  @GetMapping("/{facilityTypeId}")
  public ResponseEntity<List<FacilityTypeExtension>> getByFacilityTypeId(
      @PathVariable UUID facilityTypeId) {
    List<FacilityTypeExtension> extensions = service
        .getByFacilityTypeId(facilityTypeId);
    return ResponseEntity.ok(extensions);
  }

  /**
   * Updates an existing FacilityTypeExtension by its ID.
   *
   * @param id The ID of the FacilityTypeExtension to update.
   * @param updatedExtension The new values for the extension.
   * @return The updated FacilityTypeExtension.
   */
  @PutMapping("/{id}")
  public ResponseEntity<FacilityTypeExtension> update(
         @PathVariable UUID id,
         @RequestBody FacilityTypeExtension updatedExtension) {
    FacilityTypeExtension extension = service
        .update(id, updatedExtension);
    return ResponseEntity.ok(extension);
  }

  /**
   * Retrieves all FacilityTypeExtensions.
   *
   * @return A list of all FacilityTypeExtensions.
   */
  @GetMapping
  public ResponseEntity<List<FacilityTypeExtension>> getAll() {
    List<FacilityTypeExtension> extensions = service.getAll();
    return ResponseEntity.ok(extensions);
  }

}
