/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.openlmis.common.dto.AbstractStockOutNotificationDto.MsdStockOutNotificationDto;
import org.openlmis.common.dto.AbstractStockOutNotificationDto.StockOutNotificationDto;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.i18n.StockOutNotificationMessageKeys;
import org.openlmis.common.service.StockOutNotificationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(StockOutNotificationController.RESOURCE_PATH)
@RequiredArgsConstructor
@Validated
public class StockOutNotificationController extends BaseController {

  public static final String RESOURCE_PATH = ResourceNames.BASE_PATH + "/stock-out-notifications";

  private final StockOutNotificationService stockOutNotificationService;

  /**
   * Submit stock out notification.
   *
   * @param notification submitted stock out notification.
   * @param bindingResult      validation results of the stock out notification.
   * @return
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public String submitStockOutNotification(
      @RequestBody @Valid MsdStockOutNotificationDto notification,
      BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(StockOutNotificationMessageKeys.ERROR_INVALID_PARAMS);
    }
    stockOutNotificationService.submit(notification);
    return "Submitted Successfully";
  }

  /**
   * Retrieves stock out notifications.
   *
   * @return a list of stock out notifications persisted.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<StockOutNotificationDto> findStockOutNotifications(
      @RequestParam(required = false) Optional<String> facilityNameQuery,
      @RequestParam(required = false) Optional<UUID> userId,
      Pageable pageable) {
    return stockOutNotificationService.findAllStockOutNotificationDtos(
        userId.orElse(null),
        facilityNameQuery.orElse(null),
        pageable
    );
  }

  /**
   * Retrieves stock out notification count.
   *
   * @param userId id of the user to fetch SO notification for.
   * @return a count of stock out notifications associated with the specified user.
   */
  @GetMapping("/count")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Long countStockOutNotifications(@RequestParam(required = true) UUID userId) {
    return stockOutNotificationService.countAll(userId);
  }
}
