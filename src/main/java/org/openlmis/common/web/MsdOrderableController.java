/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import java.util.List;
import java.util.stream.Collectors;
import org.openlmis.common.domain.MsdOrderable;
import org.openlmis.common.dto.referencedata.OrderableDto;
import org.openlmis.common.service.MsdOrderableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(MsdOrderableController.RESOURCE_PATH)
public class MsdOrderableController
    extends BaseController {

  public static final String RESOURCE_PATH = API_PATH + "/msd-orderables";

  @Autowired
  private MsdOrderableService service;

  /**
   * save all MsdOrderable orderables.
   *
   * @return Orderables saved successiful
   */
  @PostMapping("save")
  public ResponseEntity<String> saveOrderables(@RequestBody List<MsdOrderable> orderables) {

    try {

      List<MsdOrderable> msdOrderables = orderables.stream()
          .peek(orderable -> orderable.setProcessed(false))
          .collect(Collectors.toList());

      List<MsdOrderable> savedOrderables = service.saveAll(msdOrderables);
      return ResponseEntity.ok("Orderables saved successfully: "
          + savedOrderables.size());
    } catch (IllegalArgumentException e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }

  }

  /**
   * Retrieves all MsdOrderable entities.
   *
   * @return List of all MsdOrderable entities
   */
  @GetMapping("/all")
  public List<MsdOrderable> getAllOrderables() {

    List<MsdOrderable> allOrderables = service.getAllOrderables();

    return allOrderables.stream()
        .filter(orderable -> Boolean.FALSE
            .equals(orderable.getProcessed()))
        .collect(Collectors.toList());
  }

  /**
   * Retrieves an MsdOrderable entity by its ID.
   *
   * @param id The ID of the MsdOrderable entity to retrieve
   * @return The MsdOrderable entity with the given ID
   */
  @GetMapping("/{id}")
  public MsdOrderable getOrderableById(@PathVariable Long id) {
    return service.getOrderableById(id);
  }

  /**
   * Retrieves an MsdOrderable entity by its code.
   *
   * @param code The code of the MsdOrderable entity to retrieve
   * @return The MsdOrderable entity with the given code
   */
  @GetMapping("/code/{code}")
  public MsdOrderable getOrderableByCode(@PathVariable String code) {
    return service.getOrderableByCode(code);
  }

  /**
   * Accepts and saves an OrderableDto.
   */
  @PostMapping("/acceptOrderable")
  public ResponseEntity<OrderableDto> acceptOrderable(
      @RequestBody OrderableDto code) {

    OrderableDto savedOrderable = service
        .postOrderable(code.getProductCode());
    return ResponseEntity.ok(savedOrderable);
  }

  /**
   * Delete msd orderable specified code.
   *
   * @param code code to be deleted.
   */
  @ResponseStatus(HttpStatus.OK)
  @DeleteMapping(value = "/delete/{code}")
  public void deleteMsdOrderable(
      @PathVariable("code") String code) {
    service.deleteMsdOrderableByCode(code);
  }

}
