/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import java.util.List;
import java.util.UUID;
import lombok.NoArgsConstructor;
import org.openlmis.common.domain.FacilityTypeExtension;
import org.openlmis.common.dto.RequisitionApprovalFinalResponseDto;
import org.openlmis.common.dto.referencedata.SupervisoryNodeDto;
import org.openlmis.common.dto.referencedata.UserDto;
import org.openlmis.common.dto.requisition.RequisitionDto;
import org.openlmis.common.exception.NotFoundException;
import org.openlmis.common.i18n.MessageKeys;
import org.openlmis.common.service.FacilityTypeExtensionService;
import org.openlmis.common.service.referencedata.SupervisoryNodeReferenceDataService;
import org.openlmis.common.service.requisition.RequisitionApprovalService;
import org.openlmis.common.service.requisition.RequisitionService;
import org.openlmis.common.service.requisition.RequisitionServiceExtension;
import org.openlmis.common.util.AuthenticationHelper;
import org.openlmis.common.util.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@NoArgsConstructor
@RequestMapping(TzRequisitionController.RESOURCE_PATH2)
public class TzRequisitionController extends BaseController {

  private static final Logger LOGGER = LoggerFactory.getLogger(TemplateController.class);

  public static final String RESOURCE_PATH2 = API_PATH + "/tzRequisitions";

  @Autowired
  private RequisitionService requisitionService;

  @Autowired
  private RequisitionApprovalService requisitionApprovalService;

  @Autowired
  private SupervisoryNodeReferenceDataService supervisoryNodeReferenceDataService;

  @Autowired
  AuthenticationHelper authenticationHelper;

  @Autowired
  private FacilityTypeExtensionService extensionService;

  @Autowired
  private RequisitionServiceExtension requisitionServiceExtension;

  /**
   * Approve specified by id requisition.
   */
  @PostMapping("/{id}/approve")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public RequisitionApprovalFinalResponseDto approveRequisition(
      @PathVariable("id") UUID requisitionId) {
    LOGGER.debug("Approve requisition");

    UserDto userDto = authenticationHelper.getCurrentUser();

    RequisitionDto requisition = requisitionService.findOne(requisitionId);
    System.out.println(requisition.getSupervisoryNode());

    SupervisoryNodeDto supervisoryNode = supervisoryNodeReferenceDataService
        .findSupervisoryNode(requisition.getProgram().getId(),
            requisition.getFacility().getId());
    LOGGER.debug("Get supervisory nodes and update");

    if (supervisoryNode != null) {
      LOGGER.debug("Get parent for supplying facility");

      SupervisoryNodeDto supervisoryNodeP = supervisoryNodeReferenceDataService
          .findOne(supervisoryNode.getParentNode().getId());

      SupervisoryNodeDto supervisoryNodePP;

      if (supervisoryNodeP.getParentNode() == null) {

        supervisoryNodePP = supervisoryNodeReferenceDataService
            .findOne(supervisoryNodeP.getId());

      } else {
        supervisoryNodePP = supervisoryNodeReferenceDataService
            .findOne(supervisoryNodeP.getParentNode().getId());
      }

      UUID supplyingFacilityId = supervisoryNodePP.getFacility().getId();

      requisitionService.finalApproval(requisition, supervisoryNode, userDto,
          supplyingFacilityId, supervisoryNodePP.getId());
    } else {
      throw new NotFoundException(
          new Message(MessageKeys.ERROR_ORDER_NOT_FOUND, requisitionId));
    }

    LOGGER.debug("={}", supervisoryNode);
    RequisitionApprovalFinalResponseDto responseDto
        = new RequisitionApprovalFinalResponseDto();
    responseDto.setRequisitionId(requisitionId);
    responseDto.setSupervisoryNodeId(supervisoryNode.getParentNode().getId());

    return responseDto;

  }

  /**
   * Approve specified by id requisition.
   */
  @PostMapping("/{id}/approveAndReleaseOrder")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public RequisitionApprovalFinalResponseDto approveAndReleaseOrder(
      @PathVariable("id") UUID requisitionId) {

    UUID currentUserToken = authenticationHelper.getCurrentUserTokenValue();
    LOGGER.info("get requisition details");
    RequisitionDto dto = requisitionApprovalService.findOne(requisitionId);
    UUID facilityTypeId = dto.getFacility().getType().getId();
    LOGGER.info("get facility type details = {}", facilityTypeId);
    List<FacilityTypeExtension> extension = extensionService
        .getByFacilityTypeId(facilityTypeId);
    LOGGER.info("get facility type details = {}", facilityTypeId);
    if (!extension.isEmpty() && (extension
        .get(0).getReleaseOrderAfterAuthorization())) {

      LOGGER.info("Used for RRH approval : {}", extension.get(0)
          .getReleaseOrderAfterAuthorization());
      //authorize
      dto = requisitionApprovalService.postRequisition(requisitionId,
          currentUserToken, "/authorize");
    }

    UserDto userDto = authenticationHelper.getCurrentUser();

    LOGGER.info("requisition dto: {}", dto);
    RequisitionDto dto2 = requisitionApprovalService.findOne(dto.getId());
    return this.requisitionApprovalService.approveRequisition(requisitionId, userDto,
        supervisoryNodeReferenceDataService, dto2, requisitionService);

  }

  /**
   * Delete specified by id requisition.
   */
  @DeleteMapping("/{id}/delete")
  public ResponseEntity<String> deleteRequisition(@PathVariable UUID id) {
    requisitionServiceExtension.deleteRequisition(id);
    return ResponseEntity.ok("Requisition " + id + "records deleted successfully.");
  }

}
