/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.openlmis.common.domain.ProofOfDelivery;
import org.openlmis.common.dto.ProofOfDeliveryDto;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.i18n.PodMessageKeys;
import org.openlmis.common.service.ProofOfDeliveryService;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ProofOfDeliveryController.RESOURCE_PATH)
@RequiredArgsConstructor
@Validated
public class ProofOfDeliveryController
    extends BaseController {

  public static final String
      RESOURCE_PATH = ResourceNames.BASE_PATH + "/proof-of-deliveries";

  private final ProofOfDeliveryService service;

  /**
   * Submit proof of deliveries.
   *
   * @param pod submitted proof of deliveries.
   * @param bindingResult      validation results of proof of deliveries.
   * @return
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public String submit(
      @RequestBody @Valid ProofOfDeliveryDto pod,
      BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(PodMessageKeys.ERROR_INVALID_PARAMS);
    }
    service.createOrUpdate(ProofOfDelivery.fromDto(pod));
    return "Submitted Successfully";
  }

}
