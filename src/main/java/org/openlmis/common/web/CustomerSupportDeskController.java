/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import javax.transaction.Transactional;
import lombok.NoArgsConstructor;
import org.hibernate.exception.DataException;
import org.openlmis.common.dto.referencedata.UserDto;
import org.openlmis.common.dto.supportdesk.SdIssue;
import org.openlmis.common.service.supportdesk.RestSdService;
import org.openlmis.common.util.AuthenticationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.slf4j.profiler.Profiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@NoArgsConstructor
@Transactional
@RequestMapping(value = "/api/support-desk")
public class CustomerSupportDeskController extends BaseController {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  private final XLogger extLogger = XLoggerFactory.getXLogger(getClass());

  @Autowired
  RestSdService restSdService;

  @Autowired
  AuthenticationHelper authenticationHelper;

  /**
   * Retrieve articles by search keyword.
   *
   * @param query String of searched key.
   *
   * @return searched articles.
   */
  @ResponseBody
  @GetMapping(value = "/getArticlesBySearchKeyword")
  @ResponseStatus(HttpStatus.OK)
  public String getArticlesBySearchKeyword(@RequestParam String query) {
    try {

      return restSdService.getArticlesBySearchKeyword(query);

    } catch (DataException de) {
      logger.debug("error in retrieval ={}", de.toString());
      return new String("error in retrieval " + de.getSQLException());
    }
  }

  /**
   * report an issue.
   *
   * @param summary     String of content to be reported.
   * @param description String of content to be reported.
   *
   * @return content.
   */
  @ResponseBody
  @RequestMapping(value = "/reportIssueOnSd", method = GET)
  public String reportIssueOnSD(@RequestParam String summary,
                                @RequestParam String description,
                                @RequestParam String impact,
                                @RequestParam String priority,
                                @RequestParam String type) {
    try {

      Profiler profiler = getProfiler("REPORT_ISSUE_ON_SD");

      profiler.start("GET_REPORT_USERS");

      UserDto user = getCurrentUser(profiler);

      profiler.start("ANALYSE_QUERY_SUMMARY");
      SdIssue sdIssue = new SdIssue();
      sdIssue.setRaiseOnBehalfOf(user.getEmail());
      sdIssue.setSummary(summary);
      sdIssue.setDescription(description);
      sdIssue.setImpact(impact);
      sdIssue.setPriority(priority);
      sdIssue.setType(type);

      stopProfiler(profiler, sdIssue);

      return restSdService.createIssue(sdIssue);

    } catch (DataException de) {

      return new String("error in retrieval " + de.getSQLException());

    }
  }

  Profiler getProfiler(String name, Object... entryArgs) {
    extLogger.entry(entryArgs);

    Profiler profiler = new Profiler(name);
    profiler.setLogger(extLogger);

    return profiler;
  }

  private void stopProfiler(Profiler profiler, Object... exitArgs) {
    profiler.stop().log();
    extLogger.exit(exitArgs);
  }

  private UserDto getCurrentUser(Profiler profiler) {
    profiler.start("GET_CURRENT_USER");
    return authenticationHelper.getCurrentUser();
  }

}
