/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import static java.util.stream.Collectors.toList;
import static org.openlmis.common.web.ProcessingPeriodController.RESOURCE_PATH;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.openlmis.common.domain.ProcessingPeriod;
import org.openlmis.common.dto.referencedata.ProcessingPeriodDto;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.service.ProcessingPeriodService;
import org.openlmis.common.util.Message;
import org.openlmis.common.validate.ProcessingPeriodValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.profiler.Profiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequiredArgsConstructor
@RequestMapping(RESOURCE_PATH)
public class ProcessingPeriodController extends BaseController {
  private static final Logger LOGGER = LoggerFactory.getLogger(ProcessingPeriodController.class);

  public static final String RESOURCE_PATH = BaseController.API_PATH + "/tzProcessingPeriods";

  private final ProcessingPeriodService processingPeriodService;

  @Autowired
  private ProcessingPeriodValidator validator;

  /**
   * Delete processing period of specified id.
   *
   * @param periodId id of the processing period to be deleted.
   */

  @ResponseStatus(HttpStatus.OK)
  @DeleteMapping(value = "/{id}")
  public void deleteProcessingPeriod(@PathVariable("id") UUID periodId) {
    processingPeriodService.deleteProcessingPeriod(periodId);
  }

  /**
   * Delete program supported of specified id.
   *
   * @param program  id of the program supported to be deleted.
   * @param facility id of the programSupported to be deleted.
   */

  @ResponseStatus(HttpStatus.OK)
  @DeleteMapping(value = "/{program}/{facility}/supportedPrograms")
  public void deleteProgramSupported(@PathVariable("program") UUID program,
                                     @PathVariable("facility") UUID facility) {
    processingPeriodService.deleteProgramSupported(program, facility);
  }

  /**
   * Create a new processing period using the provided processing period DTO.
   *
   * @param periodDto     processing period DTO with which to create the processing period
   * @param bindingResult Object used for validation.
   * @return the new processing period.
   */
  @RequestMapping(value = "/create", method = RequestMethod.POST)
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public ProcessingPeriodDto createProcessingPeriod(
      @RequestBody ProcessingPeriodDto periodDto, BindingResult bindingResult) throws IOException {
    ProcessingPeriod newPeriod = ProcessingPeriod.newPeriod(periodDto);
    LOGGER.debug("Creating new processingPeriod");
    validator.validate(newPeriod, bindingResult);
    if (bindingResult.getErrorCount() == 0) {
      processingPeriodService.saveProcessingPeriod(newPeriod);

      return exportToDto(newPeriod);
    } else {
      ObjectError error = bindingResult.getAllErrors().get(0);
      throw new ValidationMessageException(new Message(error.getCode(), error.getArguments()));
    }
  }

  private ProcessingPeriodDto exportToDto(ProcessingPeriod period) {
    ProcessingPeriodDto periodDto = new ProcessingPeriodDto();
    period.export(periodDto);
    return periodDto;
  }

  private Page<ProcessingPeriodDto> exportToDto(Page<ProcessingPeriod> periods, Profiler profiler,
                                                Pageable pageable) {
    List<ProcessingPeriodDto> dtos = periods.getContent()
        .stream()
        .map(this::exportToDto)
        .collect(toList());
    return toPage(dtos, pageable, periods.getTotalElements(), profiler);
  }

}
