/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import java.util.UUID;
import org.openlmis.common.domain.JasperTemplate;
import org.openlmis.common.dto.JasperTemplateDto;
import org.openlmis.common.dto.referencedata.TemplateDto;
import org.openlmis.common.exception.ReportingException;
import org.openlmis.common.repository.JasperTemplateRepository;
import org.openlmis.common.service.JasperTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

@Controller
@Transactional
@RequestMapping("/api/reports/templates/common")
public class TemplateController extends BaseController {

  private static final Logger LOGGER = LoggerFactory.getLogger(TemplateController.class);

  private static final String CONSISTENCY_REPORT = "Consistency Report";

  @Autowired
  private JasperTemplateService templateService;

  @Autowired
  private JasperTemplateRepository templateRepository;

  /**
   * Adding report templates with ".jrxml" format to database.
   *
   * @param file        File in ".jrxml" format to upload
   * @param name        Name of file in database
   * @param description Description of the file
   */
  @RequestMapping(method = RequestMethod.POST)
  @ResponseStatus(HttpStatus.OK)
  public void createJasperReportTemplate(@RequestPart("file") MultipartFile file,
                                         String name, String description)
      throws ReportingException {
    JasperTemplate template = new JasperTemplate(name, null,
        CONSISTENCY_REPORT, true, description, null, null);
    templateService.validateFileAndInsertTemplate(template, file);
  }

  /**
   * Get all templates.
   *
   * @return Templates.
   */
  @RequestMapping(method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<Iterable<TemplateDto>> getAllTemplates() {
    Iterable<JasperTemplateDto> templates = JasperTemplateDto
        .newInstance(templateRepository.findAll());
    return new ResponseEntity(templates, HttpStatus.OK);
  }

  /**
   * Allows updating templates.
   *
   * @param templateDto A template bound to the request body
   * @param templateId  UUID of template which we want to update
   *
   * @return ResponseEntity containing the updated template
   */
  @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
  public ResponseEntity<JasperTemplateDto> updateTemplate(
      @RequestBody JasperTemplateDto templateDto,
      @PathVariable("id") UUID templateId) {
    JasperTemplate template = JasperTemplate.newInstance(templateDto);
    JasperTemplate templateToUpdate = templateRepository
        .findById(templateId).orElse(null);
    if (templateToUpdate == null) {
      templateToUpdate = new JasperTemplate();
      LOGGER.info("Creating new template");
    } else {
      LOGGER.debug("Updating template with id: {}", templateId);
    }

    templateToUpdate.updateFrom(template);
    templateToUpdate = templateRepository.save(templateToUpdate);

    LOGGER.debug("Saved template with id: {}", templateToUpdate.getId());
    return new ResponseEntity<>(
        JasperTemplateDto.newInstance(templateToUpdate), HttpStatus.OK);
  }

  /**
   * Get chosen template.
   *
   * @param templateId UUID of template which we want to get
   *
   * @return Template.
   */
  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public ResponseEntity<TemplateDto> getTemplate(@PathVariable("id") UUID templateId) {
    JasperTemplate template = templateRepository.findById(templateId).orElse(null);
    if (template == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } else {
      return new ResponseEntity(JasperTemplateDto.newInstance(template), HttpStatus.OK);
    }
  }

  /**
   * Allows deleting template.
   *
   * @param templateId UUID of template which we want to delete
   *
   * @return ResponseEntity containing the HTTP Status
   */
  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<JasperTemplateDto> deleteTemplate(@PathVariable("id")
                                                              UUID templateId) {
    JasperTemplate template = templateRepository.findById(templateId).orElse(null);
    if (template == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } else {
      templateRepository.delete(template);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
  }

}