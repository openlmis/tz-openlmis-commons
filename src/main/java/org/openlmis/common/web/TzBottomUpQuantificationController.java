/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;
import org.openlmis.common.dto.buq.BottomUpQuantificationDto;
import org.openlmis.common.exception.NotFoundException;
import org.openlmis.common.service.buq.BottomUpQuantificationService;
import org.openlmis.common.service.buq.BuqReferenceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Controller used to expose Bottom-Up Quantifications via HTTP.
 */
@Controller
@RequestMapping(TzBottomUpQuantificationController.RESOURCE_PATH)
@Transactional
public class TzBottomUpQuantificationController
    extends BaseController {

  public static final String RESOURCE_PATH = API_PATH + "/tzBottomUpQuantifications";
  public static final String TEXT_CSV_MEDIA_TYPE = "text/csv";
  public static final String BUQ_FORM_CSV_FILENAME = "buq_quantification_forecasting_report";

  @Autowired
  private BuqReferenceDataService buqReferenceDataService;

  @Autowired
  private BottomUpQuantificationService bottomUpQuantificationService;

  /**
   * Allows downloading csv file.
   *
   * @return bytes containing bottom-up quantification data in csv format.
   * @throws IOException I/O exception
   */
  @GetMapping("/{id}/download")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<byte[]> download(@PathVariable("id") UUID bottomUpQuantificationId)
      throws IOException {

    Optional<BottomUpQuantificationDto> optionalBuq = Optional
        .ofNullable(buqReferenceDataService.findOne(bottomUpQuantificationId));
    BottomUpQuantificationDto buq;

    if (optionalBuq.isPresent()) {
      buq = optionalBuq.get();
    } else {
      throw new NotFoundException("Bottom up quantification not found");
    }
    return ResponseEntity.ok()
        .contentType(MediaType.valueOf(TEXT_CSV_MEDIA_TYPE))
        .header(HttpHeaders.CONTENT_DISPOSITION,
            "attachment;filename=" + BUQ_FORM_CSV_FILENAME + ".csv")
        .body(bottomUpQuantificationService.getPreparationFormData(buq));
  }

}