/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.util;

import java.io.IOException;
import java.io.Reader;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.ObjectUtils;

public class CsvUtils {
  public static final String ERROR_EMPTY_CSV =
      "The file does not contain any items information to be imported";
  public static final String ERROR_INCORRECT_CSV_FORMAT =
      "The file does not contain the correct format to be imported";

  public static final CSVFormat CSV_FORMAT = CSVFormat.DEFAULT;
  public static final String MIME_TYPE_CSV = "text/csv";

  private CsvUtils() {
  }

  public static List<Map<String, String>> readToMapWithFirstRecordAsKeys(Reader csvInput)
      throws IOException {
    CSVFormat format = CSV_FORMAT.withFirstRecordAsHeader();
    return read(format, csvInput);
  }

  /**
   * Read CSV stream of data to a list of maps.
   *
   * @param csvInput a CSV data stream read.
   * @return a list of map representing a row of data.
   */
  public static List<Map<String, String>> readToMapWithFirstRecordAsKeys(
      Reader csvInput, Map<String, String> resultKeysMapper) throws IOException {
    return readToMapWithFirstRecordAsKeys(csvInput, resultKeysMapper, true);
  }

  /**
   * Read CSV stream of data to a list of maps.
   *
   * @param csvInput         a CSV data stream read.
   * @param resultKeysMapper a mapping of column in the CSV versus the keys to be used in the Maps
   * @return a list of map representing a row of data.
   */
  public static List<Map<String, String>> readToMapWithFirstRecordAsKeys(
      Reader csvInput, Map<String, String> resultKeysMapper, boolean validate) throws IOException {
    List<Map<String, String>> data = readToMapWithFirstRecordAsKeys(csvInput);
    data.forEach(entry -> CollectionUtils.changeKeys(entry, resultKeysMapper));

    if (validate) {
      //throws an exception if the data imported does not match the expected keys
      CsvUtils.validate(data, resultKeysMapper.values());
    }

    return data;
  }

  /**
   * Read CSV stream of data to a list of maps.
   *
   * @param csvInput        a CSV data stream read.
   * @param headers         the names of the headers of the CSV
   * @param skipFirstRecord should the first row skipped
   * @return a list of map representing a row of data.
   */
  public static List<Map<String, String>> readToMapsWithKeys(
      Reader csvInput, List<String> headers, boolean skipFirstRecord) throws IOException {
    CSVFormat format = CSV_FORMAT.withHeader(headers.stream().toArray(value -> new String[value]));
    if (skipFirstRecord) {
      format = format.withSkipHeaderRecord();
    }

    return read(format, csvInput);
  }

  /**
   * Read CSV stream of data to a list of maps.
   *
   * @param csvInput  a CSV data stream read.
   * @param csvFormat format of the CSV to be read.
   * @return a list of map representing a row of data.
   */
  public static List<Map<String, String>> read(
      CSVFormat csvFormat, Reader csvInput) throws IOException {
    CSVParser records = csvFormat.parse(csvInput);
    return read(records);
  }

  /**
   * Read CSV records to a list of maps.
   *
   * @param records to be read.
   * @return a list of map representing a row of data.
   */
  public static List<Map<String, String>> read(Iterable<CSVRecord> records) {
    List<Map<String, String>> data = new LinkedList<Map<String, String>>();
    for (CSVRecord record : records) {
      data.add(record.toMap());
    }
    return data;
  }

  /**
   * Validate the data imported to ensure that is not empty and the headers are correct.
   *
   * @param data    the data to be validated
   * @param headers correct headers for the data
   * @throws IllegalArgumentException if the data is empty
   */
  public static void validate(Collection<Map<String, String>> data, Collection<String> headers) {
    //ensure not empty
    if (ObjectUtils.isEmpty(data)) {
      throw new IllegalArgumentException(ERROR_EMPTY_CSV);
    }

    //ensure the expected columns are all available in the data
    Map<String, String> candidate = data.stream().findFirst().get();
    Set<String> uploadedKeys = new HashSet<>(candidate.keySet());
    Set<String> expectedKeys = new HashSet<>(headers);
    if (!uploadedKeys.containsAll(expectedKeys)) {
      throw new IllegalArgumentException(ERROR_INCORRECT_CSV_FORMAT);
    }
  }
}
