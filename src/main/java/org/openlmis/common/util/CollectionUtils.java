/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.util;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import lombok.NonNull;

/**
 * Additional collection utils apart from the ones provided by apache commons and spring.
 */
public class CollectionUtils {
  /**
   * Prevent creating instances of this class.
   */
  private CollectionUtils() {
  }

  /**
   * Group similar item in the same list.
   * <br/>
   * For example a list [1,2,2,4,5,2,3,1] will create a list with entries:
   * <ul>
   *     <li>1->[1,1]</li>
   *     <li>2->[2,2,2]</li>
   *     <li>3->[3]</li>
   *     <li>4->[4]</li>
   *     <li>5->[5]</li>
   * </ul>
   */
  public static <E, I> Map<I, List<E>> groupSimilarItems(
      @NonNull Collection<E> items, @NonNull Function<E, I> similarItemsGroupIdentifier) {

    //group similar items into their own unique collections
    //hence creating a map with each entry containing similar items
    // where the key is the identifier common for all the entry values
    Map<I, List<E>> groups = items.stream()
        .collect(Collectors.groupingBy(similarItemsGroupIdentifier, Collectors.toList()));

    return groups;
  }

  /**
   * Replace in place the key and value stored in {@link LinkedHashMap} with a
   * new key and value specified while preserving the insertion order
   * of the {@link LinkedHashMap}.
   *
   * @param map      the {@link LinkedHashMap} to replace key and value with new key value
   * @param oldKey   the old key to be replaced
   * @param newKey   the newly replacing key
   * @param newValue the value to be associated with the new key
   * @param <K>      Key type
   * @param <V>      Value type
   * @see LinkedHashMap
   * @see LinkedHashMap#replace(Object, Object, Object)
   */
  public static <K, V> void replacePreserveOrder(
      @NonNull LinkedHashMap<K, V> map, K oldKey, K newKey, @NonNull V newValue) {
    LinkedHashMap<K, V> tmp = new LinkedHashMap<K, V>(map);

    map.clear();

    for (Map.Entry<K, V> e : tmp.entrySet()) {
      if (e.getKey().equals(oldKey)) {
        map.put(newKey, newValue);
      } else {
        map.put(e.getKey(), e.getValue());
      }
    }
  }

  /**
   * Modify in-place/same map by changing the old keys to
   * the new specified keys but keeping the values.
   *
   * @param map              map to change its keys
   * @param resultKeysMapper a mapping of old keys and new keys
   * @param <K>              the key type for both new and old ones
   * @param <V>              the value type
   */
  public static <K, V> void changeKeys(Map<K, V> map, Map<K, K> resultKeysMapper) {
    Set<K> keys = resultKeysMapper.keySet();
    for (K key : keys) {
      if (map.containsKey(key)) {
        V value = map.remove(key);
        K newKey = resultKeysMapper.get(key);
        map.put(newKey, value);
      }
    }
  }

  /**
   * Generate a new map with different keys but same values.
   *
   * @param map               a map to change keys
   * @param resultKeysMapper  mapping of the old key against the new
   *                          key to replace it in the specified map
   * @param newMapInitializer initilize a new map to be filled with the new keys and value
   * @param <K>               the old key type
   * @param <N>               the new key type
   * @param <V>               the value
   * @return a new map with different keys but same values
   */
  public static <K, N, V> Map<N, V> toDifferentKeysMap(
      Map<K, V> map, Map<K, N> resultKeysMapper, Supplier<Map<N, V>> newMapInitializer) {
    Set<K> keys = resultKeysMapper.keySet();
    Map<N, V> newMap = newMapInitializer.get();

    for (K key : keys) {
      if (map.containsKey(key)) {
        V value = map.get(key);
        N newKey = resultKeysMapper.get(key);
        newMap.put(newKey, value);
      }
    }

    return newMap;
  }

  /**
   * Find duplicate with the specified collection.
   *
   * @param entries           collection to check.
   * @param identityGenerator entry identity generator.
   * @param <E>               type of entry.
   * @param <I>               type of identity.
   * @return a map of identity against duplicate entries as values.
   */
  public static <E, I> Map<I, List<E>> findDuplicates(
      Collection<E> entries, Function<E, I> identityGenerator) {
    return groupSimilarItems(entries, identityGenerator)
        .entrySet()
        .stream()
        .filter(entry -> entry.getValue().size() > 1)
        .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()));
  }
}
