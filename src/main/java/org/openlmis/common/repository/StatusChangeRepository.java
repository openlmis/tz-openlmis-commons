/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.hibernate.Session;
import org.openlmis.common.domain.StatusChange;
import org.openlmis.common.dto.fulfillment.StatusMessage2Dto;
import org.openlmis.common.dto.requisition.RequisitionDto;
import org.springframework.stereotype.Repository;

@Repository
public class StatusChangeRepository {
  @PersistenceContext
  private EntityManager entityManager;

  /**
   * Verify that the status changes of the specified id exist.
   *
   * @param requisitionId id of requisition to verify.
   */
  public void verifyRequisitionExists(UUID requisitionId) {

    String existSql = "select count(id) from requisition.status_changes where id='"
        + requisitionId
        + "'";
    Object existing = entityManager.createNativeQuery(existSql).getSingleResult();
    if (Long.valueOf(existing.toString()) <= 0L) {
      throw new IllegalArgumentException("There is no requisition with id: " + requisitionId);
    }
  }

  /**
   * Save requisition object.
   *
   * @param requisition requisition to update requisition.
   */
  @Transactional
  public void updateRequisition(RequisitionDto requisition) {
    entityManager.createNativeQuery(
            "UPDATE requisition.requisitions "
                + "SET status = ?, "
                + "createdDate = ?, "
                + "modifiedDate = ?, "
                + "supervisoryNodeId = ?, "
                + "supplyingfacilityid = ? "
                + "WHERE id = ?")
        .setParameter(1, requisition.getApprovalStatus())
        .setParameter(2, LocalDateTime.now())
        .setParameter(3, LocalDateTime.now())
        .setParameter(4, requisition.getSupervisoryNode())
        .setParameter(5, requisition.getSupplyingFacilityId())
        .setParameter(6, requisition.getId())
          .executeUpdate();
  }

  /**
   * Save status change object.
   *
   * @param statusChange processingPeriod of status change to save periods.
   */
  @Transactional
  public void saveStatusChange(StatusChange statusChange) {

    entityManager.createNativeQuery(
            "INSERT INTO requisition.status_changes ("
                + "id, createdDate, modifiedDate, authorId, status, "
                + "requisitionid, supervisorynodeid)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?)")
        .setParameter(1, UUID.randomUUID())
        .setParameter(2, LocalDateTime.now())
        .setParameter(3, LocalDateTime.now())
        .setParameter(4, statusChange.getAuthorId())
        .setParameter(5, statusChange.getStatus())
        .setParameter(6, statusChange.getRequisitionId())
        .setParameter(7, statusChange.getSupervisoryNodeId())
        .executeUpdate();
  }

  private static final String SQL_QUERY = "SELECT "
      + "    Cast(gzz.id as varchar) id , "
      + "    Cast(gzz.requisitionId as varchar) requisitionId, "
      + "    Cast(gzz.statusChangeId as varchar) statusChangeId, "
      + "    Cast(gzz.authorId as varchar) authorId, "
      + "    gzz.status, "
      + "    gzz.body, "
      + "    gzz.authorFirstName, "
      + "    gzz.authorLastName "
      + " FROM requisition.status_messages gzz "
      + " WHERE gzz.requisitionId = :requisitionId ";

  /**
   * Retrieves a list of status messages for a given requisition ID.
   *
   * @param requisitionId the UUID representing the requisition ID
   * @return a list of StatusMessage2Dto objects.
   */
  @Transactional
  public List<StatusMessage2Dto> findByRequisitionId(UUID requisitionId) {

    try (Session session = entityManager.unwrap(Session.class)) {
      Query query = session.createNativeQuery(SQL_QUERY);
      setQueryParameters(query, requisitionId);
      List<Object[]> resultList = query.getResultList();
      return mapResultListToDtoList(resultList);
    }

  }

  /**
   * Sets the parameter "requisitionId" for the given query.
   *
   * @param query        the query to set the parameter for
   * @param requisitionId the UUID representing the requisition ID
   */
  private void setQueryParameters(Query query, UUID requisitionId) {
    query.setParameter("requisitionId", requisitionId);
  }

  private List<StatusMessage2Dto>
      mapResultListToDtoList(List<Object[]> resultList) {
    List<StatusMessage2Dto> statusMessageDtos = new ArrayList<>();

    for (Object[] row : resultList) {
      StatusMessage2Dto
          statusMessageDto = new StatusMessage2Dto();
      statusMessageDto.setId((String) row[0]);
      statusMessageDto.setRequisitionId((String) row[1]);
      statusMessageDto.setStatusChangeId((String) row[2]);
      statusMessageDto.setAuthorId((String) row[3]);
      statusMessageDto.setStatus((String) row[4]);
      statusMessageDto.setBody((String) row[5]);
      statusMessageDto.setAuthorFirstName((String) row[6]);
      statusMessageDto.setAuthorLastName((String) row[7]);
      statusMessageDtos.add(statusMessageDto);
    }

    return statusMessageDtos;
  }

  /**
   * Verify that the status changes of the specified id exist.
   * @param requisitionId id of requisition to verify.
   * @param status status of requisition status to verify.
   * @return true if the status exists, false otherwise.
   */
  public boolean verifyRequisitionStatusExists(UUID requisitionId,
                                               String status) {
    if (requisitionId == null) {
      throw new IllegalArgumentException("Requisition ID cannot be null");
    }

    String existSql = "SELECT COUNT(CAST(id AS VARCHAR)) FROM requisition.status_changes WHERE "
        + "requisitionId = CAST(:requisitionId AS UUID) AND status = :status";

    Object result = entityManager.createNativeQuery(existSql)
        .setParameter("requisitionId", requisitionId.toString())
        .setParameter("status", status)
        .getSingleResult();

    Long existingCount = (result != null) ? ((Number) result).longValue() : 0L;

    return existingCount > 0L;
  }

}