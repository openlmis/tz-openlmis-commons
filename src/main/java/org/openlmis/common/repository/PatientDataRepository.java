/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.repository;

import java.util.Optional;
import java.util.UUID;
import org.openlmis.common.domain.PatientData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientDataRepository
    extends JpaRepository<PatientData, UUID> {
  Optional<PatientData> findByPeriodIdAndFacilityCode(
      @Param("periodId") String periodId,
      @Param("facilityCode") String facilityCode);

  @Query(value = "SELECT * FROM common.dhis_patients s "
      + " WHERE lower(s.facilityCode) = lower(:facilityCode) ORDER BY s.id DESC LIMIT 1",
      nativeQuery = true)
  Optional<PatientData> getLastFacilityDetails(@Param("facilityCode") String facilityCode);
}
