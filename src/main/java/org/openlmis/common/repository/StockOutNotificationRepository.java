/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.repository;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.openlmis.common.domain.StockOutNotification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StockOutNotificationRepository
    extends PagingAndSortingRepository<StockOutNotification, UUID> {

  @Query(value =
      "select cast(r.facilityid as varchar),"
          + "cast(r.programid as varchar),"
          + "cast(r.processingperiodid as varchar) "
          + "from fulfillment.orders r "
          + "where r.ordercode=?1",
      nativeQuery = true)
  Map<String, Object> findRequisitionInformation(String orderCode);

  Optional<StockOutNotification> findOneByElmisOrderNumber(String orderCode);

  @Query(value = "SELECT * FROM common.msd_stock_out_notifications sn "
      + "WHERE (COALESCE(?1) IS NULL OR sn.facilityid IN ?1) "
      + "AND (COALESCE(?2) IS NULL OR (LOWER(sn.customername) LIKE LOWER(CONCAT('%', ?2, '%')))) "
      + "ORDER BY ?#{#pageable}",
      countQuery = "SELECT COUNT(*) FROM common.msd_stock_out_notifications sn "
          + "WHERE (COALESCE(?1) IS NULL OR sn.facilityid IN ?1) AND "
          + "(COALESCE(?2) IS NULL OR (LOWER(sn.customername) LIKE LOWER(CONCAT('%', ?2, '%'))))",
      nativeQuery = true)
  Page<StockOutNotification> findAll(@Param("facilities") Set<String> facilityIds,
      @Param("facilityNameQuery") String facilityNameQuery,
      @Param("pageable") Pageable pageable);

  Long countByFacilityIdIn(Set<UUID> facilityIds);

  Page<StockOutNotification> findAllByFacilityIdInAndCustomerNameContainingIgnoreCase(
      Set<UUID> facilityIds,
      String facilityNameQuery,
      Pageable pageable);
}