/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.repository;

import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

@Repository
public class RequisitionRepository {

  @PersistenceContext
  private EntityManager entityManager;

  /**
   * Verify that the requisition is deleted of the specified id exist.
   *
   * @param requisitionId id of requisition to verify.
   */
  @Transactional
  public void deleteRequisitionAndAssociations(UUID requisitionId) {
    StoredProcedureQuery query = entityManager
        .createStoredProcedureQuery(
            "delete_requisition_and_associations");
    query.registerStoredProcedureParameter("requisitionId",
        UUID.class, ParameterMode.IN);
    query.setParameter("requisitionId", requisitionId);
    query.execute();
  }

}
