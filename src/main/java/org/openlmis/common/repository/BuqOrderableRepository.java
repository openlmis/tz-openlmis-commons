/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.repository;

import java.util.List;
import java.util.UUID;
import org.openlmis.common.domain.BuqOrderable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BuqOrderableRepository
    extends PagingAndSortingRepository<BuqOrderable, UUID> {

  Page<BuqOrderable> findAllByCategory_IdAndOrderableId(
      UUID categoryId, UUID orderableId, Pageable pageable
  );

  Page<BuqOrderable> findAllByCategory_Id(UUID categoryId, Pageable pageable);

  Page<BuqOrderable> findAllByOrderableId(UUID orderableId, Pageable pageable);

  List<BuqOrderable> findAllByOrderableId(UUID orderableId);

}