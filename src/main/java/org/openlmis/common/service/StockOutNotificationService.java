/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service;

import static java.util.stream.Collectors.toSet;
import static org.openlmis.common.service.PermissionService.REQUISITION_VIEW;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.beanutils.BeanUtils;
import org.openlmis.common.domain.StockOutNotification;
import org.openlmis.common.dto.AbstractStockOutNotificationDto.MsdStockOutNotificationDto;
import org.openlmis.common.dto.AbstractStockOutNotificationDto.StockOutNotificationDto;
import org.openlmis.common.dto.referencedata.PermissionStringDto;
import org.openlmis.common.dto.referencedata.UserDto;
import org.openlmis.common.exception.NotFoundException;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.i18n.StockOutNotificationMessageKeys;
import org.openlmis.common.repository.StockOutNotificationRepository;
import org.openlmis.common.service.referencedata.FacilityReferenceDataService;
import org.openlmis.common.service.referencedata.PeriodReferenceDataService;
import org.openlmis.common.service.referencedata.ProgramReferenceDataService;
import org.openlmis.common.service.referencedata.UserReferenceDataService;
import org.openlmis.common.util.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

@Service
@RequiredArgsConstructor
public class StockOutNotificationService {

  private final StockOutNotificationRepository stockOutNotificationRepository;
  private final PermissionService permissionService;
  private final FacilityReferenceDataService facilityReferenceDataService;
  private final ProgramReferenceDataService programReferenceDataService;
  private final PeriodReferenceDataService periodReferenceDataService;
  private final UserReferenceDataService userReferenceDataService;

  /**
   * Handle SO Notifications from MSD and persist them in the DB.
   *
   * @param msdNotificationDto notification data from MSD.
   * @return created SO notification record.
   */
  @Async
  @SneakyThrows
  @Transactional
  public StockOutNotification submit(MsdStockOutNotificationDto msdNotificationDto) {

    if (CollectionUtils.isEmpty(msdNotificationDto.getCloseToExpireItems())
        && CollectionUtils.isEmpty(msdNotificationDto.getFullFilledItems())
        && CollectionUtils.isEmpty(msdNotificationDto.getInSufficientFundingItems())
        && CollectionUtils.isEmpty(msdNotificationDto.getStockOutItems())
        && CollectionUtils.isEmpty(msdNotificationDto.getPhasedOutItems())
        && CollectionUtils.isEmpty(msdNotificationDto.getRationingItems())) {
      throw new ValidationMessageException(StockOutNotificationMessageKeys.ERROR_INVALID_PARAMS);
    }

    if (msdNotificationDto.getProcessingDate() != null
        && msdNotificationDto.getNotificationDate() == null) {
      msdNotificationDto.setNotificationDate(
          msdNotificationDto.getProcessingDate());
    }

    String orderNumber = msdNotificationDto.getElmisOrderNumber();
    Optional<StockOutNotification> existingNotif = stockOutNotificationRepository
        .findOneByElmisOrderNumber(orderNumber);

    StockOutNotification notification;
    if (existingNotif.isPresent()) {
      notification = existingNotif.get();
      BeanUtils.copyProperties(notification, msdNotificationDto);
    } else {
      notification = new StockOutNotification();
      BeanUtils.copyProperties(notification, msdNotificationDto);

      Map<String, Object> req = stockOutNotificationRepository.findRequisitionInformation(
          orderNumber
      );

      notification.setFacilityId(UUID.fromString((String) req.get("facilityid")));
      notification.setProgramId(UUID.fromString((String) req.get("programid")));
      notification.setProcessingPeriodId(UUID.fromString((String) req.get("processingperiodid")));
    }

    notification = stockOutNotificationRepository.save(notification);
    //todo send email about the msdNotificationDto

    return notification;
  }

  /**
   * Find all SO notification records based on the provided filters.
   *
   * @param userId            id of the user associated with the SO notification.
   * @param facilityNameQuery a facility name filter token to search.
   * @param pageable          pagination information
   * @return a page of found SO notifications.
   */
  public Page<StockOutNotification> findAllStockOutNotifications(UUID userId,
      String facilityNameQuery,
      Pageable pageable) {

    Set<UUID> facilityIds = findAssociatedFacilityIds(userId);

    if (facilityNameQuery == null) {
      facilityNameQuery = "";
    }

    return stockOutNotificationRepository.findAllByFacilityIdInAndCustomerNameContainingIgnoreCase(
        facilityIds, facilityNameQuery, pageable);
  }

  /**
   * Find all SO notification records with facility, program and processing period based on the
   * provided filters.
   *
   * @param userId            id of the user associated with the SO notification.
   * @param facilityNameQuery a facility name filter token to search.
   * @param pageable          pagination information
   * @return a page of found SO notifications.
   */
  @SneakyThrows
  public Page<StockOutNotificationDto> findAllStockOutNotificationDtos(UUID userId,
      String facilityNameQuery,
      Pageable pageable) {
    return findAllStockOutNotifications(userId, facilityNameQuery, pageable)
        .map(notif -> toStockOutNotificationDto(notif));
  }

  @SneakyThrows
  private StockOutNotificationDto toStockOutNotificationDto(StockOutNotification notif) {
    StockOutNotificationDto notificationDto = new StockOutNotificationDto();
    BeanUtils.copyProperties(notificationDto, notif);
    notificationDto.setFacility(facilityReferenceDataService.findOne(notif.getFacilityId()));
    notificationDto.setProgram(programReferenceDataService.findOne(notif.getProgramId()));
    notificationDto.setProcessingPeriod(
        periodReferenceDataService.findOne(notif.getProcessingPeriodId()));

    return notificationDto;
  }

  private Set<UUID> findAssociatedFacilityIds(UUID userId) {
    Set<UUID> facilityIds = new LinkedHashSet<>();
    if (userId != null) {
      UserDto userDto = userReferenceDataService.findOne(userId);
      UUID homeFacilityId = userDto.getHomeFacilityId();
      if (homeFacilityId != null) {
        facilityIds.add(homeFacilityId);
      } else {
        PermissionStrings.Handler handler = permissionService.getPermissionStrings(userId);
        Set<PermissionStringDto> permissionStrings = handler.get();
        return permissionStrings.stream()
            .filter(permission -> REQUISITION_VIEW.equals(permission.getRightName()))
            .map(PermissionStringDto::getFacilityId)
            .collect(toSet());
      }
    }
    return facilityIds;
  }

  /**
   * Count SO notification records associated with the specified user.
   *
   * @param userId ID of the user.
   * @return count of SO notification associated with the specified user.
   */
  public Long countAll(@NotNull UUID userId) {
    Set<UUID> facilityIds = findAssociatedFacilityIds(userId);
    return stockOutNotificationRepository.countByFacilityIdIn(facilityIds);
  }

  /**
   * Get SO notification with specified id.
   *
   * @param id id of the SO notification
   * @return stock out notification.
   */
  public StockOutNotificationDto getStockOutNotificationDto(UUID id) {
    StockOutNotification notification = stockOutNotificationRepository.findById(id)
        .orElseThrow(() -> new NotFoundException(
                new Message(StockOutNotificationMessageKeys.ERROR_NOT_FOUND)
            )
        );
    return toStockOutNotificationDto(notification);
  }
}
