/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service;

import static org.openlmis.common.i18n.MessageKeys.DUPLICATED;
import static org.openlmis.common.i18n.MessageKeys.MUST_CONTAIN_VALUE;
import static org.openlmis.common.i18n.MessageKeys.NOT_FOUND;
import static org.openlmis.common.i18n.MsdDailyStockStatusMessageKeys.ERROR_ID_MISMATCH;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import org.openlmis.common.domain.ApplicationInterface;
import org.openlmis.common.domain.InterfaceDataSet;
import org.openlmis.common.dto.ApplicationInterfaceDto;
import org.openlmis.common.exception.ContentNotFoundMessageException;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.repository.ApplicationInterfaceRepository;
import org.openlmis.common.util.Message;
import org.openlmis.common.validate.ApplicationInterfaceValidator;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

@SuppressWarnings("PMD.TooManyMethods")
@Service
public class ApplicationInterfaceService {
  private final XLogger extLogger = XLoggerFactory.getXLogger(getClass());

  @Autowired
  private ApplicationInterfaceRepository repository;

  @Autowired
  private ApplicationInterfaceValidator validator;

  @Autowired
  private ApplicationInterfaceBuilder interfaceBuilder;

  /**
   * Submits a bottomUpQuantification.
   *
   * @param applicationInterfaceDto DTO of bottomUpQuantification
   * @param id                      id of bottomUpQuantification
   * @param bindingResult           bindingResult of bottomUpQuantification
   * @return Bottom-up quantification dto with new data.
   */
  public ApplicationInterfaceDto submit(
      ApplicationInterfaceDto applicationInterfaceDto, UUID id, BindingResult bindingResult) {
    validator.validate(applicationInterfaceDto, bindingResult);

    ApplicationInterface applicationInterface = save(applicationInterfaceDto, id);
    return interfaceBuilder
        .buildDto(applicationInterface);
  }

  /**
   * Saves new data for application interface.
   *
   * @param applicationInterfaceImporter DTO containing new data.
   * @param id                           ID of the application interface to be saved.
   * @return Application Interface with new data.
   */
  public ApplicationInterface save(ApplicationInterfaceDto applicationInterfaceImporter,
                                   UUID id) {
    ApplicationInterface updated =
        updateApplicationInterface(applicationInterfaceImporter, id);
    repository.save(updated);

    return updated;
  }

  private ApplicationInterface updateApplicationInterface(

      ApplicationInterfaceDto applicationInterfaceDto,
      UUID id) {
    if (null != applicationInterfaceDto.getId()
        && !Objects.equals(applicationInterfaceDto.getId(), id)) {
      throw new ValidationMessageException(ERROR_ID_MISMATCH);
    }

    ApplicationInterface applicationInterfaceToUpdate =
        findApplicationInterface(id);

    List<InterfaceDataSet> updatedLineItems = applicationInterfaceDto
        .getDataSets()
        .stream()
        .map(lineItemDto -> {
          InterfaceDataSet lineItem = InterfaceDataSet
              .newInstance(lineItemDto);
          lineItem.setApplicationInterface(applicationInterfaceToUpdate);
          lineItem.setId(lineItemDto.getId());

          return lineItem;
        })
        .collect(Collectors.toList());
    applicationInterfaceToUpdate.updateFrom(updatedLineItems);

    return applicationInterfaceToUpdate;
  }

  /**
   * Finds and retrieves a ApplicationInterface entity by its unique identifier.
   *
   * @param id The UUID identifier of the ApplicationInterface to find.
   * @return The found ApplicationInterface entity.
   * @throws ContentNotFoundMessageException If the ApplicationInterface with the given ID is
   *                                         not found.
   */
  public ApplicationInterface findApplicationInterface(UUID id) {
    return repository
        .findById(id)
        .orElseThrow(() -> new ContentNotFoundMessageException(
            NOT_FOUND, id)
        );
  }

  /**
   * Save or update stock card line item reason.
   *
   * @param application reason DTO object
   * @return created reason DTO object
   */
  public ApplicationInterface saveOrUpdate(ApplicationInterface application) {
    validateRequiredValueNotNull(application);
    validateReasonNameDuplicate(application);
    extLogger.debug("Is going to save application interface");
    return repository.save(application);
  }

  /**
   * Check try to update reason to be duplicate with other one.
   * Throw exception without update itself.
   *
   * @param reason would be updated
   */
  private void validateReasonNameDuplicate(ApplicationInterface reason) {
    ApplicationInterface foundReason = repository.findByName(reason.getName());

    if (foundReason != null && !foundReason.getId().equals(reason.getId())) {
      throwException(DUPLICATED);
    }
  }

  private void validateRequiredValueNotNull(ApplicationInterface reason) {
    if (reason.getName().isEmpty()) {
      throwException(MUST_CONTAIN_VALUE);
    }
  }

  private void throwException(String errorKey) {
    throw new ValidationMessageException(new Message(errorKey));
  }

  /**
   * Check if the id would be updated reason exists.
   *
   * @param reasonId would be updated reason's ID
   */
  public void checkUpdateIdExists(UUID reasonId) {
    if (!repository.existsById(reasonId)) {
      throw new ValidationMessageException(new Message(NOT_FOUND));
    }
  }

}
