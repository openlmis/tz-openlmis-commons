/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.openlmis.common.domain.DhisOrderableConfig;
import org.openlmis.common.domain.MalariaValidation;
import org.openlmis.common.domain.PatientData;
import org.openlmis.common.domain.PatientDataLineItem;
import org.openlmis.common.dto.referencedata.FacilityDto;
import org.openlmis.common.dto.referencedata.OrderableDto;
import org.openlmis.common.repository.DhisOrderableConfigRepository;
import org.openlmis.common.repository.MalariaValidationRepository;
import org.openlmis.common.repository.PatientDataLineItemRepository;
import org.openlmis.common.repository.PatientDataRepository;
import org.openlmis.common.service.referencedata.OrderableReferenceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PatientDataService {

  @Autowired
  private PatientDataRepository patientDataRepository;

  @Autowired
  private PatientDataLineItemRepository lineItemRepository;

  @Autowired
  private MalariaValidationRepository malariaValidationRepository;

  @Autowired
  private DhisOrderableConfigRepository orderableConfigRepository;

  @Autowired
  private OrderableReferenceDataService orderableReferenceDataService;

  /**
   * Save or update stock card line item reason.
   *
   * @param patientDataList reason DTO object
   */
  public void saveOrUpdatePatientData(List<PatientData> patientDataList) {

    // Find the existing patient data by periodId and facilityCode

    for (PatientData newPatientData : patientDataList) {

      // Check if PatientData exists by periodId and facilityCode
      Optional<PatientData> existingPatientDataOptional = patientDataRepository
          .findByPeriodIdAndFacilityCode(newPatientData.getPeriodId(),
              newPatientData.getFacilityCode());

      if (existingPatientDataOptional.isPresent()) {
        // If it exists, delete existing LineItems
        PatientData existingPatientData = existingPatientDataOptional.get();
        System.out.println(existingPatientData.getId());
        lineItemRepository.deleteByPatientData(existingPatientData);

        // Update existing PatientData with new LineItems
        existingPatientData.getLineItems().addAll(newPatientData.getLineItems());

        for (PatientDataLineItem item : newPatientData.getLineItems()) {
          item.setPatientData(existingPatientData);
          lineItemRepository.save(item);
        }

      } else {
        // If it doesn't exist, save the new PatientData
        patientDataRepository.save(newPatientData);

        for (PatientDataLineItem item : newPatientData.getLineItems()) {
          item.setPatientData(newPatientData);
          lineItemRepository.save(item);
        }

      }

    }

  }

  /**
   * get malaria data for requisition.
   *
   * @param facilityInfo  facility DTO object
   * @param requisitionId requisitionId DTO object
   * @return
   */
  public void processMalariaDataForRequisition(
      FacilityDto facilityInfo, UUID requisitionId) {

    String hfrCode = facilityInfo.getExtraData()
        .getOrDefault("hfrCode", null);

    if (hfrCode == null) {
      hfrCode = facilityInfo.getCode();
    }

    //get By Facility Code
    Optional<PatientData> patientData = patientDataRepository
        .getLastFacilityDetails(hfrCode);

    if (patientData.isPresent()) {

      List<PatientDataLineItem> items = lineItemRepository
          .findAllByPatientData(patientData.get());

      Iterable<DhisOrderableConfig> orderableConfigList = orderableConfigRepository.findAll();

      MalariaValidation validation;

      malariaValidationRepository.deleteByRequisitionId(requisitionId);

      for (DhisOrderableConfig config: orderableConfigList) {

        //find orderable details
        OrderableDto orderableDto = orderableReferenceDataService.findOne(config.getOrderableId());

        validation = new MalariaValidation();

        for (PatientDataLineItem item: items) {

          //Find matching in patient line item data to support malaria data validation object
          if (orderableDto.getProductCode().equalsIgnoreCase(item.getProductCode())) {

            validation.setRequisitionId(requisitionId);
            validation.setProductCode(orderableDto.getProductCode());
            validation.setProductId(orderableDto.getId());
            validation.setDispensingUnit(orderableDto.getDispensable().getDispensingUnit());
            validation.setFullProductName(orderableDto.getFullProductName());
            validation.setTotalPatient(item.getValue());
            validation.setTotalConsumption(Integer.parseInt(config.getValue()) * item.getValue());

            //save malaria validation
            malariaValidationRepository.save(validation);
          }
        }

      }
    }
  }

  /**
   * get malaria data for requisition.
   *
   * @return patient repository DTO object
   */
  public Iterable<PatientData> findAllPatientsData() {
    return patientDataRepository.findAll();
  }

  /**
   * get malaria data for requisition.
   *
   * @return patient repository DTO object
   */
  public Iterable<MalariaValidation> findByRequisitionId(UUID requisitionId) {

    return malariaValidationRepository.findByRequisitionId(requisitionId);

  }
}
