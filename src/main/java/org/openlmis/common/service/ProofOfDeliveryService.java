/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service;

import javax.transaction.Transactional;
import org.openlmis.common.domain.ProofOfDelivery;
import org.openlmis.common.repository.ProofOfDeliveryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProofOfDeliveryService {

  private final ProofOfDeliveryRepository repository;

  @Autowired
  public ProofOfDeliveryService(
      ProofOfDeliveryRepository repository) {
    this.repository = repository;
  }

  /**
   * Creates or updates a {@link ProofOfDelivery}.
   * If an entity with the same order number exists, it updates it.
   * Otherwise, it creates a new entity.
   *
   * @param pod the {@link ProofOfDelivery} to save or update.
   *
   * @return the saved or updated {@link ProofOfDelivery}.
   */
  @Transactional
  public ProofOfDelivery createOrUpdate(ProofOfDelivery pod) {
    return repository.findByOrderNumber(pod.getOrderNumber())
        .map(existingPod -> {
          // Update the fields of the existing ProofOfDelivery
          existingPod.setComment(pod.getComment());
          existingPod.setCustomerId(pod.getCustomerId());
          existingPod.setDeliveredBy(pod.getDeliveredBy());
          existingPod.setDeliveredStatus(pod.getDeliveredStatus());
          existingPod.setInvoiceDate(pod.getInvoiceDate());
          existingPod.setInvoiceNumber(pod.getInvoiceNumber());
          existingPod.setMsdOrderNumber(pod.getMsdOrderNumber());
          existingPod.setNumberOfItemsReceived(pod.getNumberOfItemsReceived());
          existingPod.setOrderDate(pod.getOrderDate());
          existingPod.setReceivedBy(pod.getReceivedBy());
          existingPod.setReceivedDate(pod.getReceivedDate());
          existingPod.setPodLineItems(pod.getPodLineItems());
          return repository.save(existingPod);
        })
        .orElseGet(() -> repository.save(pod));
  }

}
