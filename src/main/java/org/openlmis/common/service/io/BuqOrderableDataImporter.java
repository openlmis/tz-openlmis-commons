/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.io;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import lombok.SneakyThrows;
import org.openlmis.common.service.BuqOrderableService;
import org.openlmis.common.util.CsvUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("buq_orderables.csv")
public class BuqOrderableDataImporter implements DataImport<Integer> {

  @Autowired
  private BuqOrderableService buqOrderableService;

  @Override
  @SneakyThrows
  public Integer doImport(InputStream dataStream) {
    Map<String, String> headers = new LinkedHashMap<>();
    headers.put("Product Code", "orderable_code");
    headers.put("Category Code", "category_code");
    headers.put("Product Class", "orderable_class");

    List<Map<String, String>> orderables = CsvUtils.readToMapWithFirstRecordAsKeys(
        new InputStreamReader(dataStream),
        headers
    );

    for (Map<String, String> orderable : orderables) {
      String orderableCode = orderable.get("orderable_code");
      String categoryCode = orderable.get("category_code");
      String orderableClass = orderable.get("orderable_class");

      buqOrderableService.createBuqOrderable(orderableCode, categoryCode, orderableClass);
    }

    return orderables.size();
  }
}
