/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.io;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import lombok.SneakyThrows;
import org.openlmis.common.dto.BuqOrderableCategoryDto;
import org.openlmis.common.service.BuqOrderableService;
import org.openlmis.common.util.CsvUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("buq_orderable_categories.csv")
public class BuqOrderableCategoryDataImporter implements DataImport<Integer> {

  @Autowired
  private BuqOrderableService buqOrderableService;

  @Override
  @SneakyThrows
  public Integer doImport(InputStream dataStream) {
    Map<String, String> headers = new LinkedHashMap<>();
    headers.put("Code", "code");
    headers.put("Name", "name");
    headers.put("Description", "description");

    List<Map<String, String>> categories = CsvUtils.readToMapWithFirstRecordAsKeys(
        new InputStreamReader(dataStream),
        headers
    );

    for (Map<String, String> category : categories) {
      buqOrderableService.createOrUpdateCategory(BuqOrderableCategoryDto.newInstance(category));
    }

    return categories.size();
  }
}
