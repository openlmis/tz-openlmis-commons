/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.io;

import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.i18n.CsvUploadMessageKeys;
import org.openlmis.common.util.FileHelper;
import org.openlmis.common.util.Message;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
public class DataImportService {

  @Autowired
  private FileHelper fileHelper;

  @Autowired
  private BeanFactory beanFactory;

  /**
   * Imports the data from a ZIP with CSV files.
   *
   * @param zipFile ZIP archive being imported.
   */
  @Transactional
  public Map<String, ?> importData(MultipartFile zipFile) {
    fileHelper.validateMultipartFile(zipFile);
    Map<String, InputStream> fileMap = fileHelper.convertMultipartFileToZipFileMap(zipFile);

    Map<String, Object> result = new LinkedHashMap<>();
    for (Map.Entry<String, InputStream> entry : fileMap.entrySet()) {
      String fileName = entry.getKey();
      if (fileName.startsWith("_") || fileName.startsWith(".")) {
        continue;
      }

      try {
        fileHelper.validateCsvFile(fileName);
        DataImport persister =
            beanFactory.getBean(fileName, DataImport.class);
        result.put(fileName, persister.doImport(entry.getValue()));
      } catch (NoSuchBeanDefinitionException e) {
        throw new ValidationMessageException(e, new Message(
            CsvUploadMessageKeys.ERROR_FILE_NAME_INVALID, fileName));
      }
    }

    return result;
  }

}
