/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.fulfillment;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.jxpath.JXPathContext;
import org.openlmis.common.dto.ErpOrderLineItemDto;
import org.openlmis.common.dto.VersionEntityReference;
import org.openlmis.common.dto.fulfillment.OrderDto;
import org.openlmis.common.dto.fulfillment.OrderLineItemDto;
import org.openlmis.common.dto.fulfillment.OrderPathContext;
import org.openlmis.common.dto.fulfillment.StatusChangeDto;
import org.openlmis.common.dto.referencedata.FacilityDto;
import org.openlmis.common.dto.referencedata.FileColumnDto;
import org.openlmis.common.dto.referencedata.FileTemplateDto;
import org.openlmis.common.dto.referencedata.OrderableDto;
import org.openlmis.common.dto.referencedata.ProgramDto;
import org.openlmis.common.dto.referencedata.UserDto;
import org.openlmis.common.dto.requisition.ProcessingPeriodDto;
import org.openlmis.common.dto.requisition.RequisitionDto;
import org.openlmis.common.service.referencedata.FacilityReferenceDataService;
import org.openlmis.common.service.referencedata.OrderableReferenceDataService;
import org.openlmis.common.service.referencedata.PeriodReferenceDataService;
import org.openlmis.common.service.referencedata.ProgramReferenceDataService;
import org.openlmis.common.service.referencedata.UserReferenceDataService;
import org.openlmis.common.service.requisition.RequisitionService;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
/**
 * TzOrderCsvHelper helps in OrderCsvHelper .
 */

@Component
public class TzOrderCsvHelper {

  private static final XLogger XLOGGER = XLoggerFactory.getXLogger(TzOrderCsvHelper.class);

  private static final String FACILITY = "Facility";
  private static final String PROGRAM = "Program";
  private static final String USER = "User";

  private static final String PRODUCT = "Orderable";

  private static final String PERIOD = "ProcessingPeriod";

  @Autowired
  private FacilityReferenceDataService facilityReferenceDataService;

  @Autowired
  private PeriodReferenceDataService periodReferenceDataService;

  @Autowired
  private OrderableReferenceDataService orderableReferenceDataService;

  @Autowired
  private ProgramReferenceDataService programReferenceDataService;

  @Autowired
  private RequisitionService requisitionService;

  @Autowired
  private UserReferenceDataService userReferenceDataService;

  @Value("${order.export.includeZeroQuantity}")
  private boolean includeZeroQuantity;

  /**
   * Exporting order to csv.
   */
  public void writeCsvFile(OrderDto order, FileTemplateDto fileTemplate, Writer writer)
      throws IOException {

    List<FileColumnDto> fileColumns = fileTemplate.getFileColumns();
    CsvColumnHelper.removeExcludedColumns(fileColumns);
    if (fileTemplate.getHeaderInFile()) {
      CsvColumnHelper.writeHeader(fileColumns, writer);
    }

    XLOGGER.info("Get reference data ");

    List<UUID> orderableIds = new ArrayList<>();

    for (OrderLineItemDto orderLineItem : order.getOrderLineItems()) {
      orderableIds.add(orderLineItem.getOrderable().getId());
    }

    StatusChangeDto approved = order.getApprovedStatusChange();
    StatusChangeDto authorized = order.getAuthorizedStatusChange();
    StatusChangeDto regionApproved = order.getApprovedStatusChange();
    if (approved.getAuthorId() != null) {
      UserDto user = userReferenceDataService.findOne(approved.getAuthorId());
      approved.setAuthor(user);
    }
    if (authorized.getAuthorId() != null) {
      UserDto user = userReferenceDataService.findOne(authorized.getAuthorId());
      authorized.setAuthor(user);
    }

    if (regionApproved.getAuthorId() != null) {
      UserDto user = userReferenceDataService.findOne(regionApproved.getAuthorId());
      regionApproved.setAuthor(user);
    }

    List<OrderableDto> orderables = orderableReferenceDataService
        .findByIdies(orderableIds);

    Map<UUID, OrderableDto> orderablesMap = orderables.stream()
        .collect(Collectors.toMap(OrderableDto::getId, Function.identity()));

    FacilityDto supplyingFacility
        = facilityReferenceDataService.findOne(order.getSupplyingFacility().getId());
    FacilityDto facilityDto
        = facilityReferenceDataService.findOne(order.getFacility().getId());
    ProcessingPeriodDto processingPeriodDto
        = periodReferenceDataService.findOne(order.getProcessingPeriod().getId());

    RequisitionDto requisitionDto
        = requisitionService.findOne(order.getExternalId());

    writeLineItems(order, requisitionDto, authorized, approved, regionApproved,
        order.getOrderLineItems(),
        supplyingFacility, facilityDto, processingPeriodDto, orderablesMap,
        fileColumns, writer);
  }

  private void writeLineItems(OrderDto order, RequisitionDto requisitionDto,
                              StatusChangeDto authorized,
                              StatusChangeDto approved,
                              StatusChangeDto regionApproved,
                              List<OrderLineItemDto> orderLineItems,
                              FacilityDto supplyingFacility,
                              FacilityDto facilityDto,
                              ProcessingPeriodDto processingPeriodDto,
                              Map<UUID, OrderableDto> orderablesMap,
                              List<FileColumnDto> fileColumns, Writer writer)
      throws IOException {
    int counter = 1;

    for (OrderLineItemDto orderLineItem : orderLineItems) {
      if (includeZeroQuantity || orderLineItem.getOrderedQuantity() > 0) {

        XLOGGER.info("Populate ERP Line Item Export Data ");

        ErpOrderLineItemDto erpOrderLineItem = new ErpOrderLineItemDto(order, requisitionDto,
            authorized,
            approved,
            regionApproved,
            orderLineItem, orderablesMap, facilityDto,
            supplyingFacility, processingPeriodDto, counter);

        writeCsvLineItem(order, orderLineItem, orderLineItem.getOrderable(),
            fileColumns, writer, counter++, erpOrderLineItem);
        writer.write(CsvColumnHelper.LINE_SEPARATOR);
      }
    }
  }

  private void writeCsvLineItem(OrderDto order, OrderLineItemDto orderLineItem,
                                OrderableDto orderable,
                                List<FileColumnDto> fileColumns, Writer writer, int counter,
                                ErpOrderLineItemDto erpOrderLineItem)
      throws IOException {
    JXPathContext orderContext = JXPathContext.newContext(order);
    JXPathContext lineItemContext = JXPathContext.newContext(orderLineItem);
    JXPathContext lineItemOrderableContext = JXPathContext.newContext(orderable);

    JXPathContext erpOrderLineItemContext = JXPathContext.newContext(erpOrderLineItem);

    for (FileColumnDto fileColumn : fileColumns) {
      if (fileColumn.getNested() == null || fileColumn.getNested().isEmpty()) {
        if (fileColumns.indexOf(fileColumn) < fileColumns.size() - 1) {
          writer.write(",");
        }
        continue;
      }

      OrderPathContext orderPathContext = OrderPathContext
          .builder()
          .orderContext(orderContext)
          .lineItemContext(lineItemContext)
          .lineItemOrderableContext(lineItemOrderableContext)
          .fileColumn(fileColumn)
          .erpOrderLineItemContext(erpOrderLineItemContext)
          .build();

      Object columnValue = getColumnValue(counter, orderPathContext);

      columnValue = CsvColumnHelper.formatColumn(fileColumn, columnValue);

      CsvColumnHelper.separateColumns(fileColumns, writer, fileColumn, columnValue);
    }
  }

  private  Object getColumnValue(int counter, OrderPathContext orderContext) {
    Object columnValue = CsvColumnHelper.getXpathValue(counter, orderContext);
    FileColumnDto fileColumn = orderContext.getFileColumn();
    if (fileColumn.getRelated() != null && !fileColumn.getRelated().isEmpty()) {
      if (columnValue instanceof VersionEntityReference) {
        columnValue = ((VersionEntityReference) columnValue).getId();
      }
      if (columnValue instanceof org.openlmis.common.dto.BaseDto) {
        columnValue = ((org.openlmis.common.dto.BaseDto) columnValue).getId();
      }
      columnValue = getRelatedColumnValue((UUID) columnValue, fileColumn);
    }

    return columnValue == null ? "" : columnValue;
  }

  private Object getRelatedColumnValue(UUID relatedId, FileColumnDto fileColumn) {
    if (relatedId == null) {
      return null;
    }

    Object columnValue;

    switch (fileColumn.getRelated()) {
      case FACILITY:
        FacilityDto facility = facilityReferenceDataService.findOne(relatedId);
        columnValue = getValue(facility, fileColumn.getRelatedKeyPath());
        break;
      case PRODUCT:
        OrderableDto product = orderableReferenceDataService.findOne(relatedId);
        columnValue = getValue(product, fileColumn.getRelatedKeyPath());
        break;
      case PERIOD:
        ProcessingPeriodDto period = periodReferenceDataService.findOne(relatedId);
        columnValue = getValue(period, fileColumn.getRelatedKeyPath());
        break;
      case PROGRAM:
        ProgramDto program = programReferenceDataService.findOne(relatedId);
        columnValue = getValue(program, fileColumn.getRelatedKeyPath());
        break;
      case USER:
        UserDto user = userReferenceDataService.findOne(relatedId);
        columnValue = getValue(user, fileColumn.getRelatedKeyPath());
        break;
      default:
        columnValue = null;
        break;
    }

    return columnValue;
  }

  private Object getValue(Object object, String keyPath) {
    JXPathContext context = JXPathContext.newContext(object);

    return context.getValue(keyPath);
  }

}
