/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.fulfillment;

import java.io.IOException;
import java.io.Writer;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.openlmis.common.dto.fulfillment.OrderPathContext;
import org.openlmis.common.dto.referencedata.FileColumnDto;

/**
 * Helper class for handling CSV columns in fulfillment services.
 */
public class CsvColumnHelper {
  public static final String STRING = "string";
  public static final String LINE_NO = "line_no";
  public static final String ORDER = "order";
  public static final String LINE_ITEM_ORDERABLE = "lineItemOrderable";
  public static final String ERP_ORDER_LINE_ITEM = "ErpOrderLineItemData";
  public static final String LINE_SEPARATOR = "\r\n";
  public static final Boolean ENCLOSE_VALUES_WITH_QUOTES = false;

  /**
   * Removes columns that are excluded from fileColumns.
   *
   * @param fileColumns List of FileColumnDto.
   */

  public static void removeExcludedColumns(List<FileColumnDto> fileColumns) {
    CollectionUtils.filter(fileColumns, object -> ((FileColumnDto) object).getInclude());
  }

  /**
   * Writes the header line for the CSV file.
   *
   * @param fileColumns List of FileColumnDto
   * @param writer      Writer to write the header to
   * @throws IOException if an I/O error occurs
   */

  public static void writeHeader(List<FileColumnDto> fileColumns,
                                 Writer writer) throws IOException {
    for (FileColumnDto column : fileColumns) {
      String columnLabel = column.getColumnLabel();
      if (columnLabel == null) {
        columnLabel = "";
      }
      writer.write(columnLabel);
      if (fileColumns.indexOf(column) == (fileColumns.size() - 1)) {
        writer.write(LINE_SEPARATOR);
        break;
      }
      writer.write(",");
    }
  }

  /**
   * Handles separation of columns in the CSV output.
   *
   * @param fileColumns List of FileColumnDto.
   * @param writer      Writer to write the data to.
   * @param fileColumn  Current column being written.
   * @param columnValue Value of the column to write.
   * @throws IOException if an I/O error occurs.
   */

  public static void separateColumns(List<FileColumnDto> fileColumns, Writer writer,
                                     FileColumnDto fileColumn, Object columnValue)
      throws IOException {
    if (ENCLOSE_VALUES_WITH_QUOTES) {
      writer.write("\"" + columnValue.toString() + "\"");
    } else {
      writer.write(columnValue.toString());
    }

    if (fileColumns.indexOf(fileColumn) < fileColumns.size() - 1) {
      writer.write(",");
    }
  }

  /**
   * Formats the column value according to the specified format in FileColumnDto.
   *
   * @param fileColumn  The FileColumnDto that contains formatting information.
   * @param columnValue The value to format.
   * @return The formatted object.
   */
  public static Object formatColumn(FileColumnDto fileColumn, Object columnValue) {
    if (columnValue instanceof ZonedDateTime) {
      columnValue = ((ZonedDateTime) columnValue)
          .format(DateTimeFormatter
              .ofPattern(fileColumn.getFormat()));
    } else if (columnValue instanceof LocalDate) {
      columnValue = ((LocalDate) columnValue)
          .format(DateTimeFormatter.ofPattern(fileColumn.getFormat()));
    }
    return columnValue;
  }

  /**
   * Retrieves the value for a column based on its XPath from the provided order context.
   *
   * @param counter      The line number in the CSV file.
   * @param orderContext Context containing all necessary data.
   * @return The value to write for the current column.
   */
  public static Object getXpathValue(int counter, OrderPathContext orderContext) {
    Object columnValue;
    FileColumnDto fileColumn = orderContext.getFileColumn();

    switch (fileColumn.getNested()) {
      case STRING:
        columnValue = fileColumn.getKeyPath();
        break;
      case LINE_NO:
        columnValue = counter;
        break;
      case ERP_ORDER_LINE_ITEM:
        columnValue = orderContext.getErpOrderLineItemContext().getValue(fileColumn.getKeyPath());
        break;
      case ORDER:
        columnValue = orderContext.getOrderContext().getValue(fileColumn.getKeyPath());
        break;
      case LINE_ITEM_ORDERABLE:
        columnValue = orderContext.getLineItemOrderableContext().getValue(fileColumn.getKeyPath());
        break;
      default:
        columnValue = orderContext.getLineItemContext().getValue(fileColumn.getKeyPath());
        break;
    }
    return columnValue;
  }
}
