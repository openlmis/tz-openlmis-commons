/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.fulfillment;

import org.openlmis.common.dto.referencedata.FileTemplateDto;
import org.openlmis.common.dto.referencedata.TemplateType;
import org.openlmis.common.service.RequestParameters;
import org.springframework.stereotype.Service;

@Service
public class FileTemplateService extends BaseFulfillmentService<FileTemplateDto> {

  @Override
  protected String getUrl() {
    return "/api/fileTemplates/";
  }

  @Override
  protected Class<FileTemplateDto> getResultClass() {
    return FileTemplateDto.class;
  }

  @Override
  protected Class<FileTemplateDto[]> getArrayResultClass() {
    return FileTemplateDto[].class;
  }

  /**
   * This method retrieves order template with given template type.
   *
   * @return TemplateDto containing template's data, or null if such template was not found.
   */
  public FileTemplateDto findOrderTemplate() {

    RequestParameters parameter = RequestParameters
        .init()
        .set("templateType", TemplateType.ORDER);

    return modifiedFindOne(getUrl(), parameter, FileTemplateDto.class);
  }

}
