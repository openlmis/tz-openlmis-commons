/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service;

import static org.apache.commons.lang3.StringUtils.startsWith;
import static org.openlmis.common.i18n.PermissionMessageKeys.ERROR_NO_FOLLOWING_PERMISSION;

import java.util.UUID;
import org.openlmis.common.dto.referencedata.PermissionStringDto;
import org.openlmis.common.exception.PermissionMessageException;
import org.openlmis.common.util.AuthenticationHelper;
import org.openlmis.common.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

@Service
@SuppressWarnings("PMD.TooManyMethods")
public class PermissionService {
  public static final String DISTRICT_CONSUMPTION_REPORT_VIEW = "DISTRICT_CONSUMPTION_REPORT_VIEW";
  public static final String FACILITY_CONSUMPTION_REPORT_VIEW = "FACILITY_CONSUMPTION_REPORT_VIEW";
  public static final String REQUISITION_VIEW = "REQUISITION_VIEW";
  public static final String PROCESSING_SCHEDULES_MANAGE_RIGHT = "PROCESSING_SCHEDULES_MANAGE";
  public static final String ORDERABLES_MANAGE = "ORDERABLES_MANAGE";
  public static final String DATA_IMPORT = "DATA_IMPORT";

  static final String REPORT_TEMPLATES_EDIT = "REPORT_TEMPLATES_EDIT";
  static final String REPORTS_VIEW = "REPORTS_VIEW";
  public static final String ORDERS_EDIT = "ORDERS_EDIT";

  @Autowired
  private AuthenticationHelper authenticationHelper;

  @Autowired
  private PermissionStrings permissionStrings;


  @Value("${auth.server.clientId}")
  private String serviceTokenClientId;

  @Value("${auth.server.clientId.apiKey.prefix}")
  private String apiKeyPrefix;

  /**
   * Check if user has the specified right.
   *
   * @param rightName the name of the right to check
   */
  public void checkPermission(String rightName) {
    if (!hasPermission(rightName)) {
      throw new PermissionMessageException(new Message(ERROR_NO_FOLLOWING_PERMISSION, rightName));
    }
  }

  /**
   * Check if user has the specified right on the specified program and facility.
   *
   * @param rightName the name of the right to check
   * @param program   the program to check
   * @param facility  the facility to check
   */
  public void checkPermission(String rightName, UUID program, UUID facility) {
    if (!hasPermission(rightName, program, facility)) {
      throw new PermissionMessageException(new Message(ERROR_NO_FOLLOWING_PERMISSION, rightName));
    }
  }

  private Boolean hasPermission(String rightName) {
    return hasPermission(rightName, null, null);
  }

  private Boolean hasPermission(String rightName, UUID program, UUID facility) {
    return hasPermission(rightName, program, facility, true, true, false);
  }

  private boolean hasPermission(String rightName, UUID program, UUID facility,
                                boolean allowUserTokens, boolean allowServiceTokens,
                                boolean allowApiKey) {
    OAuth2Authentication authentication = (OAuth2Authentication) SecurityContextHolder
        .getContext()
        .getAuthentication();

    return authentication.isClientOnly()
        ? checkServiceToken(allowServiceTokens, allowApiKey, authentication)
        : checkUserToken(rightName, program, facility, allowUserTokens);
  }

  private boolean checkUserToken(String rightName, UUID program, UUID facility,
                                 boolean allowUserTokens) {
    if (!allowUserTokens) {
      return false;
    }

    UUID user = authenticationHelper.getCurrentUser().getId();
    PermissionStrings.Handler handler = getPermissionStrings(user);
    PermissionStringDto permission = PermissionStringDto.create(rightName, facility, program);

    return handler.get().contains(permission);
  }

  private boolean checkServiceToken(boolean allowServiceTokens, boolean allowApiKey,
                                    OAuth2Authentication authentication) {
    String clientId = authentication.getOAuth2Request().getClientId();

    if (serviceTokenClientId.equals(clientId)) {
      return allowServiceTokens;
    }

    if (startsWith(clientId, apiKeyPrefix)) {
      return allowApiKey;
    }

    return false;
  }

  public PermissionStrings.Handler getPermissionStrings(UUID userId) {
    return permissionStrings.forUser(userId);
  }

  public void canViewRequisitions(UUID program, UUID district) {
    checkPermission(REQUISITION_VIEW, program, district);
  }

  public void canViewFacilityQuantifications(UUID program, UUID facility) {
    checkPermission(FACILITY_CONSUMPTION_REPORT_VIEW);
    canViewRequisitions(program, facility);
  }

  public void canViewDistrictQuantifications(UUID program, UUID district) {
    checkPermission(DISTRICT_CONSUMPTION_REPORT_VIEW);
    canViewRequisitions(program, district);
  }

}