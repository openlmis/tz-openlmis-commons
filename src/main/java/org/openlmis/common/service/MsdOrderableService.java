/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import org.openlmis.common.domain.MsdOrderable;
import org.openlmis.common.dto.referencedata.DispensableDto;
import org.openlmis.common.dto.referencedata.OrderableDto;
import org.openlmis.common.repository.MsdOrderableRepository;
import org.openlmis.common.service.referencedata.OrderableReferenceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MsdOrderableService {

  @Autowired
  private OrderableReferenceDataService referenceService;

  @Autowired
  private MsdOrderableRepository repository;

  /**
   * Save all MSD Orderables.
   */
  public List<MsdOrderable> saveAll(
      List<MsdOrderable> orderables) {
    List<MsdOrderable> savedOrderables = new ArrayList<>();
    for (MsdOrderable orderable : orderables) {
      // Perform validation before saving
      if (orderable.getItemCode() == null
          || orderable.getCode().isEmpty()) {
        throw new
            IllegalArgumentException("Item code cannot be empty");
      }
      //search orderable by code
      List<OrderableDto> orderableDtoList = referenceService
          .findByCode(orderable.getCode());

      if (!orderableDtoList.isEmpty()) {
        return savedOrderables;
      }

      // Check if item code already exists
      Optional<MsdOrderable> existingOrderableOpt = repository
          .findByCodeIgnoreCase(orderable.getCode());

      if (existingOrderableOpt.isPresent()) {
        // Update existing orderable
        MsdOrderable
            existingOrderable = existingOrderableOpt.get();
        existingOrderable.setCode(orderable.getCode());
        existingOrderable.setPrimaryName(
            orderable.getPrimaryName());
        existingOrderable.setRevision(
            orderable.getRevision());
        existingOrderable.setUom(orderable.getUom());
        existingOrderable.setUomDescription(
            orderable.getUomDescription());
        existingOrderable.setProductCategory(
            orderable.getProductCategory());
        existingOrderable.setForm(orderable.getForm());
        existingOrderable.setPackSize(orderable.getPackSize());
        existingOrderable.setPriceCode(orderable.getPriceCode());
        existingOrderable.setPrice(orderable.getPrice());
        savedOrderables.add(repository.save(existingOrderable));
      } else {
        // Save new orderable
        savedOrderables.add(repository.save(orderable));
      }
    }
    return savedOrderables;
  }

  /**
   * Retrieves all MsdOrderable entities from the database.
   *
   * @return List of all MsdOrderable entities
   */
  public List<MsdOrderable> getAllOrderables() {
    return repository.findAll();
  }

  /**
   * Retrieves an MsdOrderable entity by its ID.
   *
   * @param id The ID of the MsdOrderable entity to retrieve
   * @return The MsdOrderable entity with the given ID
   * @throws EntityNotFoundException if no exists.
   */
  public MsdOrderable getOrderableById(Long id) {
    return repository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException(
            "MsdOrderable not found with id: " + id));
  }

  /**
   * Save all MSD Orderables.
   *
   * @param code The code of the MsdOrderable entity to retrieve
   * @return The MsdOrderable entity with the given ID
   */
  public MsdOrderable getOrderableByCode(String code) {
    return repository.findByCodeIgnoreCase(code)
        .orElseThrow(() -> new EntityNotFoundException(
            "MsdOrderable not found with code: " + code));
  }

  /**
   * Retrieves an OrderableDto object for the given product code.
   *
   * @param code The product code.
   * @return The OrderableDto object.
   */
  @Transactional
  public OrderableDto postOrderable(String code) {

    MsdOrderable msdOrderable = getOrderableByCode(code);
    OrderableDto dto = createOrderableDto(msdOrderable);
    updateOrderableDto(dto, msdOrderable);
    List<OrderableDto> dto1 = referenceService.postOrderable(dto);
    OrderableDto orderableDto = dto1.isEmpty() ? null : dto1.get(0);
    return orderableDto;
  }

  /**
   * Creates an OrderableDto object from an MsdOrderable object.
   *
   * @param msdOrderable The MsdOrderable object.
   * @return The OrderableDto object.
   */
  private OrderableDto createOrderableDto(
      MsdOrderable msdOrderable) {
    OrderableDto dto = new OrderableDto();
    dto.setProductCode(msdOrderable.getCode());
    dto.setFullProductName(msdOrderable.getPrimaryName());
    DispensableDto dispensable = createDispensableDto(
        msdOrderable);
    dto.setDispensable(dispensable);
    dto.setNetContent(msdOrderable.getPackSize());
    dto.setDescription(msdOrderable.getPrimaryName());
    return dto;
  }

  /**
   * Creates a DispensableDto object from an MsdOrderable object.
   *
   * @param msdOrderable The MsdOrderable object.
   * @return The DispensableDto object.
   */
  private DispensableDto createDispensableDto(
      MsdOrderable msdOrderable) {
    DispensableDto dispensable = new DispensableDto();
    dispensable.setDispensingUnit(msdOrderable.getUomDescription());
    dispensable.setDisplayUnit(msdOrderable.getUomDescription());
    return dispensable;
  }

  /**
   * Updates an OrderableDto object with additional data.
   *
   * @param dto The OrderableDto object.
   * @return The updated OrderableDto object.
   */
  private OrderableDto updateOrderableDto(
      OrderableDto dto, MsdOrderable msdOrderable) {
    Map<String, String> extraData = new HashMap<>();

    extraData.put("uom", msdOrderable.getUom());
    extraData.put("revision", msdOrderable.getRevision());
    extraData.put("price", msdOrderable.getPrice().toString());
    extraData.put("productCategory", msdOrderable.getProductCategory());
    extraData.put("form", msdOrderable.getForm());
    extraData.put("priceCode", msdOrderable.getPriceCode());
    extraData.put("itemCode", msdOrderable.getItemCode());
    extraData.put("processed", "false");
    dto.setPackRoundingThreshold(1);
    dto.setRoundToZero(true);
    dto.setExtraData(extraData);
    return dto;
  }

  /**
   * Delete Orderable code.
   *
   * @param code The code of the Orderable entity to delete.
   */
  @Transactional
  public void deleteMsdOrderableByCode(String code) {
    repository.deleteByCode(code);
  }

}
