/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.referencedata;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.openlmis.common.dto.referencedata.OrderableDto;
import org.openlmis.common.service.RequestHeaders;
import org.openlmis.common.service.RequestParameters;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
public class OrderableReferenceDataService
    extends BaseReferenceDataService<OrderableDto> {

  @Override
  protected String getUrl() {
    return "/api/orderables/";
  }

  @Override
  protected Class<OrderableDto> getResultClass() {
    return OrderableDto.class;
  }

  @Override
  protected Class<OrderableDto[]> getArrayResultClass() {
    return OrderableDto[].class;
  }

  /**
   * Finds orderables by their ids.
   *
   * @param ids ids to look for.
   * @return a page of orderables
   */
  public List<OrderableDto> findByIds(Set<OrderableDto> ids) {
    return CollectionUtils.isEmpty(ids)
        ? Collections.emptyList()
        : getPage(RequestParameters.init().set("id", ids)).getContent();
  }


  @Override
  public List<OrderableDto> findAll() {
    return getPage(RequestParameters.init()).getContent();
  }

  /**
   * Finds orderables by their ids.
   *
   * @param orderableIds ids to look for.
   * @return a page of orderables
   */
  public List<OrderableDto> findByIdies(List<UUID> orderableIds) {
    return CollectionUtils.isEmpty(orderableIds)
        ? Collections.emptyList()
        : getPage(RequestParameters.init().set("id", orderableIds)).getContent();
  }

  /**
   * Finds orderable by code.
   */
  public List<OrderableDto> findByCode(String code) {

    if (code == null) {
      return Collections.emptyList();
    }

    OrderableSearchParams payload = new OrderableSearchParams(
        code, null, null, null, null, null);

    return getPage("/search", RequestParameters.init(), payload).getContent();
  }

  //TODO Updating of orderable does not return a list. Need to fix this logic
  public List<OrderableDto> postOrderable(OrderableDto dto) {
    return getPage("", RequestParameters.init(), dto,
        HttpMethod.PUT, OrderableDto.class).getContent();
  }

  /**
   * Update orderable.
   *
   * @param orderable orderable details for updating
   * @return
   */
  public OrderableDto updateOrderable(OrderableDto orderable) {
    return execute("/" + orderable.getId(), RequestParameters.init(),
        RequestHeaders.init(), orderable,
        HttpMethod.PUT,
        OrderableDto.class).getBody();
  }
}
