/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.referencedata;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.openlmis.common.dto.referencedata.ProgramDto;
import org.openlmis.common.service.RequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

@Service
public class ProgramReferenceDataService extends BaseReferenceDataService<ProgramDto> {

  @Override
  protected String getUrl() {
    return "/api/programs/";
  }

  @Override
  protected Class<ProgramDto> getResultClass() {
    return ProgramDto.class;
  }

  @Override
  protected Class<ProgramDto[]> getArrayResultClass() {
    return ProgramDto[].class;
  }

  @Override
  public List<ProgramDto> findAll() {
    return findAll("",
        null, null, HttpMethod.GET, ProgramDto[].class);
  }

  /**
   * This method retrieves Facilities for given ids.
   *
   * @param programIds list of facility ids.
   *
   * @return List of FacilityDtos with similar ids.
   */
  public List<ProgramDto> search(Set<UUID> programIds) {
    return getPage(RequestParameters.init().set("id", programIds)).getContent();
  }

  /**
   * This method retrieves Programs with facilityName similar with name parameter or
   * facilityCode similar with code parameter.
   *
   * @param code Field with string to find similar code.
   *
   * @return List of FacilityDtos with similar code or name.
   */
  public List<ProgramDto> search(String code) {
    Map<String, Object> requestBody = new HashMap<>();
    requestBody.put("code", code);
    return getBasicFacilityPage("search", RequestParameters.init(), requestBody).getContent();
  }

  protected Page<ProgramDto> getBasicFacilityPage(String resourceUrl,
                                                  RequestParameters parameters,
                                                  Object payload) {
    return getPage(resourceUrl, parameters, payload, HttpMethod.POST, ProgramDto.class);
  }

}
