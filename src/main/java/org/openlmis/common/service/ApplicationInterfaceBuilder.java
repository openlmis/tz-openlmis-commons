/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service;

import java.util.List;
import java.util.stream.Collectors;
import org.openlmis.common.domain.ApplicationInterface;
import org.openlmis.common.domain.InterfaceDataSet;
import org.openlmis.common.dto.ApplicationInterfaceDto;
import org.openlmis.common.dto.InterfaceDataSetDto;
import org.springframework.stereotype.Component;

@Component
public class ApplicationInterfaceBuilder {

  /**
   * Creates a DTO based on the application interface passed.
   *
   * @param applicationInterface {@link org.openlmis.common.domain.ApplicationInterface}
   *       object to be converted to a DTO.
   * @return {@link org.openlmis.common.dto.ApplicationInterfaceDto}
   */
  public ApplicationInterfaceDto
      buildDto(ApplicationInterface applicationInterface) {
    ApplicationInterfaceDto dto = new ApplicationInterfaceDto();
    applicationInterface.export(dto);

    List<InterfaceDataSet> interfaceDataSetList = applicationInterface.getDataSets();

    List<InterfaceDataSetDto> lineItemDtoList = interfaceDataSetList.stream()
        .map(InterfaceDataSetDto::newInstance)
        .collect(Collectors.toList());
    dto.setDataSets(lineItemDtoList);

    return dto;
  }
}
