/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.requisition;

import static java.util.Comparator.comparing;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.openlmis.common.dto.requisition.BasicRequisitionDto;
import org.openlmis.common.dto.requisition.RequisitionDto;
import org.openlmis.common.dto.requisition.RequisitionLineItemDto;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.service.RequestParameters;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class QuantificationService {

  private static final String[] DISTRICT_LEVEL_QUANTIFICATION_REPORT_HEADERS = {
      "Facility Name", "Facility Code", "Product Name", "Product Code", "Unit",
      "Adjusted Consumption"
  };
  private static final String[] FACILITY_LEVEL_QUANTIFICATION_REPORT_HEADERS = {
      "Facility Name", "Facility Code", "Product Name", "Product Code", "Unit",
      "Adjusted Consumption"
  };
  private final RequisitionService requisitionService;

  /**
   * Generates a exportable CSV input stream with district level quantification report.
   *
   * @param district          A destrict to generate report for.
   * @param program           A program filter for the entries to be included.
   * @param period            Processing period filter for the entries to be included.
   * @param initiatedDateFrom A beginning date filter for the entries to be included.
   * @param initiatedDateTo   An end date filter for the entries to be included.
   *
   * @return Generated CSV Input Stream
   */
  public ByteArrayInputStream generateDistrictLevelQuantificationCsv(
      UUID district,
      UUID program,
      UUID period,
      ZonedDateTime initiatedDateFrom,
      ZonedDateTime initiatedDateTo) {
    List<BasicRequisitionDto> requisitions = requisitionService.search(
        RequestParameters.init()
            .set("supervisoryNode", district)
            .set("program", program)
            .set("processingPeriod", period)
            .set("initiatedDateFrom", initiatedDateFrom)
            .set("initiatedDateTo", initiatedDateTo), false);

    final CSVFormat format = CSVFormat.DEFAULT.withQuoteMode(QuoteMode.MINIMAL)
        .withHeader(DISTRICT_LEVEL_QUANTIFICATION_REPORT_HEADERS);

    try {
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      CSVPrinter csvPrinter = new CSVPrinter(new PrintWriter(out), format);

      Map<Triple<String, String, String>, List<Pair<RequisitionLineItemDto, RequisitionDto>>> data
          = requisitions.stream().map(basicReq -> requisitionService.findOne(basicReq.getId()))
          .flatMap(requisition ->
              requisition.getRequisitionLineItems()
                  .stream()
                  .map(lineItem -> Pair.of(lineItem, requisition)))
          .collect(Collectors.groupingBy(lineItem -> {
            return Triple.of(
                lineItem.getRight().getFacility().getCode(),
                lineItem.getLeft().getOrderable().getProductCode(),
                lineItem.getLeft().getOrderable().getDispensable().getDisplayUnit()
            );
          }));

      List<Triple<String, String, String>> dataKeys = data.keySet().stream()
          .sorted(comparing(Triple::toString))
          .collect(Collectors.toList());

      for (Triple<String, String, String> entry : dataKeys) {
        List<Pair<RequisitionLineItemDto, RequisitionDto>> entryData = data.get(entry);
        Pair<RequisitionLineItemDto, RequisitionDto> template = entryData.get(0);
        csvPrinter.printRecord(
            template.getRight().getFacility().getName(),
            template.getRight().getFacility().getCode(),
            template.getLeft().getOrderable().getFullProductName(),
            template.getLeft().getOrderable().getProductCode(),
            template.getLeft().getOrderable().getDispensable().getDisplayUnit(),
            entryData.stream()
                .map(lineItem -> {
                  Integer adjustedConsumption = lineItem.getLeft().getAdjustedConsumption();
                  return adjustedConsumption != null ? adjustedConsumption : 0;
                })
                .reduce(0, (total, adj) -> total + adj)
        );
      }

      csvPrinter.flush();
      return new ByteArrayInputStream(out.toByteArray());
    } catch (IOException e) {
      throw new ValidationMessageException(e, "fail to export data to CSV file: " + e.getMessage());
    }
  }


  /**
   * Generates exportable CSV input stream with facility level quantification report.
   *
   * @param facility          A facility to generate report for.
   * @param program           A program filter for the entries to be included.
   * @param period            Processing period filter for the entries to be included.
   * @param initiatedDateFrom A beginning date filter for the entries to be included.
   * @param initiatedDateTo   An end date filter for the entries to be included.
   *
   * @return CSV Input Stream
   */
  public ByteArrayInputStream generateFacilityLevelQuantificationCsv(
      UUID facility,
      UUID program,
      UUID period,
      ZonedDateTime initiatedDateFrom,
      ZonedDateTime initiatedDateTo) {
    List<BasicRequisitionDto> requisitions = requisitionService.search(
        RequestParameters.init()
            .set("facility", facility)
            .set("program", program)
            .set("processingPeriod", period)
            .set("initiatedDateFrom", initiatedDateFrom)
            .set("initiatedDateTo", initiatedDateTo), false
    );

    final CSVFormat format = CSVFormat.DEFAULT.withQuoteMode(QuoteMode.MINIMAL)
        .withHeader(FACILITY_LEVEL_QUANTIFICATION_REPORT_HEADERS);

    try {
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      CSVPrinter csvPrinter = new CSVPrinter(new PrintWriter(out), format);

      Map<Pair<String, String>, List<Pair<RequisitionLineItemDto, RequisitionDto>>> data
          = requisitions.stream().map(basicReq -> requisitionService.findOne(basicReq.getId()))
          .flatMap(requisition ->
              requisition.getRequisitionLineItems()
                  .stream()
                  .map(lineItem -> Pair.of(lineItem, requisition)))
          .collect(Collectors.groupingBy(lineItem -> {
            return Pair.of(
                lineItem.getLeft().getOrderable().getProductCode(),
                lineItem.getLeft().getOrderable().getDispensable().getDisplayUnit()
            );
          }));

      List<Pair<String, String>> dataKeys = data.keySet().stream()
          .sorted(comparing(Pair::toString))
          .collect(Collectors.toList());

      for (Pair<String, String> entry : dataKeys) {
        List<Pair<RequisitionLineItemDto, RequisitionDto>> entryData = data.get(entry);
        Pair<RequisitionLineItemDto, RequisitionDto> template = entryData.get(0);
        csvPrinter.printRecord(
            template.getLeft().getOrderable().getFullProductName(),
            template.getLeft().getOrderable().getProductCode(),
            template.getLeft().getOrderable().getDispensable().getDisplayUnit(),
            entryData.stream()
                .map(lineItem -> {
                  Integer adjustedConsumption = lineItem.getLeft().getAdjustedConsumption();
                  return adjustedConsumption != null ? adjustedConsumption : 0;
                })
                .reduce(0, (total, adj) -> total + adj)
        );
      }

      csvPrinter.flush();
      return new ByteArrayInputStream(out.toByteArray());
    } catch (IOException e) {
      throw new ValidationMessageException(e, "fail to export data to CSV file: " + e.getMessage());
    }
  }
}
