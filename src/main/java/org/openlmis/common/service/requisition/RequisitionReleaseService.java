/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.requisition;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.openlmis.common.domain.StatusChange;
import org.openlmis.common.dto.OrderDtoBuilder;
import org.openlmis.common.dto.ReleasableRequisitionDto;
import org.openlmis.common.dto.fulfillment.OrderDto;
import org.openlmis.common.dto.referencedata.UserDto;
import org.openlmis.common.dto.requisition.RequisitionDto;
import org.openlmis.common.repository.StatusChangeRepository;
import org.openlmis.common.service.fulfillment.OrderFulfillmentService;
import org.openlmis.common.util.AuthenticationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.profiler.Profiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RequisitionReleaseService {

  private static final Logger LOGGER = LoggerFactory.getLogger(RequisitionReleaseService.class);
  private static final String RELEASED_STATUS = "RELEASED";

  @Autowired
  private StatusChangeRepository statusChangeRepository;

  @Autowired
  private AuthenticationHelper authenticationHelper;

  @Autowired
  private OrderDtoBuilder orderDtoBuilder;

  private final OrderFulfillmentService orderFulfillmentService;

  public RequisitionReleaseService(OrderFulfillmentService
                                       orderFulfillmentService) {
    this.orderFulfillmentService = orderFulfillmentService;
  }

  /**
   * Prepares release orders for the given requisition and user.
   *
   * @param requisition the requisition to prepare release orders for
   * @param userDto     the user responsible for the release orders
   */
  public void prepareReleaseOrder(RequisitionDto requisition, UserDto userDto) {
    LOGGER.info("CONVERT_TO_ORDER");
    LOGGER.info("RELEASE_REQUISITIONS_AS_ORDER");

    List<ReleasableRequisitionDto> dtoList = new ArrayList<>();
    ReleasableRequisitionDto dto = new ReleasableRequisitionDto();
    dto.setRequisitionId(requisition.getRequisitionId());
    dto.setSupplyingDepotId(requisition.getSupplyingFacilityId());
    dtoList.add(dto);
    LOGGER.info("Requisition for Convert DTO: {}", requisition);
    List<RequisitionDto> releasedRequisitions = releaseRequisitionsAsOrder(dtoList,
        requisition);
    LOGGER.debug("BUILD_ORDER_DTOS_AND_SAVE_REQUISITION");
    List<OrderDto> orders = buildOrders(releasedRequisitions, userDto);

    this.orderFulfillmentService.create(orders);
  }

  /**
   * Builds orders list.
   */
  public List<OrderDto> buildOrders(List<RequisitionDto> releasedRequisitions,
                                    UserDto user) {
    List<OrderDto> orders = new ArrayList<>();
    for (RequisitionDto requisition : releasedRequisitions) {
      orders.add(orderDtoBuilder.build(requisition, user));
    }
    return orders;
  }

  /**
   * Releases the list of given requisitions as orders.
   *
   * @param convertToOrderDtos list of requisitions with their supplyingDepots.
   * @return list of released requisitions
   */
  private List<RequisitionDto> releaseRequisitionsAsOrder(
      List<ReleasableRequisitionDto> convertToOrderDtos, RequisitionDto requisition) {
    Profiler profiler = new Profiler("RELEASE_REQUISITIONS_AS_ORDER");
    profiler.setLogger(LOGGER);
    profiler.start("GET_ORDERS_EDIT_RIGHT_DTO");

    List<RequisitionDto> releasedRequisitions = new ArrayList<>();

    profiler.start("RELEASE");
    for (ReleasableRequisitionDto convertToOrderDto : convertToOrderDtos) {

      convertToOrderDto.setRequisitionId(requisition.getId());
      requisition.release(authenticationHelper.getCurrentUser().getId());

      LOGGER.info("save status change");
      LOGGER.info("order requisitions= {}", convertToOrderDto);
      LOGGER.info("requisitionId: {}", requisition.getId());
      if (!statusChangeRepository
          .verifyRequisitionStatusExists(
              requisition.getId(),
              RELEASED_STATUS)) {
        StatusChange statusChange = new StatusChange();
        statusChange.setRequisitionId(requisition.getId());
        statusChange.setAuthorId(authenticationHelper.getCurrentUser().getId());
        statusChange.setSupervisoryNodeId(requisition.getSupervisoryNode());
        statusChange.setStatus(RELEASED_STATUS);
        statusChange.setCreatedDate(ZonedDateTime.now());
        statusChangeRepository.saveStatusChange(statusChange);

      }

      UUID facilityId = convertToOrderDto.getSupplyingDepotId();
      requisition.setSupplyingFacilityId(facilityId);

      releasedRequisitions.add(requisition);
    }

    profiler.stop().log();
    return releasedRequisitions;
  }

}
