/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.requisition;

import java.util.Collections;
import java.util.UUID;
import org.openlmis.common.dto.RequisitionApprovalFinalResponseDto;
import org.openlmis.common.dto.referencedata.SupervisoryNodeDto;
import org.openlmis.common.dto.referencedata.UserDto;
import org.openlmis.common.dto.requisition.RequisitionDto;
import org.openlmis.common.exception.NotFoundException;
import org.openlmis.common.i18n.MessageKeys;
import org.openlmis.common.service.BaseCommunicationService;
import org.openlmis.common.service.referencedata.SupervisoryNodeReferenceDataService;
import org.openlmis.common.util.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RequisitionApprovalService
    extends BaseCommunicationService<RequisitionDto> {

  private static final Logger LOGGER = LoggerFactory.getLogger(RequisitionApprovalService.class);

  private final RestTemplate restTemplate = new RestTemplate();

  @Value("${requisition.url}")
  private String requisitionUrl;

  /**
   * Sends a POST request to the specified endpoint to authorize a requisition.
   */
  public RequisitionDto postRequisition(UUID requisitionId,
                                        UUID token,
                                        String endPoint) {
    String constructedUrl = String.format("%s%s%s%s?access_token=%s",
        getServiceUrl(), getUrl(), requisitionId, endPoint, token);

    LOGGER.info("Constructed URL: {}", constructedUrl);

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
    headers.setBearerAuth(token.toString());

    LOGGER.info("Request Headers: {}", headers);

    HttpEntity<String> entity = new HttpEntity<>(headers);
    ResponseEntity<RequisitionDto> response = restTemplate.exchange(
        constructedUrl, HttpMethod.POST, entity, RequisitionDto.class
    );

    RequisitionDto responseBody = response.getBody();
    LOGGER.info("Response Body: {}", responseBody);

    return responseBody;
  }

  /**
   * Approves the requisition by performing the necessary checks and updates.
   */
  public RequisitionApprovalFinalResponseDto approveRequisition(
      UUID requisitionId, UserDto userDto,
      SupervisoryNodeReferenceDataService supervisoryNodeReferenceDataService,
      RequisitionDto requisition, RequisitionService requisitionService) {

    LOGGER.debug("Approve requisition with ID: {}", requisitionId);

    SupervisoryNodeDto supervisoryNode = supervisoryNodeReferenceDataService
        .findSupervisoryNode(
        requisition.getProgram().getId(),
        requisition.getFacility().getId()
    );

    if (supervisoryNode != null) {
      SupervisoryNodeDto supervisoryNodeP = getParentSupervisoryNode(
          supervisoryNode, supervisoryNodeReferenceDataService);
      SupervisoryNodeDto supervisoryNodePP = getParentSupervisoryNode(
          supervisoryNodeP, supervisoryNodeReferenceDataService);

      UUID supplyingFacilityId = supervisoryNodePP.getFacility().getId();

      requisitionService.finalApproval(requisition, supervisoryNode, userDto,
          supplyingFacilityId,
          supervisoryNodePP.getId());

      RequisitionApprovalFinalResponseDto responseDto = new
          RequisitionApprovalFinalResponseDto();
      responseDto.setRequisitionId(requisitionId);
      responseDto.setSupervisoryNodeId(supervisoryNodePP.getId());

      return responseDto;
    } else {
      throw new NotFoundException(
          new Message(MessageKeys.ERROR_ORDER_NOT_FOUND, requisitionId));
    }
  }

  /**
   * Returns the parent node of the given node, or the node itself if no parent exists.
   *
   * @param supervisoryNode The node to get the parent for.
   * @param reference The service to fetch node details.
   * @return The parent node or the node itself.
   */
  public SupervisoryNodeDto getParentSupervisoryNode(
      SupervisoryNodeDto supervisoryNode,
      SupervisoryNodeReferenceDataService reference) {

    if (supervisoryNode.getParentNode() == null) {
      return reference.findOne(supervisoryNode.getId());
    } else {
      return reference.findOne(supervisoryNode.getParentNode().getId());
    }
  }

  @Override
  protected String getServiceUrl() {
    return requisitionUrl;
  }

  @Override
  protected String getUrl() {
    return "/api/requisitions/";
  }

  @Override
  protected Class<RequisitionDto> getResultClass() {
    return RequisitionDto.class;
  }

  @Override
  protected Class<RequisitionDto[]> getArrayResultClass() {
    return RequisitionDto[].class;
  }

  @Override
  protected String getServiceName() {
    return "Requisition";
  }

}
