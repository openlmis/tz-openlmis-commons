/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.openlmis.common.domain.FacilityTypeExtension;
import org.openlmis.common.repository.FacilityTypeExtensionRepository;
import org.openlmis.common.response.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service layer for handling business logic related to FacilityTypeExtension entities.
 */
@Service
public class FacilityTypeExtensionService {

  private final FacilityTypeExtensionRepository repository;

  @Autowired
  public FacilityTypeExtensionService(FacilityTypeExtensionRepository repository) {
    this.repository = repository;
  }

  /**
   * Saves or updates a FacilityTypeExtension.
   * Checks if an extension with the given FacilityTypeId already exists:
   * - If it exists, updates the record.
   * - If it does not exist, creates a new one.
   *
   * @param extension The FacilityTypeExtension to save or update.
   * @return The saved or updated FacilityTypeExtension.
   */
  public FacilityTypeExtension save(FacilityTypeExtension extension) {
    List<FacilityTypeExtension> existingExtensions = repository
        .findByFacilityTypeId(extension.getFacilityTypeId());
    if (!existingExtensions.isEmpty()) {
      // Assuming there is only one extension per FacilityTypeId; update the first one.
      FacilityTypeExtension existingExtension = existingExtensions.get(0);
      existingExtension.setReleaseOrderAfterAuthorization(extension
          .getReleaseOrderAfterAuthorization());
      return repository.save(existingExtension);
    }
    // Create a new FacilityTypeExtension if none exists for the FacilityTypeId
    return repository.save(extension);
  }

  /**
   * Retrieves a list of FacilityTypeExtensions by FacilityTypeId.
   *
   * @param facilityTypeId The FacilityTypeId to search by.
   * @return List of FacilityTypeExtensions matching the given FacilityTypeId.
   */
  public List<FacilityTypeExtension> getByFacilityTypeId(UUID facilityTypeId) {
    return repository.findByFacilityTypeId(facilityTypeId);
  }

  /**
   * Updates an existing FacilityTypeExtension by its ID.
   *
   * @param id The ID of the FacilityTypeExtension to update.
   * @param updatedExtension The new values for the extension.
   * @return The updated FacilityTypeExtension.
   */
  public FacilityTypeExtension update(UUID id, FacilityTypeExtension updatedExtension) {
    Optional<FacilityTypeExtension> existingExtension = repository.findById(id);
    if (existingExtension.isPresent()) {
      FacilityTypeExtension extension = existingExtension.get();
      extension.setFacilityTypeId(updatedExtension.getFacilityTypeId());
      extension.setReleaseOrderAfterAuthorization(updatedExtension
          .getReleaseOrderAfterAuthorization());
      return repository.save(extension);
    }
    throw new DataException("FacilityTypeExtension not found");
  }

  /**
   * Retrieves all FacilityTypeExtensions.
   *
   * @return A list of all FacilityTypeExtensions.
   */
  public List<FacilityTypeExtension> getAll() {
    return repository.findAll();
  }

}
