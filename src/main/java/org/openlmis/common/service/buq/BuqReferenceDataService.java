/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.buq;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.openlmis.common.dto.buq.BottomUpQuantificationDto;
import org.openlmis.common.service.RequestParameters;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BuqReferenceDataService
    extends BaseBuqService<BottomUpQuantificationDto> {
  @Override
  protected String getUrl() {
    return "/api/bottomUpQuantifications/";
  }

  @Override
  protected Class<BottomUpQuantificationDto> getResultClass() {
    return BottomUpQuantificationDto.class;
  }

  @Override
  protected Class<BottomUpQuantificationDto[]> getArrayResultClass() {
    return BottomUpQuantificationDto[].class;
  }

  @Override
  public List<BottomUpQuantificationDto> findAll() {
    return getPage(RequestParameters.init()).getContent();
  }

  /**
   * This method retrieves BUQ for given ids.
   *
   * @param buqIds list of buq ids.
   * @return List of BottomUpQuantificationDto with similar ids.
   */
  public List<BottomUpQuantificationDto> search(Set<UUID> buqIds) {
    return getPage(RequestParameters.init().set("id", buqIds)).getContent();
  }

}
