/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.buq;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.openlmis.common.dto.buq.BottomUpQuantificationDto;
import org.openlmis.common.dto.csv.BottomUpQuantificationLineItemCsv;
import org.openlmis.common.dto.referencedata.FacilityDto;
import org.openlmis.common.dto.referencedata.OrderableDto;
import org.openlmis.common.exception.ContentNotFoundMessageException;
import org.openlmis.common.service.referencedata.FacilityReferenceDataService;
import org.openlmis.common.service.referencedata.OrderableReferenceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BottomUpQuantificationService {

  @Autowired
  private OrderableReferenceDataService orderableReferenceDataService;

  @Autowired
  private FacilityReferenceDataService facilityReferenceDataService;

  @Autowired
  private CsvService csvService;

  /**
   * Prepares data for downloading.
   *
   * @param bottomUpQuantification BottomUpQuantification containing data to be downloaded.
   * @return byte array containing the data to be downloaded.
   * @throws java.io.IOException I/O exception
   */
  public byte[] getPreparationFormData(BottomUpQuantificationDto bottomUpQuantification)
      throws IOException {
    FacilityDto facility = findFacility(bottomUpQuantification.getFacilityId());
    List<BottomUpQuantificationLineItemCsv> csvLineItems = bottomUpQuantification
        .getBottomUpQuantificationLineItems()
        .stream()
        .map(lineItem -> {
          OrderableDto dto = findOrderable(lineItem.getOrderableId());
          String buqCategoryName = dto.getExtraData()
              .get("BUQ_CATEGORY_NAME");
          String buqOrderableClass = dto.getExtraData()
              .get("BUQ_ORDERABLE_CLASS");
          return new BottomUpQuantificationLineItemCsv(
              facility.getCode(),
              facility.getName(),
              facility.getType().getName(),
              facility.getGeographicZone().getName(),
              dto.getProductCode(),
              dto.getFullProductName(),
              buqCategoryName,
              buqOrderableClass,
              dto.getNetContent(),
              lineItem.getAnnualAdjustedConsumption(),
              lineItem.getVerifiedAnnualAdjustedConsumption(),
              lineItem.getForecastedDemand(),
              (!dto.getPrograms().isEmpty()) ? dto.getPrograms().get(0).getPricePerPack() : null,
              lineItem.getTotalCost(),
              (lineItem.getRemark() != null) ? lineItem.getRemark().getName() : null
          );
        })
        .collect(Collectors.toList());

    return csvService.generateCsv(csvLineItems, BottomUpQuantificationLineItemCsv.class);
  }

  private OrderableDto findOrderable(UUID orderableId) {
    return findResource(orderableId, orderableReferenceDataService::findOne
    );
  }

  private FacilityDto findFacility(UUID facilityId) {
    return findResource(facilityId, facilityReferenceDataService::findOne
    );
  }

  private <R> R findResource(UUID id, Function<UUID, R> finder) {
    return Optional
        .ofNullable(finder.apply(id))
        .orElseThrow(() -> new ContentNotFoundMessageException("orderable not found", id)
        );
  }

}
