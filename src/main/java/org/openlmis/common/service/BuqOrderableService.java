/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.openlmis.common.domain.BuqOrderable;
import org.openlmis.common.domain.BuqOrderableCategory;
import org.openlmis.common.dto.BuqOrderableCategoryDto;
import org.openlmis.common.dto.referencedata.OrderableDto;
import org.openlmis.common.exception.NotFoundException;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.i18n.BuqProductCategoryMessageKeys;
import org.openlmis.common.repository.BuqOrderableCategoryRepository;
import org.openlmis.common.repository.BuqOrderableRepository;
import org.openlmis.common.service.referencedata.OrderableReferenceDataService;
import org.openlmis.common.util.Message;
import org.openlmis.common.util.UuidUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;


@Service
@Validated
@RequiredArgsConstructor
public class BuqOrderableService {

  private final BuqOrderableCategoryRepository buqOrderableCategoryRepository;
  private final BuqOrderableRepository buqOrderableRepository;
  private final PermissionService permissionService;
  private final OrderableReferenceDataService orderableService;

  /**
   * Create a new buq product mqppint using the specified details. If the mapping exists
   * it will updated.
   *
   * @param orderableCode  orderable code
   * @param categoryCode   category code
   * @param orderableClass orderable class
   */
  public void createBuqOrderable(String orderableCode, String categoryCode, String orderableClass) {
    List<OrderableDto> orderables = orderableService.findByCode(orderableCode.trim());
    BuqOrderableCategory category = getCategoryByCode(categoryCode.trim());

    for (OrderableDto orderable : orderables) {
      //the result of search by code may return orderable containing part of the code which
      //must be ignored and process the exact match
      if (!orderable.getProductCode().equalsIgnoreCase(orderableCode)) {
        continue;
      }

      BuqOrderable buqOrderable = BuqOrderable.newBuqOrderable(orderable.getId(), category,
          orderableClass);
      buqOrderable = buqOrderableRepository.save(buqOrderable);
      orderable.getExtraData().put("BUQ_CATEGORY_CODE", categoryCode);
      orderable.getExtraData().put("BUQ_CATEGORY_NAME", category.getName());
      orderable.getExtraData().put("BUQ_ORDERABLE_CLASS", orderableClass);

      orderableService.updateOrderable(orderable);
    }

  }

  /**
   * Create a new one or update if existing a buq product category using the specified details.
   *
   * @param categoryDto details of the buqProductCategory .
   * @return creatd or updated category.
   */
  @Transactional
  public BuqOrderableCategory createOrUpdateCategory(
      @Valid BuqOrderableCategoryDto categoryDto) {
    permissionService.checkPermission(PermissionService.ORDERABLES_MANAGE);

    Optional<BuqOrderableCategory> existing =
        buqOrderableCategoryRepository.findOneByNameEqualsIgnoreCase(categoryDto.getName());
    if (!existing.isPresent()) {
      existing =
          buqOrderableCategoryRepository.findOneByCodeEqualsIgnoreCase(categoryDto.getCode());
    }

    if (existing.isPresent()) {
      UUID id = existing.get().getId();
      categoryDto.setId(id);
      return updateCategory(id, categoryDto);
    } else {
      return createCategory(categoryDto);
    }

  }

  /**
   * Create a new buq product category using the specified details.
   *
   * @param importer details of the new buqProductCategory .
   * @return create category.
   */
  @Transactional
  public BuqOrderableCategory createCategory(BuqOrderableCategory.Importer importer) {
    permissionService.checkPermission(PermissionService.ORDERABLES_MANAGE);

    BuqOrderableCategory buqOrderableCategory = BuqOrderableCategory.newInstance(importer);
    buqOrderableCategory.setId(null);
    buqOrderableCategory = buqOrderableCategoryRepository.save(buqOrderableCategory);
    return buqOrderableCategory;
  }

  /**
   * Update specified buq product category.
   *
   * @param id       the id of the  to be updated.
   * @param importer the imported with new details for the .
   * @return updated category.
   */
  @Transactional
  public BuqOrderableCategory updateCategory(UUID id, BuqOrderableCategory.Importer importer) {
    permissionService.checkPermission(PermissionService.ORDERABLES_MANAGE);

    if (!UuidUtil.sameId(importer.getId(), id)) {
      throw new ValidationMessageException(
          new Message(BuqProductCategoryMessageKeys.ERROR_ID_MISMATCH)
      );
    }

    if (!buqOrderableCategoryRepository.existsById(id)) {
      return createCategory(importer);
    } else {
      BuqOrderableCategory buqOrderableCategory = BuqOrderableCategory.newInstance(importer);
      buqOrderableCategory.setId(id);
      buqOrderableCategory = buqOrderableCategoryRepository.save(buqOrderableCategory);

      return buqOrderableCategory;
    }
  }


  /**
   * Retrieve buq product category of specified Id.
   *
   * @param buqProductCategoryId id of the buq product category to retrieve.
   * @return
   */
  public BuqOrderableCategory getCategory(UUID buqProductCategoryId) {
    BuqOrderableCategory buqOrderableCategory = buqOrderableCategoryRepository
        .findById(buqProductCategoryId)
        .orElseThrow(
            () -> new NotFoundException(new Message(BuqProductCategoryMessageKeys.ERROR_NOT_FOUND))
        );
    return buqOrderableCategory;
  }

  /**
   * Retrieve buq product category of specified code.
   *
   * @param categoryCode code of the buq product category to retrieve.
   * @return
   */
  public BuqOrderableCategory getCategoryByCode(String categoryCode) {
    BuqOrderableCategory buqOrderableCategory = buqOrderableCategoryRepository
        .findOneByCodeEqualsIgnoreCase(categoryCode)
        .orElseThrow(
            () -> new NotFoundException(new Message(BuqProductCategoryMessageKeys.ERROR_NOT_FOUND))
        );
    return buqOrderableCategory;
  }

  /**
   * Find all available buq product categories paginated.
   *
   * @param pageable pagination details.
   * @return
   */
  public Page<BuqOrderableCategory> findAllCategories(Pageable pageable) {
    return buqOrderableCategoryRepository.findAll(pageable);
  }

  /**
   * Search buq product categories based on the name filter.
   *
   * @param name     name search parameter value.
   * @param pageable pagination information.
   * @return a page of found buq product category which conforms the search parameters.
   */
  public Page<BuqOrderableCategory> searchCategories(String name, Pageable pageable) {
    return buqOrderableCategoryRepository.findAllByNameContainingIgnoreCase(name, pageable);
  }

}
