/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service;

import static org.openlmis.common.i18n.MessageKeys.ERROR_JASPER_REPORT_CREATION_WITH_MESSAGE;
import static org.openlmis.common.i18n.MessageKeys.NOT_FOUND;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.sql.Connection;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.openlmis.common.domain.JasperExporter;
import org.openlmis.common.domain.JasperPdfExporter;
import org.openlmis.common.domain.JasperTemplate;
import org.openlmis.common.exception.ReportingException;
import org.openlmis.common.i18n.MessageKeys;
import org.slf4j.profiler.Profiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

@Service
public class JasperReportsViewService {

  static final String PARAM_DATASOURCE = "datasource";

  @Autowired
  private static DataSource replicationDataSource;

  private final String templatePath = "/jasperTemplates/stockOutNotification.jrxml";

  private final String msdLogo = "images/msd-logo.jpeg";

  private final String msdSignature = "images/msd-signature.png";

  /**
   * Generate a report based on the Jasper template.
   * Create compiled report from bytes from Template entity, and use compiled report to fill in data
   * and export to desired format.
   *
   * @param jasperTemplate template that will be used to generate a report
   * @param params         map of parameters
   *
   * @return data of generated report
   */
  public byte[] generateReport(JasperTemplate jasperTemplate,
                               Map<String, Object> params)
      throws ReportingException {

    JasperReport jasperReport = getReportFromTemplateData(jasperTemplate);

    try {
      JasperPrint jasperPrint;
      if (params.containsKey(PARAM_DATASOURCE)) {
        jasperPrint = JasperFillManager.fillReport(jasperReport, params,
            new JRBeanCollectionDataSource((List) params.get(PARAM_DATASOURCE)));
      } else {
        try (Connection connection = replicationDataSource.getConnection()) {
          jasperPrint = JasperFillManager.fillReport(jasperReport, params,
              connection);
        }
      }

      return prepareReport(jasperPrint, params);
    } catch (Exception e) {
      throw new ReportingException(e, ERROR_JASPER_REPORT_CREATION_WITH_MESSAGE,
          e.getMessage());
    }
  }

  /**
   * Get (compiled) Jasper report from Jasper template.
   *
   * @param jasperTemplate template
   *
   * @return Jasper report
   */
  private JasperReport getReportFromTemplateData(JasperTemplate jasperTemplate)
      throws ReportingException {

    try (ObjectInputStream inputStream =
             new ObjectInputStream(new ByteArrayInputStream(jasperTemplate.getData()))) {

      return (JasperReport) inputStream.readObject();
    } catch (IOException ex) {
      throw new ReportingException(ex, MessageKeys.ERROR_IO, ex.getMessage());
    } catch (ClassNotFoundException ex) {
      throw new ReportingException(ex, NOT_FOUND, JasperReport.class.getName());
    }
  }

  private byte[] prepareReport(JasperPrint jasperPrint, Map<String, Object> params)
      throws JRException {
    if (null != params.get("format")) {
      return getJasperExporter((String) params.get("format"), jasperPrint).exportReport();
    }

    return getJasperExporter("pdf", jasperPrint).exportReport();
  }

  private JasperExporter getJasperExporter(String keyParam, JasperPrint jasperPrint) {
    if ("pdf".equals(keyParam)) {
      return new JasperPdfExporter(jasperPrint);
    }
    throw new IllegalArgumentException(keyParam);
  }


  /**
   * Generate a report based on the Jasper template.
   * Create compiled report from bytes from Template entity, and use compiled report to fill in data
   * and export to pdf format.
   *
   * @param params map of parameters
   */
  public void generateNotification(Map<String, Object> params,
                                   HttpServletResponse response,
                                   Profiler profiler)
      throws ReportingException {

    try {
      ClassLoader classLoader = getClass().getClassLoader();

      params.put("msdLogo", String.valueOf(classLoader.getResource(msdLogo)));

      params.put("signature", String.valueOf(classLoader.getResource(msdSignature)));

      profiler.start("GENERATE JASPER REPORT");

      response.setContentType(MediaType.APPLICATION_PDF_VALUE);
      response.setHeader("Content-disposition",
          "inline; filename=stock_out_notification_report.pdf");

      JRBeanCollectionDataSource source =
          new JRBeanCollectionDataSource((List) params.get(PARAM_DATASOURCE));

      JasperPrint jasperPrint = JasperFillManager
          .fillReport(loadTemplate(profiler), params, source);

      profiler.start("EXPORT TO PDF");
      JasperExportManager.exportReportToPdfStream(jasperPrint,
          response.getOutputStream());

      profiler.stop();
    } catch (JRException | IOException e) {

      throw new ReportingException(e, MessageKeys.ERROR_IO, e.getMessage());

    }
  }

  /**
   * load the template.
   *
   * @return the compiled jasper report
   */
  private JasperReport loadTemplate(Profiler profiler) throws JRException {

    profiler.start("GET_JRXML_FILE");
    InputStream stream = this.getClass().getResourceAsStream(templatePath);

    return JasperCompileManager.compileReport(stream);

  }
}