/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.response;


import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashMap;
import java.util.Map;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;


public class CustomRestResponse {
  
  private static final String ERROR = "error";
  private static final String SUCCESS = "success";
  private static final String HEADER_CONTENT_TYPE = "Content-Type";
  private final Map<String, Object> data = new HashMap<>();
  
  public CustomRestResponse() {
  }
  
  public CustomRestResponse(String key, Object data) {
    this.data.put(key, data);
  }
  
  public CustomRestResponse(String code) {
  }
  
  public static ResponseEntity<CustomRestResponse> success(String successMessage) {
    return response(SUCCESS, successMessage, HttpStatus.OK, APPLICATION_JSON_VALUE);
  }
  
  public static ResponseEntity<CustomRestResponse> error(String errorMessage,
                                                         HttpStatus statusCode) {
    return response(ERROR, errorMessage, statusCode, APPLICATION_JSON_VALUE);
  }
  
  private static ResponseEntity<CustomRestResponse> response(
      String key, String message, HttpStatus statusCode, String contentType) {
    MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
    headers.add(HEADER_CONTENT_TYPE, contentType);
    return new ResponseEntity<>(new CustomRestResponse(key, message), headers, statusCode);
  }
  
  @JsonAnySetter
  public void addData(String key, Object data) {
    this.data.put(key, data);
  }
  
  @JsonAnyGetter
  public Map<String, Object> getData() {
    return data;
  }
  
  public ResponseEntity<CustomRestResponse> successEntity(String successMessage) {
    addData(SUCCESS, successMessage);
    return buildResponse(HttpStatus.OK);
  }
  
  public ResponseEntity<CustomRestResponse> errorEntity(String errorMessage,
                                                        HttpStatus statusCode) {
    addData(ERROR, errorMessage);
    return buildResponse(statusCode);
  }
  
  private ResponseEntity<CustomRestResponse> buildResponse(HttpStatus status) {
    MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
    headers.add(HEADER_CONTENT_TYPE, APPLICATION_JSON_VALUE);
    return new ResponseEntity<>(this, headers, status);
  }
  
  @JsonIgnore
  public String getErrorMsg() {
    return (String) data.get(ERROR);
  }
  
  @JsonIgnore
  public String getSuccessMsg() {
    return (String) data.get(SUCCESS);
  }
}