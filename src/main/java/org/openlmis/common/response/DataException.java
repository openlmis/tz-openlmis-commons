/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.response;

import lombok.Getter;

public class DataException extends RuntimeException {
  
  @Getter
  private CustomRestResponse restResponse;
  
  public DataException(String code) {
    restResponse = new CustomRestResponse(code);
  }
  
  /**
   * Receives code and object for processing.
   */
  public DataException(String code, Object... params) {
    StringBuilder stringParams = new StringBuilder();
    String delimiter = "#"; // Choose a delimiter other than '#', if necessary
    int paramsLength = params.length;
    for (int i = 0; i < paramsLength; i++) {
      if (params[i] != null) {
        stringParams.append(params[i]);
        if (i < paramsLength - 1) {
          stringParams.append(delimiter);
        }
      }
    }
    restResponse = new CustomRestResponse(code, stringParams.toString());
  }
  
  public DataException(CustomRestResponse customRestResponse) {
    this.restResponse = customRestResponse;
  }
  
  @Override
  public String toString() {
    return restResponse.toString();
  }
  
  @Deprecated
  @Override
  public String getMessage() {
    return restResponse.toString();
  }
  
  
}
