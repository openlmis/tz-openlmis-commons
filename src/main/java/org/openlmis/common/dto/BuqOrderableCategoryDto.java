/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto;

import java.util.Map;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.codehaus.jackson.map.ObjectMapper;
import org.openlmis.common.domain.BuqOrderableCategory;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class BuqOrderableCategoryDto extends BaseDto
    implements BuqOrderableCategory.Importer, BuqOrderableCategory.Exporter {

  @NotBlank
  private String code;

  @NotBlank
  private String name;

  private String description;

  /**
   * Creates a new BuqProductCategory DTO from the entity.
   *
   * @param buqOrderableCategory the buq product category to export into DTO
   * @return created BuqProductCategory DTO
   */
  public static BuqOrderableCategoryDto newInstance(BuqOrderableCategory buqOrderableCategory) {
    BuqOrderableCategoryDto categoryDto = new BuqOrderableCategoryDto();
    buqOrderableCategory.export(categoryDto);
    return categoryDto;
  }

  /**
   * Creates a new BuqProductCategoryDTO from the map.
   *
   * @param data the buq product category data Map to export into DTO
   * @return created BuqProductCategory DTO
   */
  public static BuqOrderableCategoryDto newInstance(Map<String, String> data) {
    final BuqOrderableCategoryDto dto = new ObjectMapper()
        .convertValue(data, BuqOrderableCategoryDto.class);

    return dto;
  }
}
