/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto.buq;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
public final class BottomUpQuantificationDto {

  @Getter
  @Setter
  private UUID facilityId;

  @Getter
  @Setter
  private UUID programId;

  @Getter
  @Setter
  private UUID processingPeriodId;

  @Getter
  @Setter
  @JsonFormat(shape = STRING)
  private ZonedDateTime createdDate;

  @Getter
  @Setter
  @JsonFormat(shape = STRING)
  private ZonedDateTime modifiedDate;

  @Getter
  @Setter
  private Integer targetYear;

  @Getter
  @Setter
  private BottomUpQuantificationStatus status;

  @Setter
  @Getter
  private List<BottomUpQuantificationLineItemDto> bottomUpQuantificationLineItems;

  @Setter
  @Getter
  private List<BottomUpQuantificationStatusChangeDto> statusChanges;

  @Getter
  @Setter
  private BottomUpQuantificationFundingDetailsDto fundingDetails;

  @Getter
  @Setter
  private UUID supervisoryNodeId;

}