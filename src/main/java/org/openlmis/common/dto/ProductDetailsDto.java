/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto;

import lombok.Getter;
import lombok.Setter;
import org.openlmis.common.dto.referencedata.OrderableDto;

@Getter
@Setter
public class ProductDetailsDto {

  private String productCode;
  private String revision;
  private String uom;

  /**
   * Constructs a ProductDetailsDto.
   *
   * @param productCode the code of the product
   * @param revision    the revision of the product
   * @param uom         the unit of measure of the product
   */
  public ProductDetailsDto(String productCode,
                           String revision,
                           String uom) {
    this.productCode = productCode;
    this.revision = revision;
    this.uom = uom;
  }

  /**
   * Constructs a ProductDetailsDto.
   *
   * @param productDto the code of the product
   */
  public static ProductDetailsDto processProductCode(OrderableDto
                                                   productDto) {
    String input = productDto.getProductCode();
    String productCode = "";
    String revision = "";
    String uom = "";

    if (input.contains("-")) {
      // Split the input string by the hyphen
      String[] parts = input.split("-");
      productCode = parts[0];
      revision = parts[1];
      uom = parts[2];
    } else {
      int i = 0;
      while (i < input.length() && Character.isDigit(input.charAt(i))) {
        i++;
      }
      productCode = input.substring(0, i);
      revision = input.substring(i);
      uom = productDto.getDispensable().getDispensingUnit();
    }

    return new ProductDetailsDto(productCode, revision, uom);
  }

}
