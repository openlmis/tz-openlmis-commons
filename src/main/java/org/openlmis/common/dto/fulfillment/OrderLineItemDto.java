/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto.fulfillment;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.common.dto.ProductDetailsDto;
import org.openlmis.common.dto.referencedata.OrderableDto;
import org.openlmis.common.dto.requisition.RequisitionLineItemDto;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderLineItemDto {

  private UUID id;

  private OrderableDto orderable;

  private Long orderedQuantity;

  private Long totalDispensingUnits;

  private Map<String, Object> extraData;

  @Setter
  private OrderDto order;

  /**
   * Static factory method for constructing new OrderLineItem based on RequisitionLineItem.
   *
   * @param lineItem RequisitionLineItem to create instance from.
   */
  public static OrderLineItemDto newOrderLineItem(
      RequisitionLineItemDto lineItem, OrderableDto productDto) {

    OrderLineItemDto orderLineItem = new OrderLineItemDto();
    orderLineItem.setOrderedQuantity(lineItem.getPacksToShip());

    if (orderLineItem.getExtraData() == null) {
      orderLineItem.setExtraData(new HashMap<>());
    }

    if (productDto.getExtraData() == null) {
      productDto.setExtraData(new HashMap<>());
    }

    productDto.getExtraData()
        .put("approvedQuantity", lineItem
            .getApprovedQuantity().toString());

    productDto.getExtraData().put(
        "amc",
        lineItem.getAverageConsumption() != null
            ? lineItem.getAverageConsumption().toString() : null);
    //convert product details
    ProductDetailsDto product = ProductDetailsDto
        .processProductCode(productDto);
    productDto.getExtraData()
        .put("revision", product.getRevision());
    productDto.getExtraData()
        .put("uom", product.getUom());
    productDto.getExtraData()
        .put("description", productDto
            .getFullProductName());

    orderLineItem.setOrderable(productDto);

    orderLineItem.getExtraData().put("productCode", product.getProductCode());
    orderLineItem.getExtraData().put("approvedQuantity", lineItem.getApprovedQuantity());
    orderLineItem.getExtraData().put("amc", lineItem.getAverageConsumption() != null
            ? lineItem.getAverageConsumption() : null);
    orderLineItem.getExtraData().put("revision", product.getRevision());
    orderLineItem.getExtraData().put("uom", product.getUom());
    orderLineItem.getExtraData().put("description", productDto.getFullProductName());

    return orderLineItem;
  }

  public String getOrderableDispensingUnit() {
    return getOrderable().getDispensable().getDispensingUnit();
  }

  public String getOrderableDisplayUnit() {
    return getOrderable().getDispensable().getDisplayUnit();
  }

  public String getProductCode() {
    return getOrderable().getProductCode();
  }

  public String getProductName() {
    return getOrderable().getFullProductName();
  }

}