/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto.fulfillment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.jxpath.JXPathContext;
import org.openlmis.common.dto.referencedata.FileColumnDto;

/**
 * OrderPathContext.
 */

@Getter
@Setter
@Builder
@AllArgsConstructor
public class OrderPathContext {
  private final  JXPathContext orderContext;
  private final JXPathContext lineItemContext;
  private final JXPathContext lineItemOrderableContext;
  private final FileColumnDto fileColumn;
  private final JXPathContext erpOrderLineItemContext;
}
