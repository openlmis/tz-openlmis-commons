/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.common.dto.referencedata.FacilityDto;
import org.openlmis.common.dto.referencedata.ProgramDto;
import org.openlmis.common.dto.requisition.ProcessingPeriodDto;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"customerId", "hfrCode", "elmisOrderNumber"})
public abstract class AbstractStockOutNotificationDto {

  private String quoteNumber;

  @NotBlank
  private String customerId;

  @NotBlank
  private String customerName;

  @NotBlank
  private String hfrCode;

  @JsonProperty("facilityOrderNumber")
  @NotBlank
  private String elmisOrderNumber;

  @NotNull
  private LocalDate notificationDate;

  private LocalDate processingDate;

  @NotBlank
  private String zone;

  private String comment;

  public List<Map<String, Object>> rationingItems;

  public List<Map<String, Object>> stockOutItems;

  public List<Map<String, Object>> inSufficientFundingItems;

  public List<Map<String, Object>> fullFilledItems;

  public List<Map<String, Object>> closeToExpireItems;

  public List<Map<String, Object>> phasedOutItems;

  @Getter
  @Setter
  @NoArgsConstructor
  @EqualsAndHashCode(callSuper = true)
  public static class MsdStockOutNotificationDto extends AbstractStockOutNotificationDto {

  }

  @Getter
  @Setter
  @NoArgsConstructor
  @EqualsAndHashCode(callSuper = true)
  public static class StockOutNotificationDto extends AbstractStockOutNotificationDto {

    private UUID id;

    private ZonedDateTime createdDate;

    private ZonedDateTime modifiedDate;

    private FacilityDto facility;

    private ProgramDto program;

    private ProcessingPeriodDto processingPeriod;
  }


}
