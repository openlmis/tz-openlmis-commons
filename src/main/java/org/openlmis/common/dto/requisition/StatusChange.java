/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto.requisition;

import java.time.ZonedDateTime;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.openlmis.common.domain.BaseTimestampedEntity;
import org.openlmis.common.dto.StatusMessageDto;

@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StatusChange
    extends BaseTimestampedEntity {

  @Getter
  @Setter
  private RequisitionStatus status;

  @Getter
  @Setter
  private StatusMessageDto statusMessageDto;

  @Getter
  @Setter
  private UUID authorId;

  /**
   * Export this object to the specified exporter (DTO).
   *
   * @param exporter exporter to export to
   */
  public void export(StatusChange.Exporter exporter) {
    exporter.setCreatedDate(getCreatedDate());
    exporter.setStatus(status);
    exporter.setStatusMessage(statusMessageDto);
    exporter.setAuthorId(authorId);
  }

  public interface Exporter {

    void setCreatedDate(ZonedDateTime createdDate);

    void setStatus(RequisitionStatus status);

    void setStatusMessage(StatusMessageDto statusMessage);

    void setAuthorId(UUID authorId);
  }
}
