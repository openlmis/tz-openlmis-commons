/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.common.domain.MalariaValidation;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MalariaValidationDto extends BaseDto
    implements MalariaValidation.Importer, MalariaValidation.Exporter {

  private UUID requisitionId;
  private UUID productId;
  private String productCode;
  private String fullProductName;
  private String dispensingUnit;
  private Integer totalPatient;
  private Integer totalConsumption;

  /**
   * Creates new set of MalariaValidationDto based on
   * {@link MalariaValidation} iterable.
   */
  public static Set<MalariaValidationDto> newInstance(
      Iterable<MalariaValidation> iterable) {
    Set<MalariaValidationDto> logDtos = new HashSet<>();
    iterable.forEach(i -> logDtos.add(newInstance(i)));
    return logDtos;
  }

  /**
   * Creates new instance of MalariaValidationDto based on {@link MalariaValidation}.
   */
  public static MalariaValidationDto newInstance(MalariaValidation log) {
    MalariaValidationDto logDto = new MalariaValidationDto();
    log.export(logDto);
    return logDto;
  }

}
