/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto;

import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.common.domain.JasperTemplateParameter;
import org.openlmis.common.domain.JasperTemplateParameterDependency;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JasperTemplateParameterDependencyDto implements
        JasperTemplateParameterDependency.Importer, JasperTemplateParameter.Exporter {

  private UUID id;
  private String dependency;
  private String property;
  private String placeholder;

  /**
   * Create new instance of JasperTemplateParameterDependencyDto based on given
   * {@link JasperTemplateParameterDependency}.
   *
   * @param dependency instance of parameter dependency.
   * @return new instance of JasperTemplateParameterDependencyDto.
   */
  public static JasperTemplateParameterDependencyDto newInstance(
      JasperTemplateParameterDependency dependency) {
    JasperTemplateParameterDependencyDto dto = new JasperTemplateParameterDependencyDto();
    dependency.export(dto);
    return dto;
  }

  @Override
  public void setName(String name) {

  }

  @Override
  public void setDisplayName(String displayName) {

  }

  @Override
  public void setDefaultValue(String defaultValue) {

  }

  @Override
  public void setDataType(String dataType) {

  }

  @Override
  public void setSelectExpression(String selectExpression) {

  }

  @Override
  public void setSelectMethod(String selectMethod) {

  }

  @Override
  public void setSelectBody(String selectBody) {

  }

  @Override
  public void setDescription(String description) {

  }

  @Override
  public void setSelectProperty(String selectProperty) {

  }

  @Override
  public void setDisplayProperty(String displayProperty) {

  }

  @Override
  public void setRequired(Boolean required) {

  }

  @Override
  public void setOptions(List<String> options) {

  }

  @Override
  public void setDependencies(List<JasperTemplateParameterDependencyDto> dependencies) {

  }
}