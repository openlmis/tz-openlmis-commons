/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.openlmis.common.dto.fulfillment.ExternalStatus;
import org.openlmis.common.dto.fulfillment.OrderDto;
import org.openlmis.common.dto.fulfillment.OrderLineItemDto;
import org.openlmis.common.dto.fulfillment.StatusChangeDto;
import org.openlmis.common.dto.fulfillment.StatusMessage2Dto;
import org.openlmis.common.dto.fulfillment.StatusMessageDto;
import org.openlmis.common.dto.referencedata.FacilityDto;
import org.openlmis.common.dto.referencedata.OrderableDto;
import org.openlmis.common.dto.referencedata.UserDto;
import org.openlmis.common.dto.requisition.RequisitionDto;
import org.openlmis.common.dto.requisition.RequisitionLineItemDto;
import org.openlmis.common.dto.requisition.StatusChange;
import org.openlmis.common.repository.StatusChangeRepository;
import org.openlmis.common.service.referencedata.BaseReferenceDataService;
import org.openlmis.common.service.referencedata.FacilityReferenceDataService;
import org.openlmis.common.service.referencedata.OrderableReferenceDataService;
import org.openlmis.common.service.referencedata.PeriodReferenceDataService;
import org.openlmis.common.service.referencedata.ProgramReferenceDataService;
import org.openlmis.common.service.referencedata.UserReferenceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class OrderDtoBuilder extends BaseDtoBuilder {

  @Autowired
  @Qualifier("facilityReferenceDataService")
  private FacilityReferenceDataService facilities;

  @Autowired
  private PeriodReferenceDataService periods;

  @Autowired
  private StatusChangeRepository statusMessageRepository;

  @Autowired
  @Qualifier("programReferenceDataService")
  private ProgramReferenceDataService programs;

  @Autowired
  private OrderableReferenceDataService products;

  @Autowired
  private UserReferenceDataService userReferenceDataService;

  /**
   * Create a new instance of OrderDto based on data
   * from {@link org.openlmis.common.dto.requisition.RequisitionDto}.
   *
   * @param requisition instance used to create {@link OrderDto} (can be {@code null})
   * @return new instance of {@link OrderDto}. {@code null} if passed argument is {@code null}.
   */
  public OrderDto build(RequisitionDto requisition, UserDto user) {
    if (null == requisition) {
      return null;
    }

    OrderDto order = new OrderDto();
    order.setExternalId(requisition.getId());
    order.setEmergency(requisition.getEmergency());
    order.setFacility(getIfPresent(facilities, requisition.getFacility().getId()));
    order.setProcessingPeriod(getIfPresent(periods, requisition.getProcessingPeriod().getId()));
    order.setQuotedCost(BigDecimal.ZERO);

    order.setReceivingFacility(getIfPresent(facilities, requisition.getFacility().getId()));
    order.setRequestingFacility(getIfPresent(facilities, requisition.getFacility().getId()));

    order.setSupplyingFacility(getIfPresent(facilities, requisition.getSupplyingFacilityId()));
    order.setProgram(getIfPresent(programs, requisition.getProgram().getId()));
    order.setStatusMessages(getStatusMessages(requisition));

    StatusChange authorized = requisition.getAuthorizedStatusChange(
        requisition.getStatusHistory());

    StatusChange firstApproval = requisition.getDistrictApprovalStatusChange(
        requisition.getStatusHistory());

    StatusChange secondApproval = requisition.getRegionApprovalStatusChange(
        requisition.getStatusHistory());

    String authorizedBy = "";
    String districtApprovedBy = "";
    String regionApprovedBy = "";

    authorizedBy = getUserFullName(authorized.getAuthorId());
    districtApprovedBy = getUserFullName(firstApproval.getAuthorId());
    regionApprovedBy = getUserFullName(secondApproval.getAuthorId());

    FacilityDto supplyingFac = getIfPresent(facilities, requisition.getSupplyingFacilityId());

    if (order.getExtraData() == null) {
      order.setExtraData(new HashMap<>());
    }

    populateExtraData(order, requisition, authorizedBy,
        districtApprovedBy, regionApprovedBy, authorized,
        firstApproval, secondApproval, supplyingFac);

    Set<OrderableDto> orderableIdentities = requisition
        .getRequisitionLineItems()
        .stream()
        .map(RequisitionLineItemDto::getOrderable)
        .collect(Collectors.toSet());

    List<UUID> orderableIds = orderableIdentities.stream()
        .map(OrderableDto::getId)
        .collect(Collectors.toList());

    List<OrderableDto> orderables = products
        .findByIdies(orderableIds);

    Map<UUID, OrderableDto> orderablesMap = orderables.stream()
        .collect(Collectors.toMap(OrderableDto::getId, Function.identity()));

    order.setOrderLineItems(
        requisition
            .getRequisitionLineItems()
            .stream()
            .filter(line -> !line.isLineSkipped())
            .map(line -> {
              OrderableDto orderableDto = orderablesMap
                  .get(line.getOrderable().getId());
              return OrderLineItemDto.newOrderLineItem(line, orderableDto);
            })
            .collect(Collectors.toList())
    );

    List<StatusChangeDto> statusChanges = new ArrayList<>();
    for (StatusChange statusChange : requisition.getStatusHistory()) {
      StatusChangeDto statusChangeDto = new StatusChangeDto();
      statusChangeDto.setCreatedDate(statusChange.getCreatedDate());
      statusChangeDto.setStatus(ExternalStatus.RELEASED);
      statusChangeDto.setAuthorId(statusChange.getAuthorId());
      UserDto userDto = new UserDto();
      user.setId(statusChange.getAuthorId());
      statusChangeDto.setAuthor(userDto);
      statusChanges.add(statusChangeDto);
    }
    order.setStatusChanges(statusChanges);

    order.setCreatedBy(user);

    order.setLastUpdater(new ObjectReferenceDto(user.getId()));

    return order;
  }

  private List<StatusMessageDto> getStatusMessages(RequisitionDto requisition) {
    List<StatusMessageDto> statusMessageDtoList = new ArrayList<>();
    List<StatusMessage2Dto> statusMessages = statusMessageRepository
        .findByRequisitionId(
            requisition.getId());
    for (StatusMessage2Dto statusMessage : statusMessages) {
      StatusMessageDto statusMessageDto = new StatusMessageDto();
      statusMessageDto.setAuthorId(UUID
          .fromString(statusMessage.getAuthorId()));
      statusMessageDto.setAuthorFirstName(statusMessage.getAuthorFirstName());
      statusMessageDto.setAuthorLastName(statusMessage.getAuthorLastName());
      statusMessageDto.setRequisitionId(UUID
          .fromString(statusMessage.getRequisitionId()));
      statusMessageDto.setStatusChangeId(UUID
          .fromString(statusMessage.getStatusChangeId()));
      ExternalStatus status = ExternalStatus
          .fromString(statusMessage.getStatus());
      statusMessageDto.setStatus(status);
      statusMessageDto.setBody(statusMessage.getBody());
      statusMessageDto.setCreatedDate(ZonedDateTime.now());
      statusMessageDto.setId(null);
      statusMessageDtoList.add(statusMessageDto);
    }
    return statusMessageDtoList;
  }

  private <T> T getIfPresent(BaseReferenceDataService<T> service, UUID id) {
    return Optional.ofNullable(id).isPresent() ? service.findOne(id) : null;
  }

  /**
   * Helper method to get the full name of a user by their ID.
   *
   * @param authorId the ID of the user
   * @return the full name of the user, or an empty string if the ID is null
   */
  private String getUserFullName(UUID authorId) {
    if (authorId == null) {
      return "";
    }
    UserDto user = userReferenceDataService.findOne(authorId);
    return user.getFirstName() + " " + user.getLastName();
  }

  /**
   * Populates the extra data of the order.
   *
   * @param order          the order object
   * @param requisition    the requisition object
   * @param authorizedBy   the full name of the authorized person
   * @param districtApprovedByBy    the full name of the first approver
   * @param regionApprovedBy    the full name of the second approver
   * @param authorized     the authorization object
   * @param firstApproval  the first approval object
   * @param secondApproval the second approval object
   * @param supplyingFac   the supplying facility object
   */
  private void populateExtraData(OrderDto order,
                                 RequisitionDto requisition,
                                 String authorizedBy,
                                 String districtApprovedByBy,
                                 String regionApprovedBy,
                                 StatusChange authorized,
                                 StatusChange firstApproval,
                                 StatusChange secondApproval,
                                 FacilityDto supplyingFac) {

    order.getExtraData().put(PLANT,
        supplyingFac == null ? null : supplyingFac.getCode());
    order.getExtraData().put(COMPANY, COMPANY_VALUE);
    order.getExtraData().put(ELMIS_ORDER_NUM, order.getOrderCode());
    order.getExtraData().put(FACILITY_ID, "");
    order.getExtraData().put(CUSTOMER_ID,
        requisition.getFacility().getCode());
    order.getExtraData().put(CURRENCY, CURRENCY_VALUE);
    order.getExtraData().put(PERIOD,
        requisition.getProcessingPeriod().getEndDate().toString());
    order.getExtraData().put(QUOTE_DATE,
        LocalDate.now().toString());
    order.getExtraData().put(ERROR, "");
    order.getExtraData().put(QUOTE_NUMBER, "");
    order.getExtraData().put(STATUS, STATUS_PENDING);
    order.getExtraData().put(PRICE_LIST_CODE, " ");
    order.getExtraData().put(QUOTE_LINE, " ");
    order.getExtraData().put(F_AUTHORIZED_BY, authorizedBy);
    order.getExtraData().put(F_AUTHORIZED_DATE,
        (authorized == null)
            ? null
            : (authorized.getCreatedDate() != null
            ? authorized.getCreatedDate().toLocalDate().toString()
            : null));
    order.getExtraData().put(D_APPROVED_BY, (districtApprovedByBy == null)
        ? authorizedBy : districtApprovedByBy);
    order.getExtraData().put(
        D_APPROVED_DATE,
        (firstApproval == null)
            ? authorized.getCreatedDate().toLocalDate().toString()
            : (firstApproval.getCreatedDate() != null
            ? firstApproval.getCreatedDate().toLocalDate().toString()
            : null));
    order.getExtraData().put(R_APPROVED_BY, regionApprovedBy);
    order.getExtraData().put(R_APPROVED_DATE, LocalDate.now().toString());
    order.getExtraData().put(EMERGENCY, requisition.getEmergency().toString());
    order.getExtraData().put(RNR_ID, requisition.getId().toString());
    order.getExtraData().put(PROCESSED, "false");
  }

}
