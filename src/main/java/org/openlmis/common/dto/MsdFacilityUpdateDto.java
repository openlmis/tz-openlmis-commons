/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.common.domain.MsdFacilityUpdate;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"facilityNumber", "facilityCode"}, callSuper = true)
public class MsdFacilityUpdateDto extends BaseDto implements MsdFacilityUpdate.Exporter,
    MsdFacilityUpdate.Importer {

  @NotBlank
  private String facilityNumber;

  @NotBlank
  private String facilityCode;

  @NotBlank
  private String facilityName;

  @NotBlank
  private String facilityType;

  @NotBlank
  private String region;

  @NotBlank
  private String council;

  private String hfrCode;

  private Boolean status;

  private boolean applied;

  /**
   * Creates a new MsdFacilityUpdateDto DTO from the entity.
   *
   * @param update the msd facility update to export into DTO
   * @return created MsdFacilityUpdateDto DTO
   */
  public static MsdFacilityUpdateDto newInstance(MsdFacilityUpdate update) {
    MsdFacilityUpdateDto updateDto = new MsdFacilityUpdateDto();
    update.export(updateDto);
    return updateDto;
  }
}
