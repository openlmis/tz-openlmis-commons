/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto.csv;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.joda.money.Money;
import org.openlmis.common.util.MoneyDeserializer;
import org.openlmis.common.util.MoneySerializer;

@JsonPropertyOrder({
    "Facility Code",
    "Facility Name",
    "Facility Type",
    "District",
    "MSD Item Code",
    "Product Name",
    "Product Category",
    "Classes",
    "Unit of measure",
    "Adjusted Consumption (in Packs)",
    "Verified annual adjusted consumption",
    "Forecasted demand",
    "MSD Sales Price",
    "Total Cost",
    "Remarks"
})
@Getter
@RequiredArgsConstructor
public class BottomUpQuantificationLineItemCsv {

  @JsonProperty("Facility Code")
  private final String facilityCode;

  @JsonProperty("Facility Name")
  private final String facilityName;

  @JsonProperty("Facility Type")
  private final String facilityType;

  @JsonProperty("District")
  private final String district;

  @JsonProperty("MSD Item Code")
  private final String productCode;

  @JsonProperty("Product Name")
  private final String productName;

  @JsonProperty("Product Category")
  private final String orderableDisplayCategory;

  @JsonProperty("Classes")
  private final String buqOrderableClass;

  @JsonProperty("Unit of measure")
  private final long unitOfMeasure;

  @JsonProperty("Adjusted Consumption (in Packs)")
  private final int annualAdjustedConsumption;

  @JsonProperty("Verified annual adjusted consumption")
  private final int verifiedAnnualAdjustedConsumption;

  @JsonProperty("Forecasted demand")
  private final int forecastedDemand;

  @JsonProperty("MSD Sales Price")
  @JsonSerialize(using = MoneySerializer.class)
  @JsonDeserialize(using = MoneyDeserializer.class)
  private final Money msdSalesPrice;

  @JsonProperty("Total Cost")
  @JsonSerialize(using = MoneySerializer.class)
  @JsonDeserialize(using = MoneyDeserializer.class)
  private final Money totalCost;

  @JsonProperty("Remarks")
  private final String remark;

}