/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.collect.Lists;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.openlmis.common.domain.ApplicationInterface;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public final class ApplicationInterfaceDto extends BaseDto
    implements ApplicationInterface.Importer,
    ApplicationInterface.Exporter {

  @Getter
  @Setter
  @JsonFormat(shape = STRING)
  private ZonedDateTime createdDate;

  @Getter
  @Setter
  @JsonFormat(shape = STRING)
  private ZonedDateTime modifiedDate;

  @Getter
  @Setter
  private String name;

  @Getter
  @Setter
  private Boolean active;

  @Setter
  private List<InterfaceDataSetDto> dataSets;

  /**
   * Creates new instance based on domain object.
   */
  public static ApplicationInterfaceDto
      newInstance(ApplicationInterface applicationInterface) {
    ApplicationInterfaceDto dto = new ApplicationInterfaceDto();
    applicationInterface.export(dto);

    return dto;
  }

  public List<InterfaceDataSetDto> getDataSets() {
    return Lists.newArrayList(Optional.ofNullable(dataSets)
        .orElse(Collections.emptyList()));
  }

}
