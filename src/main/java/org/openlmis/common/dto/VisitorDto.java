/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.common.dto.referencedata.FacilityDto;
import org.openlmis.common.dto.referencedata.GeographicZoneDto;
import org.openlmis.common.dto.referencedata.UserDto;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class VisitorDto extends BaseDto {
  private UserDto userDto;

  private FacilityDto facilityDto;

  private UserContactDetailsDto userContactDetailsDto;
  private String pageView;

  public GeographicZoneDto getDistrict() {
    return getFacilityDto().getGeographicZone();
  }

  public GeographicZoneDto getRegion() {
    return getDistrict().getParent();
  }

  public GeographicZoneDto getZone() {
    return getRegion().getParent();
  }
}
