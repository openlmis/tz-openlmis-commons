/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "integration_logs")
@NoArgsConstructor
@AllArgsConstructor
public class IntegrationLog extends BaseEntity {

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION, unique = true, nullable = false)
  @Getter
  @Setter
  private String sourceTransactionId;

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION, unique = true, nullable = false)
  @Getter
  @Setter
  private UUID destinationId;

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION)
  @Getter
  @Setter
  private String integrationName;

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION)
  @Getter
  @Setter
  private String errorMessage;

  @Column(columnDefinition = "boolean DEFAULT true")
  @Getter
  @Setter
  private Boolean resolved = true;

  @Column
  @Getter
  @Setter
  private Integer errorCode;

  /**
   * Creates a new IntegrationLog with given sourceOrderId.
   *
   * @param sourceOrderId the source order id
   */
  public IntegrationLog(String sourceOrderId) {

    this.sourceTransactionId = sourceOrderId.toUpperCase();

  }

  /**
   * Creates a new IntegrationLog with given id.
   *
   * @param id the log id
   */
  public IntegrationLog(UUID id) {
    setId(id);
  }

  /**
   * Creates a constructor IntegrationLog with given parameters.
   *
   * @param sourceTransactionId the source order id
   * @param destinationId       the destination id
   * @param integrationName     the integration name
   * @param errorCode           the integration name
   * @param errorMessage        the error message
   */
  public IntegrationLog(String sourceTransactionId, UUID destinationId, String integrationName,
                        String errorMessage, Integer errorCode, Boolean resolved) {
    this.sourceTransactionId = sourceTransactionId;
    this.destinationId = destinationId;
    this.integrationName = integrationName;
    this.errorMessage = errorMessage;
    this.errorCode = errorCode;
    this.resolved = resolved;
  }

  /**
   * Creates new Integration log object based on data from {@link Importer}.
   *
   * @param importer instance of {@link Importer}
   *
   * @return new instance of integration log.
   */
  public static IntegrationLog newIntegrationLog(Importer importer) {

    IntegrationLog log = new IntegrationLog();
    log.setSourceTransactionId(importer.getSourceTransactionId());
    log.setIntegrationName(importer.getIntegrationName());
    log.setDestinationId(importer.getDestinationId());
    log.setId(importer.getId());
    log.setErrorCode(importer.getErrorCode());
    log.setErrorMessage(importer.getErrorMessage());
    log.setResolved(importer.getResolved());
    return log;

  }

  public interface Importer extends BaseImporter {

    String getSourceTransactionId();

    UUID getDestinationId();

    String getIntegrationName();

    String getErrorMessage();

    Integer getErrorCode();

    Boolean getResolved();

  }

  /**
   * Exports current state of Integration object.
   *
   * @param exporter instance of {@link Exporter}
   */
  public void export(Exporter exporter) {
    exporter.setDestinationId(getDestinationId());
    exporter.setIntegrationName(integrationName);
    exporter.setErrorCode(errorCode);
    exporter.setErrorMessage(errorMessage);
    exporter.setSourceTransactionId(sourceTransactionId);
    exporter.setResolved(resolved);
    exporter.setId(getId());
  }


  public interface Exporter extends BaseExporter {

    void setSourceTransactionId(String sourceTransactionId);

    void setDestinationId(UUID destinationId);

    void setIntegrationName(String integrationName);

    void setErrorMessage(String errorMessage);

    void setErrorCode(Integer errorCode);

    void setResolved(Boolean resolved);

  }

}
