/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.javers.core.metamodel.annotation.TypeName;
import org.openlmis.common.util.HasId;

@Entity
@Table(name = "buq_orderables")
@NoArgsConstructor
@AllArgsConstructor
@TypeName("BuqOrderable")
public class BuqOrderable implements HasId {

  @Id
  @Getter
  @Setter
  @Type(type = BaseEntity.UUID_TYPE)
  private UUID orderableId;

  @Getter
  @Setter
  @Type(type = BaseEntity.UUID_TYPE)
  @ManyToOne
  @JoinColumn(name = "categoryid", nullable = false)
  private BuqOrderableCategory category;

  @NotBlank
  @Column(nullable = false, columnDefinition = BaseEntity.TEXT_COLUMN_DEFINITION)
  private String buqClass;

  public static BuqOrderable newBuqOrderable(
      UUID orderableId, BuqOrderableCategory category, String buqClass) {
    return new BuqOrderable(orderableId, category, buqClass);
  }

  @Override
  public UUID getId() {
    return orderableId;
  }
}
