/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "msd_orderables", schema = "common")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Getter
@Setter
public class MsdOrderable extends BaseEntity {

  @Column(name = "itemCode")
  private String itemCode;

  @Column(name = "code")
  private String code;

  @Column(name = "primaryName")
  private String primaryName;

  @Column(name = "revision")
  private String revision;

  @Column(name = "uom")
  private String uom;

  @Column(name = "uomDescription")
  private String uomDescription;

  @Column(name = "productCategory")
  private String productCategory;

  @Column(name = "form")
  private String form;

  @Column(name = "packSize")
  private Integer packSize;

  @Column(name = "priceCode")
  private String priceCode;

  @Column(name = "price")
  private Double price;

  @Column(name = "processed",
      columnDefinition = "boolean default false")
  private Boolean processed;

}
