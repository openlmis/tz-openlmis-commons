/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.javers.core.metamodel.annotation.TypeName;
import org.openlmis.common.dto.ProofOfDeliveryDto;
import org.openlmis.common.util.JsonArrayToListMapConverter;

@Entity
@Table(name = "msd_proof_of_deliveries")
@TypeName("ProofOfDelivery")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"customerId", "hfrCode", "orderNumber"}, callSuper = true)
public class ProofOfDelivery
    extends BaseTimestampedEntity {

  private String comment;
  private String customerId;
  private String deliveredBy;
  private String deliveredStatus;
  private LocalDate invoiceDate;
  private String invoiceNumber;
  private String msdOrderNumber;
  private int numberOfItemsReceived;
  private LocalDate orderDate;
  private String orderNumber;
  private String receivedBy;
  private String receivedDate;

  @Convert(converter = JsonArrayToListMapConverter.class)
  @Column(columnDefinition = "TEXT")
  private List<Map<String, Object>> podLineItems;

  /**
   * Maps a ProofOfDelivery DTO to an entity.
   *
   * @param dto the ProofOfDeliveryDto to map
   * @return a ProofOfDelivery entity populated with the DTO data
   */
  public static ProofOfDelivery fromDto(ProofOfDeliveryDto dto) {
    ProofOfDelivery entity = new ProofOfDelivery();
    entity.setComment(dto.getComment());
    entity.setCustomerId(dto.getCustomerId());
    entity.setDeliveredBy(dto.getDeliveredBy());
    entity.setDeliveredStatus(dto.getDeliveredStatus());
    entity.setInvoiceDate(dto.getInvoiceDate());
    entity.setInvoiceNumber(dto.getInvoiceNumber());
    entity.setMsdOrderNumber(dto.getMsdOrderNumber());
    entity.setNumberOfItemsReceived(dto.getNumberOfItemsReceived());
    entity.setOrderDate(dto.getOrderDate());
    entity.setOrderNumber(dto.getOrderNumber());
    entity.setReceivedBy(dto.getReceivedBy());
    entity.setReceivedDate(dto.getReceivedDate());
    entity.setPodLineItems(dto.getPodLineItems());
    return entity;
  }

}
