/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */


package org.openlmis.common.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.javers.core.metamodel.annotation.TypeName;

@Entity
@Table(name = "buq_orderable_categories")
@TypeName("BuqOrderableCategory")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"name"}, callSuper = true)
public class BuqOrderableCategory extends BaseEntity {

  @NotBlank
  @Column(nullable = false, unique = true)
  private String code;

  @NotBlank
  @Column(unique = true)
  private String name;

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION)
  private String description;

  /**
   * Create new buq product category from the provided buq product category details.
   *
   * @param importer provided buq product category details
   * @return new instance of buq product category.
   */
  public static BuqOrderableCategory newInstance(Importer importer) {
    BuqOrderableCategory buqOrderableCategory = new BuqOrderableCategory();
    buqOrderableCategory.setId(importer.getId());
    buqOrderableCategory.setCode(importer.getCode());
    buqOrderableCategory.setName(importer.getName());
    buqOrderableCategory.setDescription(importer.getDescription());

    return buqOrderableCategory;
  }

  /**
   * Export the content of the buq product category entity.
   *
   * @param exporter destination of the exported buq product category data.
   */
  public void export(BuqOrderableCategory.Exporter exporter) {
    exporter.setId(this.id);
    exporter.setCode(this.code);
    exporter.setName(this.name);
    exporter.setDescription(this.description);
  }

  public interface Importer extends BaseImporter {

    @NotBlank
    String getCode();

    @NotBlank
    String getName();

    String getDescription();
  }

  public interface Exporter extends BaseExporter {
    void setCode(String code);

    void setName(String name);

    void setDescription(String description);
  }
}
