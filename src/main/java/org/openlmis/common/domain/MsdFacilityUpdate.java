/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.javers.core.metamodel.annotation.TypeName;

@Entity
@Table(name = "msd_facility_updates")
@TypeName("MsdFacilityUpdate")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"facilityNumber", "facilityCode"}, callSuper = true)
public class MsdFacilityUpdate extends BaseTimestampedEntity {

  @NotBlank
  private String facilityNumber;

  @NotBlank
  private String facilityCode;

  @NotBlank
  private String facilityName;

  @NotBlank
  private String facilityType;

  @NotBlank
  private String region;

  @NotBlank
  private String council;

  private String hfrCode;

  private Boolean status;

  private boolean applied;

  /**
   * Create new update from the provided MsdFacilityUpdate details.
   *
   * @param importer provided update details
   * @return new instance of MsdFacilityUpdate.
   */
  public static MsdFacilityUpdate newInstance(MsdFacilityUpdate.Importer importer) {
    MsdFacilityUpdate update = new MsdFacilityUpdate();

    update.setId(importer.getId());
    update.setFacilityNumber(importer.getFacilityNumber());
    update.setFacilityCode(importer.getFacilityCode());
    update.setFacilityName(importer.getFacilityName());
    update.setFacilityType(importer.getFacilityType());
    update.setRegion(importer.getRegion());
    update.setCouncil(importer.getCouncil());
    update.setHfrCode(importer.getHfrCode());
    update.setStatus(importer.getStatus());

    return update;
  }

  /**
   * Export the content of the MsdFacilityUpdate entity.
   *
   * @param exporter destination of the exported update data.
   */
  public void export(MsdFacilityUpdate.Exporter exporter) {
    exporter.setId(this.id);
    exporter.setFacilityNumber(this.facilityNumber);
    exporter.setFacilityCode(this.facilityCode);
    exporter.setFacilityName(this.facilityName);
    exporter.setFacilityType(this.facilityType);
    exporter.setRegion(this.region);
    exporter.setCouncil(this.council);
    exporter.setHfrCode(this.hfrCode);
    exporter.setStatus(this.status);
  }

  public interface Importer extends BaseEntity.BaseImporter {

    String getFacilityNumber();

    String getFacilityCode();

    String getFacilityName();

    String getFacilityType();

    String getRegion();

    String getCouncil();

    String getHfrCode();

    Boolean getStatus();

  }

  public interface Exporter extends BaseEntity.BaseExporter {

    void setFacilityNumber(String facilityNumber);

    void setFacilityCode(String facilityCode);

    void setFacilityName(String facilityName);

    void setFacilityType(String facilityType);

    void setRegion(String region);

    void setCouncil(String council);

    void setHfrCode(String hfrCode);

    void setStatus(Boolean status);
  }
}
