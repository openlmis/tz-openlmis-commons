/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.javers.core.metamodel.annotation.TypeName;
import org.openlmis.common.util.JsonArrayToListMapConverter;

@Entity
@Table(name = "msd_stock_out_notifications")
@TypeName("StockOutNotification")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"customerId", "hfrCode", "elmisOrderNumber"}, callSuper = true)
public class StockOutNotification extends BaseTimestampedEntity {

  private String quoteNumber;

  @NotBlank
  private String customerId;

  @NotBlank
  private String customerName;

  @NotBlank
  private String hfrCode;

  @JsonProperty("facilityOrderNumber")
  @NotBlank
  private String elmisOrderNumber;

  @NotNull
  private LocalDate notificationDate;

  private LocalDate processingDate;

  @NotBlank
  private String zone;

  private String comment;

  @Convert(converter = JsonArrayToListMapConverter.class)
  public List<Map<String, Object>> rationingItems;

  @Convert(converter = JsonArrayToListMapConverter.class)
  public List<Map<String, Object>> stockOutItems;

  @Convert(converter = JsonArrayToListMapConverter.class)
  public List<Map<String, Object>> inSufficientFundingItems;

  @Convert(converter = JsonArrayToListMapConverter.class)
  public List<Map<String, Object>> fullFilledItems;

  @Convert(converter = JsonArrayToListMapConverter.class)
  public List<Map<String, Object>> closeToExpireItems;

  @Convert(converter = JsonArrayToListMapConverter.class)
  public List<Map<String, Object>> phasedOutItems;

  @NotNull
  @Type(type = UUID_TYPE)
  public UUID facilityId;

  @NotNull
  @Type(type = UUID_TYPE)
  public UUID programId;

  @NotNull
  @Type(type = UUID_TYPE)
  public UUID processingPeriodId;

}
