/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.javers.core.metamodel.annotation.TypeName;

@Entity
@Table(name = "msd_daily_stock_status_value")
@TypeName("MSDDailyStockStatusValue")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class MsdDailyStockStatusValue extends BaseEntity {

  @NotBlank
  public String plant;

  @NotBlank
  public String partNumber;

  @NotBlank
  public String unitOfMeasure;

  @NotBlank
  public String partDescription;

  @NotBlank
  public String date;

  @NotNull
  public Integer monthOfStock;

  @NotBlank
  public String onHandQuantity;


  /**
   * Create new stock status value from the provided value details.
   *
   * @param importer provided stock status value details
   *
   * @return new instance of stock status value.
   */
  public static MsdDailyStockStatusValue newInstance(Importer importer) {
    MsdDailyStockStatusValue value = new MsdDailyStockStatusValue();

    value.setDate(importer.getDate());
    value.setMonthOfStock(importer.getMonthOfStock());
    value.setPartDescription(importer.getPartDescription());
    value.setPlant(importer.getPlant());
    value.setPartNumber(importer.getPartNumber());
    value.setOnHandQuantity(importer.getOnHandQuantity());
    value.setUnitOfMeasure(importer.getUnitOfMeasure());
    value.setId(importer.getId());

    return value;
  }

  /**
   * Export the content of the stock status value entity.
   *
   * @param exporter destination of the exported stock status value data.
   */
  public void export(MsdDailyStockStatusValue.Exporter exporter) {
    exporter.setId(this.id);
    exporter.setPlant(this.plant);
    exporter.setPartNumber(this.partNumber);
    exporter.setUnitOfMeasure(this.unitOfMeasure);
    exporter.setPartDescription(this.partDescription);
    exporter.setDate(this.date);
    exporter.setMonthOfStock(this.monthOfStock);
    exporter.setOnHandQuantity(this.onHandQuantity);
  }

  public interface Importer extends BaseImporter {

    public String getPlant();

    public String getPartNumber();

    public String getUnitOfMeasure();

    public String getPartDescription();

    public String getDate();

    public Integer getMonthOfStock();

    public String getOnHandQuantity();
  }

  public interface Exporter extends BaseExporter {

    void setPlant(String plant);

    void setPartNumber(String partNumber);

    void setUnitOfMeasure(String unitOfMeasure);

    void setPartDescription(String partDescription);

    void setDate(String date);

    void setMonthOfStock(Integer monthOfStock);

    void setOnHandQuantity(String onHandQuantity);

  }
}
