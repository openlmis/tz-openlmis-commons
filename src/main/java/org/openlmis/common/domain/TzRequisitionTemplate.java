/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * A table created to mimic an extension of RequisitionTemplate from the core.
 * It is supposed to have an additional field called "patientsTabEnabled"
 * and be connected with the RequisitionTemplate as a foreign key
 */
@Entity
@Table(name = "tz_requisition_templates")
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
public class TzRequisitionTemplate extends BaseEntity {

  @Column(unique = true)
  @NotNull
  private UUID requisitionTemplateId;

  @Column(columnDefinition = "boolean DEFAULT false")
  private boolean patientsTabEnabled = false;
}
