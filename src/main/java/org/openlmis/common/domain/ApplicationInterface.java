/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import java.time.ZonedDateTime;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.javers.core.metamodel.annotation.TypeName;
import org.openlmis.common.dto.InterfaceDataSetDto;

@Entity
@TypeName("ApplicationInterface")
@Table(name = "application_interfaces", schema = "common")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ApplicationInterface extends BaseTimestampedEntity {

  @NotNull
  @Getter
  @Setter
  private String name;

  @Getter
  @Setter
  private Boolean active;

  @OneToMany(
      mappedBy = "applicationInterface",
      cascade = CascadeType.ALL,
      orphanRemoval = true)
  @Getter
  @Setter
  private List<InterfaceDataSet> dataSets;

  /**
   * Constructor.
   *
   * @param name   name of the interface app
   * @param active active of the interface app
   */
  public ApplicationInterface(String name, Boolean active) {
    this.name = name;
    this.active = active;
  }

  /**
   * Creates new instance based on data from the importer.
   */
  public static ApplicationInterface newInstance(Importer importer) {

    ApplicationInterface applicationInterface = new ApplicationInterface();

    applicationInterface.setCreatedDate(importer.getCreatedDate());
    applicationInterface.setModifiedDate(importer.getModifiedDate());
    applicationInterface.setName(importer.getName());
    applicationInterface.setActive(importer.getActive());

    return applicationInterface;
  }

  /**
   * Copy values of attributes into new or updated ApplicationInterface.
   *
   * @param dataSet list of interface data sets.
   */
  public void updateFrom(List<InterfaceDataSet> dataSet) {

    if (dataSet != null) {
      dataSets.clear();
      dataSets.addAll(dataSet);
      setModifiedDate(ZonedDateTime.now());
    }

  }

  /**
   * Export this object to the specified exporter (DTO).
   *
   * @param exporter exporter to export to
   */
  public void export(Exporter exporter) {

    exporter.setId(getId());
    exporter.setCreatedDate(getCreatedDate());
    exporter.setModifiedDate(getModifiedDate());
    exporter.setName(name);
    exporter.setActive(active);

  }

  public interface Exporter extends BaseTimestampedExporter {

    void setName(String name);

    void setActive(Boolean active);

  }

  public interface Importer extends BaseTimestampedImporter {

    String getName();

    Boolean getActive();

    List<InterfaceDataSetDto> getDataSets();

  }
}
