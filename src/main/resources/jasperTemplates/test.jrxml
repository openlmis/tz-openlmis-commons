<?xml version="1.0" encoding="UTF-8"?>
<!-- Created with Jaspersoft Studio version 6.20.0.final using JasperReports Library version 6.20.0-2bc7ab61c56f459e8176eb05c7705e145cd400ad  -->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="notification" pageWidth="800" pageHeight="842" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="760" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" whenResourceMissingType="Error" uuid="6e843fe2-ce72-4257-a4c3-1df03728dc95">
    <property name="com.jaspersoft.studio.data.defaultdataadapter" value="One Empty Record"/>
    <property name="reportType" value="Timeliness Report"/>
    <import value="org.openlmis.fulfillment.web.util.*"/>
    <import value="java.time.ZonedDateTime"/>
    <import value="java.time.format.DateTimeFormatter"/>
    <import value="java.text.DecimalFormat"/>
    <style name="Row" mode="Transparent">
        <conditionalStyle>
            <conditionExpression><![CDATA[$V{REPORT_COUNT} % 2 == 0]]></conditionExpression>
            <style backcolor="#F3F3F3"/>
        </conditionalStyle>
    </style>
    <parameter name="order" class="org.openlmis.common.dto.fulfillment.OrderDto" isForPrompting="false">
        <property name="displayName" value="Order"/>
    </parameter>
    <parameter name="loggedInUser" class="java.lang.String">
        <property name="displayName" value="LoggedInUser"/>
    </parameter>
    <parameter name="decimalFormat" class="java.text.DecimalFormat" isForPrompting="false">
        <property name="displayName" value="DecimalFormat"/>
    </parameter>
    <parameter name="dateTimeFormat" class="java.lang.String" isForPrompting="false">
        <property name="displayName" value="DateTimeFormat"/>
    </parameter>
    <parameter name="stockNotification" class="org.openlmis.common.dto.StockNotificationDto" isForPrompting="false"/>
    <parameter name="periodValidity" class="java.lang.String"/>
    <parameter name="msdLogo" class="java.lang.String"/>
    <parameter name="signature" class="java.lang.String"/>
    <queryString>
        <![CDATA[]]>
    </queryString>
    <field name="itemCode" class="java.lang.String"/>
    <field name="itemDescription" class="java.lang.String"/>
    <field name="uom" class="java.lang.String"/>
    <field name="quantityOrdered" class="java.lang.Integer"/>
    <field name="missingItemStatus" class="java.lang.String"/>
    <field name="quantity" class="java.lang.Integer"/>
    <background>
        <band splitType="Stretch"/>
    </background>
    <title>
        <band height="110" splitType="Stretch">
            <staticText>
                <reportElement x="250" y="80" width="280" height="24" forecolor="#3D9297" uuid="9425a6c9-4546-4c90-9119-7a7f300b6fb5"/>
                <textElement textAlignment="Center">
                    <font size="16" isBold="false"/>
                    <paragraph lineSpacing="Double" lineSpacingSize="2.0"/>
                </textElement>
                <text><![CDATA[Stock-out notification report]]></text>
            </staticText>
            <rectangle>
                <reportElement x="551" y="0" width="210" height="110" uuid="e8f7672a-fe8f-432b-be0a-a431d13e6eeb"/>
            </rectangle>
            <staticText>
                <reportElement x="575" y="77" width="181" height="12" uuid="f1d6e33c-0e81-4655-89b0-db02819337e8">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement textAlignment="Right">
                    <font size="8" isBold="true"/>
                </textElement>
                <text><![CDATA[ISO 9001:2015 CERTIFIED]]></text>
            </staticText>
            <textField>
                <reportElement x="575" y="90" width="181" height="12" uuid="2a011700-672b-4634-b73c-fc2fa8b6bb58">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement textAlignment="Right">
                    <font size="8" isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA["Zone: "+$P{stockNotification}.getZone()]]></textFieldExpression>
            </textField>
            <staticText>
                <reportElement x="575" y="65" width="181" height="12" uuid="d4a8463d-437c-4ae5-bb01-e9aaa2ecdf9a">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement textAlignment="Right">
                    <font size="8" isBold="true" isItalic="true"/>
                </textElement>
                <text><![CDATA[Website : www.msd.go.tz]]></text>
            </staticText>
            <staticText>
                <reportElement x="575" y="53" width="181" height="12" uuid="81a6b9d6-4324-4c5c-a96a-323467f1b230">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement textAlignment="Right">
                    <font size="8" isBold="true" isItalic="true"/>
                </textElement>
                <text><![CDATA[E-mail : info@msd.go.tz]]></text>
            </staticText>
            <staticText>
                <reportElement x="575" y="41" width="181" height="12" uuid="4239f799-efbf-428c-93c3-afb0ac2bfc93">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement textAlignment="Right">
                    <font size="8" isBold="true" isItalic="true"/>
                </textElement>
                <text><![CDATA[Fax:(255 22) 2865814  2865819]]></text>
            </staticText>
            <staticText>
                <reportElement x="575" y="29" width="181" height="12" uuid="048591ab-dbc1-4357-84b0-48f0b4882d85">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement textAlignment="Right">
                    <font size="8" isBold="true" isItalic="true"/>
                </textElement>
                <text><![CDATA[Telephone:(255 22) 2865814  2865819 - 7]]></text>
            </staticText>
            <staticText>
                <reportElement x="575" y="17" width="181" height="12" uuid="2d725ed6-0019-41a6-a4f4-d13f56e9e194">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement textAlignment="Right">
                    <font size="8" isBold="true" isItalic="true"/>
                </textElement>
                <text><![CDATA[P.O Box 9081,Dar Es Salaam, Tanzania]]></text>
            </staticText>
            <staticText>
                <reportElement x="575" y="5" width="181" height="12" uuid="ee1fc301-c854-4ff7-a329-c99c78785d0f">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement textAlignment="Right">
                    <font size="8" isBold="true" isItalic="true"/>
                </textElement>
                <text><![CDATA[Off Nyerere Road, Keko Mwanga]]></text>
            </staticText>
            <staticText>
                <reportElement x="1" y="87" width="151" height="11" uuid="b5c62250-6227-4e07-b22d-857b36987b8b"/>
                <textElement>
                    <font size="8" isBold="true"/>
                </textElement>
                <text><![CDATA[TIN: 101-060-195 ]]></text>
            </staticText>
            <image>
                <reportElement x="2" y="0" width="125" height="77" uuid="07d5cdfe-ab1b-4d81-adf4-8700e009e6cc"/>
                <imageExpression><![CDATA[$P{msdLogo}]]></imageExpression>
            </image>
        </band>
    </title>
    <pageHeader>
        <band height="173">
            <property name="com.jaspersoft.studio.unit.height" value="pixel"/>
            <rectangle>
                <reportElement x="-1" y="13" width="277" height="105" uuid="90ebc73d-7896-4589-bb15-c099e292eaa5"/>
            </rectangle>
            <textField>
                <reportElement x="5" y="97" width="200" height="12" uuid="68ecc6d2-b47b-4dc2-9cd9-66fbcfa7dfc5">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement verticalAlignment="Middle">
                    <font size="8" isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA["Tanzania"]]></textFieldExpression>
            </textField>
            <textField>
                <reportElement x="4" y="79" width="261" height="10" uuid="d9fcf979-ab13-4f2b-98ee-4605762e2c91">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement verticalAlignment="Middle">
                    <font size="8" isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA["Region: "+ $P{order}.getRequestingFacility().getGeographicZone().getParent().getName()]]></textFieldExpression>
            </textField>
            <textField>
                <reportElement x="6" y="64" width="261" height="10" uuid="1da26d9c-cdb0-41c4-bba1-d369a0535def">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement verticalAlignment="Middle">
                    <font size="8" isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA["P.O.Box:   "+ $P{order}.getRequestingFacility().getGeographicZone().getName()]]></textFieldExpression>
            </textField>
            <textField>
                <reportElement x="5" y="48" width="260" height="12" uuid="e0feef33-d2da-4fe9-a3ae-a3b41f9e0d1e">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement verticalAlignment="Middle">
                    <font size="8" isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA["Hfr Code: "+ $P{stockNotification}.getHfrCode()]]></textFieldExpression>
            </textField>
            <textField>
                <reportElement x="4" y="33" width="261" height="12" uuid="37b39ef5-1859-46b4-a365-e52bcd99669a">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement verticalAlignment="Middle">
                    <font size="8" isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA["Customer ID: "+$P{stockNotification}.getCustomerId()]]></textFieldExpression>
            </textField>
            <textField>
                <reportElement x="5" y="17" width="260" height="12" uuid="70dd8d82-e01e-40fb-933e-a559e3563db6">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement verticalAlignment="Middle">
                    <font size="8" isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA["Customer Name: "+$P{stockNotification}.getCustomerName()]]></textFieldExpression>
            </textField>
            <textField>
                <reportElement x="0" y="148" width="761" height="23" uuid="4eaf8ece-f225-497f-b772-435ab290429d"/>
                <textElement textAlignment="Left" verticalAlignment="Middle">
                    <font size="8" isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA["Comment : "+$P{stockNotification}.getComment()]]></textFieldExpression>
            </textField>
            <rectangle>
                <reportElement x="551" y="12" width="208" height="105" uuid="07e9e797-190d-4710-81e8-027e6c8c515d"/>
            </rectangle>
            <textField>
                <reportElement x="556" y="13" width="203" height="13" uuid="de406c27-5373-438e-809d-4be9b4d97a00">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement verticalAlignment="Middle">
                    <font size="8" isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA["Notification Date: "+$P{stockNotification}.getNotificationDate()]]></textFieldExpression>
            </textField>
            <textField>
                <reportElement x="557" y="34" width="202" height="12" uuid="181f9ee1-af98-4a80-9161-f7cd71bd8a44">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement verticalAlignment="Middle">
                    <font size="8" isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA["Notification is valid up to: "+$P{periodValidity}]]></textFieldExpression>
            </textField>
            <textField>
                <reportElement x="558" y="54" width="201" height="12" uuid="52b1b25b-871f-4b34-b095-cd8f632ee9b9">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement verticalAlignment="Middle">
                    <font size="8" isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA["eLMIS Order Number: "+ $P{stockNotification}.getElmisOrderNumber()]]></textFieldExpression>
            </textField>
            <textField>
                <reportElement x="557" y="81" width="202" height="12" uuid="2a4c2e57-f46c-4a03-9e32-abc2e1f65db3">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement verticalAlignment="Middle">
                    <font size="8" isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA["Program: " +$P{order}.getProgram().getName()]]></textFieldExpression>
            </textField>
        </band>
    </pageHeader>
    <columnHeader>
        <band height="31">
            <staticText>
                <reportElement mode="Opaque" x="0" y="0" width="50" height="30" backcolor="#3D9297" uuid="7972837a-4e14-4bc0-94ef-c9f465914ad5"/>
                <textElement textAlignment="Left" verticalAlignment="Middle">
                    <font size="11"/>
                </textElement>
                <text><![CDATA[Line #]]></text>
            </staticText>
            <staticText>
                <reportElement mode="Opaque" x="50" y="0" width="90" height="30" backcolor="#3D9297" uuid="f1296dcd-3a59-4110-99af-f111c9e49196"/>
                <textElement textAlignment="Left" verticalAlignment="Middle">
                    <font size="11"/>
                </textElement>
                <text><![CDATA[Item Code]]></text>
            </staticText>
            <staticText>
                <reportElement mode="Opaque" x="140" y="0" width="260" height="30" backcolor="#3D9297" uuid="fcca10b0-2b26-46d1-b532-5386fbf3bbc4"/>
                <textElement textAlignment="Left" verticalAlignment="Middle">
                    <font size="11"/>
                </textElement>
                <text><![CDATA[Item Description]]></text>
            </staticText>
            <staticText>
                <reportElement mode="Opaque" x="400" y="0" width="75" height="30" backcolor="#3D9297" uuid="8e7db4f0-de17-4a0a-b56c-452be828d4db"/>
                <textElement textAlignment="Left" verticalAlignment="Middle">
                    <font size="10"/>
                </textElement>
                <text><![CDATA[uom]]></text>
            </staticText>
            <staticText>
                <reportElement mode="Opaque" x="626" y="0" width="135" height="30" backcolor="#3D9297" uuid="646a24fe-b770-4291-8a0d-13db58237726"/>
                <textElement textAlignment="Left" verticalAlignment="Middle">
                    <font size="10"/>
                </textElement>
                <text><![CDATA[Missing Status]]></text>
            </staticText>
            <staticText>
                <reportElement mode="Opaque" x="476" y="0" width="75" height="30" backcolor="#3D9297" uuid="7fe29aea-f7dd-402e-8b69-aa9852967229"/>
                <textElement textAlignment="Left" verticalAlignment="Middle">
                    <font size="10"/>
                </textElement>
                <text><![CDATA[Ordered Quantity]]></text>
            </staticText>
            <staticText>
                <reportElement mode="Opaque" x="551" y="0" width="75" height="30" backcolor="#3D9297" uuid="df6b1a02-dcf2-4c0c-9ba5-11cca8b0c772"/>
                <textElement textAlignment="Left" verticalAlignment="Middle">
                    <font size="10"/>
                </textElement>
                <text><![CDATA[Missed Quantity]]></text>
            </staticText>
        </band>
    </columnHeader>
    <detail>
        <band height="26" splitType="Immediate">
            <property name="com.jaspersoft.studio.unit.height" value="pixel"/>
            <textField textAdjust="StretchHeight">
                <reportElement style="Row" stretchType="RelativeToTallestObject" mode="Opaque" x="0" y="0" width="50" height="25" uuid="28c1bf9c-6c13-4551-abf3-82481d555ab9"/>
                <box padding="3">
                    <pen lineColor="#3D9297"/>
                    <topPen lineWidth="1.0" lineStyle="Solid" lineColor="#3D9297"/>
                    <leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
                    <bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#3D9297"/>
                    <rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
                </box>
                <textElement textAlignment="Left" verticalAlignment="Middle">
                    <font size="11"/>
                </textElement>
                <textFieldExpression><![CDATA[$V{REPORT_COUNT}]]></textFieldExpression>
            </textField>
            <textField textAdjust="StretchHeight">
                <reportElement style="Row" stretchType="RelativeToTallestObject" mode="Opaque" x="51" y="0" width="90" height="25" uuid="e0a80ca5-07b6-4042-b3cc-cc7fc7bbd965">
                    <property name="com.jaspersoft.studio.unit.y" value="px"/>
                    <property name="com.jaspersoft.studio.unit.width" value="px"/>
                </reportElement>
                <box padding="3">
                    <pen lineColor="#3D9297"/>
                    <topPen lineWidth="1.0" lineStyle="Solid" lineColor="#3D9297"/>
                    <leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
                    <bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#3D9297"/>
                    <rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
                </box>
                <textElement textAlignment="Left" verticalAlignment="Middle">
                    <font size="11"/>
                </textElement>
                <textFieldExpression><![CDATA[$F{itemCode}]]></textFieldExpression>
            </textField>
            <textField textAdjust="StretchHeight">
                <reportElement style="Row" stretchType="RelativeToTallestObject" mode="Opaque" x="140" y="0" width="260" height="25" uuid="adf678b0-123d-4531-b117-0e7e6ec56383">
                    <property name="com.jaspersoft.studio.unit.y" value="px"/>
                    <property name="com.jaspersoft.studio.unit.width" value="px"/>
                    <property name="com.jaspersoft.studio.unit.x" value="px"/>
                </reportElement>
                <box padding="3">
                    <pen lineColor="#3D9297"/>
                    <topPen lineWidth="1.0" lineStyle="Solid" lineColor="#3D9297"/>
                    <leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
                    <bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#3D9297"/>
                    <rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
                </box>
                <textElement textAlignment="Left" verticalAlignment="Middle">
                    <font size="11"/>
                </textElement>
                <textFieldExpression><![CDATA[$F{itemDescription}]]></textFieldExpression>
            </textField>
            <textField textAdjust="StretchHeight">
                <reportElement style="Row" stretchType="RelativeToTallestObject" mode="Opaque" x="400" y="0" width="75" height="25" uuid="91394459-2712-4132-b5d6-291d1b329111">
                    <property name="com.jaspersoft.studio.unit.y" value="px"/>
                    <property name="com.jaspersoft.studio.unit.width" value="px"/>
                </reportElement>
                <box padding="3">
                    <pen lineColor="#3D9297"/>
                    <topPen lineWidth="1.0" lineStyle="Solid" lineColor="#3D9297"/>
                    <leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
                    <bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#3D9297"/>
                    <rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
                </box>
                <textElement textAlignment="Left" verticalAlignment="Middle">
                    <font size="11"/>
                </textElement>
                <textFieldExpression><![CDATA[$F{uom}]]></textFieldExpression>
            </textField>
            <textField textAdjust="StretchHeight">
                <reportElement style="Row" stretchType="RelativeToTallestObject" mode="Opaque" x="476" y="0" width="75" height="25" uuid="3ae7a7b9-e953-4b14-b8d1-aa8f5b7b5670">
                    <property name="com.jaspersoft.studio.unit.y" value="px"/>
                    <property name="com.jaspersoft.studio.unit.width" value="px"/>
                </reportElement>
                <box padding="3">
                    <pen lineColor="#3D9297"/>
                    <topPen lineWidth="1.0" lineStyle="Solid" lineColor="#3D9297"/>
                    <leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
                    <bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#3D9297"/>
                    <rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
                </box>
                <textElement textAlignment="Right" verticalAlignment="Middle">
                    <font size="11"/>
                </textElement>
                <textFieldExpression><![CDATA[$F{quantityOrdered}]]></textFieldExpression>
            </textField>
            <textField textAdjust="StretchHeight">
                <reportElement style="Row" stretchType="RelativeToTallestObject" mode="Opaque" x="551" y="0" width="75" height="25" uuid="cf163065-e770-4c3d-a502-438dfa8ea734">
                    <property name="com.jaspersoft.studio.unit.y" value="px"/>
                    <property name="com.jaspersoft.studio.unit.width" value="px"/>
                </reportElement>
                <box padding="3">
                    <pen lineColor="#3D9297"/>
                    <topPen lineWidth="1.0" lineStyle="Solid" lineColor="#3D9297"/>
                    <leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
                    <bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#3D9297"/>
                    <rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
                </box>
                <textElement textAlignment="Right" verticalAlignment="Middle">
                    <font size="11"/>
                </textElement>
                <textFieldExpression><![CDATA[$F{quantity}]]></textFieldExpression>
            </textField>
            <textField textAdjust="StretchHeight">
                <reportElement style="Row" stretchType="RelativeToTallestObject" mode="Opaque" x="626" y="0" width="135" height="25" uuid="7361546c-f15c-4761-b090-821e87a40c17">
                    <property name="com.jaspersoft.studio.unit.y" value="px"/>
                    <property name="com.jaspersoft.studio.unit.width" value="px"/>
                </reportElement>
                <box padding="3">
                    <pen lineColor="#3D9297"/>
                    <topPen lineWidth="1.0" lineStyle="Solid" lineColor="#3D9297"/>
                    <leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
                    <bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#3D9297"/>
                    <rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
                </box>
                <textElement textAlignment="Center" verticalAlignment="Middle">
                    <font size="11"/>
                </textElement>
                <textFieldExpression><![CDATA[$F{missingItemStatus}]]></textFieldExpression>
            </textField>
        </band>
    </detail>
    <pageFooter>
        <band height="26">
            <textField pattern="MMMMM dd, yyyy">
                <reportElement x="3" y="0" width="197" height="14" uuid="482ceaf9-f81f-4506-ae9c-ccbe939ebc40">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <textElement>
                    <font size="8" isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA["Generated On : "+new java.util.Date()]]></textFieldExpression>
            </textField>
            <textField>
                <reportElement x="205" y="2" width="161" height="11" uuid="d2216ab7-4c15-45e4-9601-bc21230cce7d"/>
                <textElement textAlignment="Center" verticalAlignment="Middle">
                    <font size="8" isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA["Source : eLMIS"]]></textFieldExpression>
            </textField>
            <textField>
                <reportElement x="675" y="0" width="40" height="14" uuid="ce4a0005-7521-4b93-8aaf-0d82e59e3b79"/>
                <textElement textAlignment="Right"/>
                <textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER}]]></textFieldExpression>
            </textField>
            <textField evaluationTime="Report">
                <reportElement x="717" y="0" width="40" height="14" uuid="b6a8f409-62c4-4304-9df3-c2f0f193fa40"/>
                <textElement textAlignment="Left"/>
                <textFieldExpression><![CDATA[" of " + $V{PAGE_NUMBER}]]></textFieldExpression>
            </textField>
            <image>
                <reportElement x="432" y="0" width="110" height="20" uuid="7d6d4d27-5dbc-4972-bade-6ec8dc26137b">
                    <property name="com.jaspersoft.studio.unit.height" value="pixel"/>
                </reportElement>
                <imageExpression><![CDATA[$P{signature}]]></imageExpression>
            </image>
        </band>
    </pageFooter>
</jasperReport>
