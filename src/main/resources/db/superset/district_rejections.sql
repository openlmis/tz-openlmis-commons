SELECT district,region,

(SELECT z.name FROM public.kafka_geographic_zones z WHERE z.id = ft.regionParentId) as msdZone,
program,period,schedule,(SELECT DATE_PART('year', rdate::DATE)) AS year,

ft.reason,count(*) as total

FROM
(SELECT rr.name as reason,p.name as program,pp.name as period,psch.name as schedule,
r.createddate as rdate,

(SELECT dt.district
FROM (SELECT gz.name as district
FROM public.kafka_geographic_zones gz
WHERE gz.id =f.geographiczoneid) as dt) as district,

(SELECT dt.region
FROM (SELECT 
(SELECT name FROM public.kafka_geographic_zones z WHERE z.id = gz.parentid) as region
FROM public.kafka_geographic_zones gz
WHERE gz.id =f.geographiczoneid) as dt) as region,

(SELECT dt.regionParentId
FROM (SELECT 
(SELECT z.parentid FROM public.kafka_geographic_zones z WHERE z.id = gz.parentid) as regionParentId
FROM public.kafka_geographic_zones gz
WHERE gz.id =f.geographiczoneid) as dt) as regionParentId


FROM public.kafka_rejections r 
INNER JOIN public.kafka_rejection_reasons rr ON r.rejectionreasonid=rr.id 
INNER JOIN public.kafka_status_changes sc ON r.statuschangeid=sc.id::TEXT
INNER JOIN public.kafka_requisitions req ON sc.requisitionid=req.id
INNER JOIN public.kafka_facilities f ON f.id=req.facilityid
INNER JOIN public.kafka_programs p ON req.programid=p.id
INNER JOIN public.kafka_processing_periods pp ON req.processingperiodid=pp.id
INNER JOIN public.kafka_processing_schedules psch ON pp.processingscheduleid=psch.id) as ft
GROUP BY ft.district, ft.region, ft.regionparentid,ft.reason,ft.program,ft.period,ft.schedule,ft.rdate;
