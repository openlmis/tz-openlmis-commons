CREATE TABLE malaria_validations
(
    id               UUID NOT NULL,
    requisitionId    UUID,
    productId        UUID,
    productCode      text,
    fullProductName  text,
    dispensingUnit   text,
    totalPatient     integer,
    totalConsumption integer,
    CONSTRAINT malaria_validations_pkey PRIMARY KEY (id)
);
