--
-- Name: widget; Type: TABLE; Schema: common; Owner: postgres; Tablespace:
--

CREATE TABLE widget (
    id uuid NOT NULL,
    name text NOT NULL
);