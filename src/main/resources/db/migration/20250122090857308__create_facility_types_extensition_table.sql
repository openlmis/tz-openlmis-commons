DROP TABLE IF EXISTS common.facility_type_extensions;

CREATE TABLE common.facility_type_extensions
(
    id UUID PRIMARY KEY,
    facilityTypeId UUID NOT NULL,
    releaseOrderAfterAuthorization BOOLEAN DEFAULT FALSE
);

ALTER TABLE common.msd_orderables
    ADD COLUMN processed BOOLEAN DEFAULT FALSE;