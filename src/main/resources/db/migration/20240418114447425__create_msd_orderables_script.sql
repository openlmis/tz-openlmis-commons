DROP TABLE IF EXISTS common.msd_orderables;
CREATE TABLE common.msd_orderables
(
    id              UUID PRIMARY KEY,
    status          TEXT,
    itemCode        TEXT,
    code            TEXT,
    primaryName     TEXT,
    revision        TEXT,
    uom             TEXT,
    uomDescription  TEXT,
    productCategory TEXT,
    form            TEXT,
    packSize        INTEGER,
    priceCode       TEXT,
    price           DOUBLE PRECISION
);
