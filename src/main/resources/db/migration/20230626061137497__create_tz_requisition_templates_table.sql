CREATE TABLE tz_requisition_templates
(
    id uuid NOT NULL,
    patientsTabEnabled boolean DEFAULT false,
    requisitionTemplateId uuid NOT NULL UNIQUE,
    CONSTRAINT tz_requisition_template_pkey PRIMARY KEY (id)
);
