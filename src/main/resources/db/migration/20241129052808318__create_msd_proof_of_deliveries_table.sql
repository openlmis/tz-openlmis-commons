CREATE TABLE msd_proof_of_deliveries (

    id uuid NOT NULL,
    modifiedDate             timestamp with time zone,
    createdDate              timestamp with time zone,
    comment TEXT,
     customerid TEXT NOT NULL,
     deliveredby TEXT,
     deliveredstatus TEXT,
     invoicedate DATE,
     invoicenumber TEXT,
     msdordernumber TEXT,
     numberofitemsreceived INT,
     orderdate DATE,
     ordernumber TEXT NOT NULL,
     receivedby TEXT,
     receiveddate TEXT,
     podlineitems TEXT
);


