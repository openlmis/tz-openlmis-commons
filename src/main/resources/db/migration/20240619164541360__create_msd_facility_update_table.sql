CREATE TABLE msd_facility_updates
(
    id             uuid NOT NULL,
    modifiedDate   timestamp with time zone,
    createdDate    timestamp with time zone,
    facilityNumber text NOT NULL,
    facilityCode   text NOT NULL,
    facilityName   text NOT NULL,
    facilityType   text NOT NULL,
    region         text NOT NULL,
    council        text NOT NULL,
    hfrCode        text NOT NULL,
    status         boolean DEFAULT FALSE,
    applied        boolean DEFAULT FALSE
);

ALTER TABLE ONLY msd_facility_updates
    ADD CONSTRAINT msd_facility_update_pkey PRIMARY KEY (id);