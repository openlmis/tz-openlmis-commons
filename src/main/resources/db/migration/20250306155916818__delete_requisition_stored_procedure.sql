CREATE OR REPLACE FUNCTION delete_requisition_and_associations(requisitionId UUID) RETURNS VOID AS $$
BEGIN
    DELETE FROM requisition.previous_adjusted_consumptions
    WHERE requisitionlineitemid IN (
        SELECT id FROM requisition.requisition_line_items
        WHERE requisitionid = requisitionId
    );

    DELETE FROM requisition.requisition_line_items WHERE requisitionid = requisitionId;
    DELETE FROM requisition.available_products WHERE requisitionid = requisitionId;
    DELETE FROM requisition.requisitions_previous_requisitions WHERE previousrequisitionid = requisitionId;
    DELETE FROM requisition.stock_adjustment_reasons WHERE requisitionid = requisitionId;
    DELETE FROM requisition.requisition_permission_strings WHERE requisitionid = requisitionId;
    DELETE FROM requisition.requisitions WHERE id = requisitionId;
END;
$$ LANGUAGE plpgsql;