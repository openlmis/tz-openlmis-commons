CREATE TABLE msd_stock_out_notifications
(
    id                       uuid NOT NULL,
    modifiedDate             timestamp with time zone,
    createdDate              timestamp with time zone,
    quoteNumber              text,
    customerId               text NOT NULL,
    customerName             text NOT NULL,
    hfrCode                  text NOT NULL,
    elmisOrderNumber         text NOT NULL,
    notificationDate         date NOT NULL,
    processingDate           date,
    zone                     text NOT NULL,
    comment                  text,
    rationingItems           text,
    stockOutItems            text,
    inSufficientFundingItems text,
    fullFilledItems          text,
    closeToExpireItems       text,
    phasedOutItems      text,
    facilityid          uuid NOT NULL,
    programid           uuid NOT NULL,
    processingperiodid  uuid NOT NULL
);

ALTER TABLE ONLY msd_stock_out_notifications
    ADD CONSTRAINT msd_stock_out_notification_pkey PRIMARY KEY (id);