CREATE TABLE buq_orderable_categories
(
    id          uuid NOT NULL,
    code        text NOT NULL,
    name        text NOT NULL,
    description text NOT NULL
);

ALTER TABLE ONLY buq_orderable_categories
    ADD CONSTRAINT buq_orderable_categories_pkey PRIMARY KEY (id);

CREATE TABLE buq_orderables
(
    categoryid  uuid NOT NULL,
    buqClass    text NOT NULL,
    orderableid uuid NOT NULL
);

ALTER TABLE ONLY buq_orderables
    ADD CONSTRAINT buq_orderables_pkey PRIMARY KEY (orderableid);